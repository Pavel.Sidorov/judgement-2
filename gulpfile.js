/**
 * Сборка проекта:
 * ```
 * ./node_modules/.bin/gulp build
 * ```
 *
 * Разработка:
 * ```
 * ./node_modules/.bin/gulp build watch
 * ```
 */

var gulp   = require('gulp');
var bower  = require('gulp-bower');
var sass   = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var jsmin  = require('gulp-jsmin');
var concat = require('gulp-concat');
var del    = require('del');
var runSeq = require('run-sequence');

/**
 * Сборка.
 */
gulp.task('build', function() {
  runSeq(
      ['bower', 'clean'],
      ['css', 'fonts', 'js']
  );
});

/**
 * Автоматическая пересборка при изменении файлов.
 */
gulp.task('watch', function(){
  gulp.watch('web/assets/scss/**/*.scss', ['css']);
  gulp.watch('web/assets/js/**/*.js', ['js']);
});

/**
 * Очистить.
 */
gulp.task('clean', function() {
  del(['web/compiled/assets/*', '!web/compiled/assets/.gitkeep']);
});

/**
 * Установить зависимости bower.
 */
gulp.task('bower', function() {
  return bower();
});

/**
 * Скомпилировать SASS исходники, минифицировать и склеить в один файл.
 */
gulp.task('css', function(){
  return gulp.src([
      'web/assets/scss/app.scss'
    ])
    .pipe(sass().on('error', sass.logError))
    .pipe(cssmin())
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('web/compiled/assets'));
});

/**
 * Копировать шрифты.
 */
gulp.task('fonts', function() {
  return gulp.src([
      'bower_components/font-awesome/fonts/*.+(otf|eot|svg|ttf|woff|woff2)'
    ])
    .pipe(gulp.dest('web/compiled/assets/fonts'));
});

/**
 * Склеить и минифицировать JS.
 */
gulp.task('js', function() {
  return gulp.src([
      'bower_components/jquery/dist/jquery.js',
      'bower_components/what-input/dist/what-input.js',
      'bower_components/motion-ui/dist/motion-ui.js',
      'bower_components/foundation-sites/dist/js/foundation.js',
      'bower_components/angular/angular.js',
      'bower_components/jquery-mask-plugin/dist/jquery.mask.min.js',
      'web/assets/js/app.js'
    ])
    .pipe(jsmin())
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('web/compiled/assets'));
});
