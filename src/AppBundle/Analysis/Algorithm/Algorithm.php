<?php

namespace AppBundle\Analysis\Algorithm;

use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Dictionary\DictionaryInterface;

/**
 * Алгоритм расчета
 */
abstract class Algorithm implements AlgorithmInterface
{
    /**
     * {@inheritDoc}
     */
    public function prepareInitialData(InitialDataInterface $initial,
            DictionaryInterface $dictionary, array $context): InitialDataInterface
    {
        return $initial;
    }
}
