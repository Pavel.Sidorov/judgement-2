<?php

namespace AppBundle\Analysis\Algorithm;

use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Dictionary\DictionaryInterface;

/**
 * Алгоритм расчета
 */
interface AlgorithmInterface
{
    /**
     * Подготовить исходные данные
     *
     * @param InitialDataInterface $initial Исходные данные
     * @param DictionaryInterface $dictionary Справочник
     * @param array $context Контекст
     *
     * @return InitialDataInterface
     */
    public function prepareInitialData(InitialDataInterface $initial,
            DictionaryInterface $dictionary, array $context): InitialDataInterface;

    /**
     * Выполнить расчет
     *
     * @param InitialDataInterface $initial Исходные данные
     * @param DictionaryInterface $dictionary Справочник
     *
     * @return ResultDataInterface Результаты
     */
    public function calculate(InitialDataInterface $initial,
            DictionaryInterface $dictionary): ResultDataInterface;
}
