<?php

namespace AppBundle\Analysis\Algorithm;

use AppBundle\Integration\Scoring\ScoringInterface;
use AppBundle\Integration\Scoring\App\AppMetaInterface;
use AppBundle\Integration\Scoring\App\AppDataInterface;

/**
 * Алгоритм расчета, использующий скоринг
 */
abstract class ScoringAwareAlgorithm extends Algorithm
{
    /**
     * Скоринг
     *
     * @var ScoringInterface
     */
    protected $scoring;

    /**
     * Конструктор
     *
     * @param ScoringInterface $scoring
     */
    public function __construct(ScoringInterface $scoring)
    {
        $this->scoring = $scoring;
    }

    /**
     * Получить сводную информацию по скоринговой заявке
     *
     * @param array $context
     *
     * @return AppMetaInterface
     * @throws \LogicException
     */
    protected function getAppMeta(array $context)
    {
        if (!isset($context['scoringId'])) {
            throw new \LogicException('No scoringId');
        }

        return $this->scoring->getAppMeta($context['scoringId']);
    }

    /**
     * Получить ответы скоринговой заявки
     *
     * @param array $context
     *
     * @return AppDataInterface
     * @throws \LogicException
     */
    protected function getAppData(array $context)
    {
        if (!isset($context['scoringId'])) {
            throw new \LogicException('No scoringId');
        }

        return $this->scoring->getAppData($context['scoringId']);
    }
}
