<?php

namespace AppBundle\Analysis\Analysis;

use AppBundle\Analysis\Dictionary\DictionaryInterface;
use AppBundle\Analysis\Report\ReportInterface;
use AppBundle\Analysis\Report\ReportBuilderInterface;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\InputForm\InputFormInterface;
use AppBundle\Analysis\Algorithm\AlgorithmInterface;

/**
 * Методика анализа
 */
class Analysis implements AnalysisInterface
{
    /**
     * Название
     *
     * @var string
     */
    private $name;

    /**
     * Тип
     *
     * @var string
     */
    private $type;

    /**
     * Версия методики
     *
     * @var int
     */
    private $version;

    /**
     * Статус
     *
     * @var string
     */
    private $status;

    /**
     * Типы скоринговых заявок для создания заданий на расчет
     *
     * @var array
     */
    private $scoringAppTypes;

    /**
     * Скоринговые статусы для создания или копирования заданий на расчет
     *
     * @var array
     */
    private $scoringStatuses;

    /**
     * Справочник
     *
     * @var DictionaryInterface
     */
    private $dictionary;

    /**
     * Отрисовщик формы
     *
     * @var InputFormInterface
     */
    private $inputForm;

    /**
     * Алгоритм расчета
     *
     * @var AlgorithmInterface
     */
    private $algorithm;

    /**
     * Построитель отчетов
     *
     * @var ReportBuilderInterface
     */
    private $reportBuilder;

    /**
     * Конструктор
     *
     * @param string $name Название
     * @param string $type Тип
     * @param int $version Версия методики
     * @param string $status Статус
     * @param array $scoringAppTypes Типы скоринговых заявок
     * @param array $scoringStatuses Статусы скоринговых заявок
     * @param DictionaryInterface $dictionary Справочник
     * @param InputFormInterface $inputForm Отрисовщик формы ввода данных
     * @param AlgorithmInterface $algorithm Алгоритм расчета
     * @param ReportBuilderInterface $reportBuilder Построитель отчетов
     */
    public function __construct(string $name, string $type, int $version,
            string $status, array $scoringAppTypes, array $scoringStatuses,
            DictionaryInterface $dictionary, InputFormInterface $inputForm,
            AlgorithmInterface $algorithm, ReportBuilderInterface $reportBuilder)
    {
        $this->name = $name;
        $this->type = $type;
        $this->version = $version;
        $this->status = $status;
        $this->scoringAppTypes = $scoringAppTypes;
        $this->scoringStatuses = $scoringStatuses;
        $this->dictionary = $dictionary;
        $this->inputForm = $inputForm;
        $this->algorithm = $algorithm;
        $this->reportBuilder = $reportBuilder;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritDoc}
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * {@inheritDoc}
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * {@inheritDoc}
     */
    public function getScoringAppTypes(): array
    {
        return $this->scoringAppTypes;
    }

    /**
     * {@inheritDoc}
     */
    public function getScoringStatuses(): array
    {
        return $this->scoringStatuses;
    }

    /**
     * {@inheritDoc}
     */
    public function prepareInitialData(InitialDataInterface $initial,
            array $context): InitialDataInterface
    {
        return $this->algorithm->prepareInitialData($initial, $this->dictionary, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function renderInputForm(InitialDataInterface $initial,
            string $targetUrl): string
    {
        return $this->inputForm->render($initial, $this->dictionary, $targetUrl);
    }

    /**
     * {@inheritDoc}
     */
    public function calculate(InitialDataInterface $initial): ResultDataInterface
    {
        return $this->algorithm->calculate($initial, $this->dictionary);
    }

    /**
     * {@inheritDoc}
     */
    public function buildReport(InitialDataInterface $initial,
            ResultDataInterface $result): ReportInterface
    {
        return $this->reportBuilder->build($initial, $result, $this->dictionary);
    }
}