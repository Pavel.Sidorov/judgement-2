<?php

namespace AppBundle\Analysis\Analysis;

use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Report\ReportInterface;

/**
 * Методика анализа
 */
interface AnalysisInterface
{
    // Тип "Первичный анализ", выполняется на этапе принятия решения о выдаче кредита
    const TYPE_PRIMARY = 'primary';

    // Тип "Вторичный анализ", выполняется на этапе действия кредитного договора
    const TYPE_SECONDARY = 'secondary';

    // Список типовz
    const TYPES = [
        self::TYPE_PRIMARY,
        self::TYPE_SECONDARY,
    ];

    // Статус "действующий"
    const STATUS_ACTIVE = 'active';

    // Статус "архивный"
    const STATUS_ARCHIVAL = 'archival';

    // Список статусов
    const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_ARCHIVAL,
    ];

    /**
     * Получить название
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Получить тип
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Получить номер версии
     *
     * @return int
     */
    public function getVersion(): int;

    /**
     * Получить статус методики
     *
     * @return string
     */
    public function getStatus(): string;

    /**
     * Получить список типов скоринговых заявок, для которых возможно создание
     * заданий на расчет
     *
     * @return array
     */
    public function getScoringAppTypes(): array;

    /**
     * Получить список статусов скоринговых заявок, при которых возможно
     * создание или копирование заданий на расчет
     *
     * @return array
     */
    public function getScoringStatuses(): array;

    /**
     * Подготовить исходные данные
     *
     * @param InitialDataInterface $initial Исходные данные
     * @param array $context Контекст
     *
     * @return InitialDataInterface
     */
    public function prepareInitialData(InitialDataInterface $initial, array $context): InitialDataInterface;

    /**
     * Получить форму для ввода данных
     *
     * @param InitialDataInterface $initial Данные для отображения в форме
     * @param string $targetUrl URL тправки формы
     *
     * @return string
     */
    public function renderInputForm(InitialDataInterface $initial, string $targetUrl): string;

    /**
     * Выполнить расчеты и вернуть результаты
     *
     * @param InitialDataInterface $initial Исходные данные для расчета
     *
     * @return ResultDataInterface
     */
    public function calculate(InitialDataInterface $initial): ResultDataInterface;

    /**
     * Построить отчет по анализу
     *
     * @param InitialDataInterface $initial Исходные данные
     * @param ResultDataInterface $result Результаты расчета
     *
     * @return ReportInterface
     */
    public function buildReport(InitialDataInterface $initial, ResultDataInterface $result): ReportInterface;
}
