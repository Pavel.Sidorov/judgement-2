<?php

namespace AppBundle\Analysis\Data;

/**
 * Исходные данные анализа
 */
interface DataInterface extends \ArrayAccess, \Countable
{
    /**
     * Получить все данные
     *
     * @return array
     */
    public function getData();

    /**
     * Установить все данные
     *
     * @param array
     */
    public function setData(array $data);

    /**
     * Получить переменную
     *
     * @param string $property
     *
     * @return mixed
     */
    public function get(string $property);

    /**
     * Установить переменную
     *
     * @param string $property
     * @param mixed $value
     */
    public function set(string $property, $value);

    /**
     * Проверить существование переменной
     *
     * @param string $property
     *
     * @return bool
     */
    public function has(string $property): bool;

    /**
     * Удалить переменную
     *
     * @param string $property
     */
    public function delete(string $property);

    /**
     * Magic-метод. Получить переменную
     *
     * @param string $property
     *
     * @return mixed
     */
    public function __get($property);

    /**
     * Magic-метод. Установить переменную
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value);

    /**
     * Magic-метод. Проверить существование переменной
     *
     * @param string $property
     *
     * @return bool
     */
    public function __isset($property);

    /**
     * Magic-метод. Удалить переменную
     *
     * @param string $property
     */
    public function __unset($property);

    /**
     * Magic-метод. Вызов методов
     *
     * @param string $property
     * @param mixed $args
     *
     * @return mixed
     */
    public function __call($method, $args);
}
