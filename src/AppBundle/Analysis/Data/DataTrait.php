<?php

namespace AppBundle\Analysis\Data;

/**
 * Трейт для реализации DataInterface
 */
trait DataTrait
{
    /**
     * Данные справочника
     *
     * @var array
     */
    private $data = [];

    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * {@inheritDoc}
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * {@inheritDoc}
     */
    public function get(string $property)
    {
        return $this->has($property) ? $this->data[$property] : null;
    }

    /**
     * {@inheritDoc}
     */
    public function set(string $property, $value)
    {
        $this->data[$property] = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function has(string $property): bool
    {
        return array_key_exists($property, $this->data);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(string $property)
    {
        unset($this->data[$property]);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetGet($property)
    {
        return $this->get($property);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetExists($property)
    {
        return $this->has($property);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetSet($property, $value)
    {
        return $this->set($property, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetUnset($property)
    {
        return $this->delete($property);
    }

    /**
     * {@inheritDoc}
     */
    public function __get($property)
    {
        return $this->get($property);
    }

    /**
     * {@inheritDoc}
     */
    public function __isset($property)
    {
        return $this->has($property);
    }

    /**
     * {@inheritDoc}
     */
    public function __set($property, $value)
    {
        return $this->set($property, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function __unset($property)
    {
        return $this->delete($property);
    }

    /**
     * {@inheritDoc}
     */
    public function __call($method, $args)
    {
        if (count($args) > 0) {
            return $this->set($method, $args[0]);
        } else {
            return $this->get($method);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function count()
    {
        return count($this->data);
    }
}
