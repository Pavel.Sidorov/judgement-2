<?php

namespace AppBundle\Analysis\Data;

/**
 * Исходные данные анализа
 */
class InitialData implements InitialDataInterface
{
    use DataTrait;
}
