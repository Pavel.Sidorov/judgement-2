<?php

namespace AppBundle\Analysis\Data;

/**
 * Исходные данные анализа
 */
interface InitialDataInterface extends DataInterface
{
}
