<?php

namespace AppBundle\Analysis\Data;

/**
 * Результаты расчета
 */
class ResultData implements ResultDataInterface
{
    use DataTrait;
}
