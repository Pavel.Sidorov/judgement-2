<?php

namespace AppBundle\Analysis\Dictionary;

/**
 * Справочник
 */
interface DictionaryInterface extends \ArrayAccess, \Countable
{
    /**
     * Получить все данные
     *
     * @return array
     */
    public function getData();

    /**
     * Получить значение
     *
     * @param string $property
     *
     * @return mixed
     */
    public function get($property);

    /**
     * Проверить существование значения
     *
     * @param string $property
     *
     * @return bool
     */
    public function has($property);

    /**
     * Magic-метод. Получить значение
     *
     * @param string $property
     *
     * @return mixed
     */
    public function __get($property);

    /**
     * Magic-метод. Проверить существование значения
     *
     * @param string $property
     *
     * @return bool
     */
    public function __isset($property);

    /**
     * Magic-метод. Вызов методов
     *
     * @param string $property
     * @param mixed $args
     *
     * @return mixed
     */
    public function __call($method, $args);
}
