<?php

namespace AppBundle\Analysis\Dictionary;

use Symfony\Component\Yaml\Yaml;

/**
 * Справочник на основе YAML-файла
 */
class YamlDictionary implements DictionaryInterface
{
    /**
     * Данные справочника
     *
     * @var array
     */
    private $data;

    /**
     * Конструктор
     *
     * @param string $file Полный путь к YAML-файлу
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $file)
    {
        if (!file_exists($file) || !is_readable($file)) {
            throw new \InvalidArgumentException('Файл не существует или недоступен для чтения');
        }
        
        $this->data = Yaml::parse(file_get_contents($file));
    }

    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * {@inheritDoc}
     */
    public function get($property)
    {
        return $this->has($property) ? $this->data[$property] : null;
    }

    /**
     * {@inheritDoc}
     */
    public function has($property)
    {
        return array_key_exists($property, $this->data);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetGet($property)
    {
        return $this->get($property);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetExists($property)
    {
        return $this->has($property);
    }

    /**
     * {@inheritDoc}
     * @throws \LogicException
     */
    public function offsetSet($property, $value)
    {
        throw new \LogicException('Недопустимая операция над словарем');
    }

    /**
     * {@inheritDoc}
     * @throws \LogicException
     */
    public function offsetUnset($property)
    {
        throw new \LogicException('Недопустимая операция над словарем');
    }

    /**
     * {@inheritDoc}
     */
    public function __get($property)
    {
        return $this->get($property);
    }

    /**
     * {@inheritDoc}
     */
    public function __isset($property)
    {
        return $this->has($property);
    }

    /**
     * {@inheritDoc}
     */
    public function count()
    {
        return count($this->data);
    }

    /**
     * {@inheritDoc}
     */
    public function __call($method, $args)
    {
        if (count($args) > 0) {
            throw new \LogicException('Недопустимая операция над словарем');
        } else {
            return $this->get($method);
        }
    }
}
