<?php

namespace AppBundle\Analysis\InputForm;

use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Dictionary\DictionaryInterface;

/**
 * Форма ввода данных
 */
interface InputFormInterface
{
    /**
     * Отрисовать форму
     *
     * @param InitialDataInterface $initial Исходные данные для подстановки в форму
     * @param DictionaryInterface $dictionary Справочник
     * @param string $targetUrl URL отправки формы
     *
     * @return string Код формы
     */
    public function render(InitialDataInterface $initial,
            DictionaryInterface $dictionary, string $targetUrl): string;
}
