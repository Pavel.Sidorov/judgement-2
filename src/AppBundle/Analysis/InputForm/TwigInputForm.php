<?php

namespace AppBundle\Analysis\InputForm;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Bundle\TwigBundle\Loader\FilesystemLoader;
use AppBundle\Analysis\InputForm\InputFormInterface;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Dictionary\DictionaryInterface;

/**
 * Отрисовщик формы ввода данных
 */
class TwigInputForm implements InputFormInterface
{
    /**
     * Twig
     *
     * @var TwigEngine
     */
    private $twig;

    /**
     * Загрузчик шаблонов
     *
     * @var FilesystemLoader
     */
    private $loader;

    /**
     * Файл шаблона
     *
     * @var string
     */
    private $template;

    /**
     * Конструктор
     *
     * @param TwigEngine $twig
     * @param FilesystemLoader $loader
     * @param string $template Полный путь к файлу шаблона
     */
    public function __construct(TwigEngine $twig, FilesystemLoader $loader, string $template)
    {
        $this->twig = $twig;
        $this->loader = $loader;
        $this->template = basename($template);

        $path = dirname($template);
        if (!in_array($path, $this->loader->getPaths(), true)) {
            $this->loader->addPath($path);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function render(InitialDataInterface $initial,
            DictionaryInterface $dictionary, string $targetUrl): string
    {
        return $this->twig->render($this->template, [
            'initial' => $initial,
            'dictionary' => $dictionary,
            'targetUrl' => $targetUrl,
        ]);
    }
}
