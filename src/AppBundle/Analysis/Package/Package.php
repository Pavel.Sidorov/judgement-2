<?php

namespace AppBundle\Analysis\Package;

use AppBundle\Analysis\Analysis\AnalysisInterface;

/**
 * Пакет методик анализа
 *
 * Содержит все версии методик анализа по одному продукту.
 */
class Package implements PackageInterface
{
    /**
     * Название
     *
     * @var string
     */
    private $name;

    /**
     * Список методик
     *
     * @var array
     */
    private $analysis = [];

    /**
     * Конструктор
     *
     * @param string $name Название
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Проверить, поддерживается ли методика
     *
     * @param AnalysisInterface $analysis
     *
     * @return bool
     */
    private function isAnalysisSupported(AnalysisInterface $analysis): bool
    {
        return $this->getName() === $analysis->getName();
    }

    /**
     * {@inheritDoc}
     * @throws \LogicException
     */
    public function addAnalysis(AnalysisInterface $analysis)
    {
        if (!$this->isAnalysisSupported($analysis)) {
            throw new \LogicException('Analysis is not supporter');
        }

        $type = $analysis->getType();
        $version = $analysis->getVersion();

        if (!array_key_exists($type, $this->analysis)) {
            $this->analysis[$type] = [];
        }

        $this->analysis[$type][$version] = $analysis;
    }

    /**
     * {@inheritDoc}
     */
    public function getAnalysisTypes(): array
    {
        return array_keys($this->analysis);
    }

    /**
     * {@inheritDoc}
     * @throws \LogicException
     */
    public function getAnalysisVersions(string $type): array
    {
        if (!array_key_exists($type, $this->analysis)) {
            throw new \LogicException('Unknown analysis type');
        }

        return array_keys($this->analysis[$type]);
    }

    /**
     * {@inheritDoc}
     * @throws \LogicException
     */
    public function getAnalysis(string $type, int $version): AnalysisInterface
    {
        if (!array_key_exists($type, $this->analysis)) {
            throw new \LogicException('Unknown analysis type');
        }

        if (!array_key_exists($version, $this->analysis[$type])) {
            throw new \LogicException('Unknown analysis version');
        }

        return $this->analysis[$type][$version];
    }

    /**
     * {@inheritDoc}
     */
    public function getAnalysisList(): array
    {
        $output = [];

        foreach ($this->analysis as $versions) {
            $output = array_merge($output, array_values($versions));
        };

        return $output;
    }
}
