<?php

namespace AppBundle\Analysis\Package;

use AppBundle\Analysis\Analysis\AnalysisInterface;

/**
 * Пакет методик анализа
 *
 * Содержит все версии методик анализа по одному продукту.
 *
 * Анализы подразделяются на два типа: первичные (primary) и вторичные (secondary).
 */
interface PackageInterface
{
    /**
     * Получить название
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Добавить методику анализа в пакет
     *
     * @param AnalysisInterface $analysis
     */
    public function addAnalysis(AnalysisInterface $analysis);

    /**
     * Получить список типов методик
     *
     * @return array
     */
    public function getAnalysisTypes(): array;

    /**
     * Получить список версий методик по типу
     *
     * @param string $type Тип
     *
     * @return array
     */
    public function getAnalysisVersions(string $type): array;

    /**
     * Получить методику по типу и версии
     *
     * @param string $type Тип
     * @param int $version Номер версии
     *
     * @return AnalysisInterface
     */
    public function getAnalysis(string $type, int $version): AnalysisInterface;

    /**
     * Получить список методик
     *
     * @return array
     */
    public function getAnalysisList(): array;
}
