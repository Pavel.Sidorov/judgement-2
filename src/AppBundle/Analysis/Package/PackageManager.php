<?php

namespace AppBundle\Analysis\Package;

/**
 * Менеджер пакетов методик анализа
 *
 * Содержит все пакеты методик анализа по всем продуктам.
 */
class PackageManager implements PackageManagerInterface
{
    /**
     * Список пакетов
     *
     * @var array
     */
    private $packages = [];

    /**
     * {@inheritDoc}
     * @throws \LogicException
     */
    public function addPackage(PackageInterface $package)
    {
        $name = $package->getName();

        if (array_key_exists($name, $this->packages)) {
            throw new \LogicException(sprintf('Для %s уже был добавен пакет', $name));
        }

        $this->packages[$package->getName()] = $package;
    }

    /**
     * {@inheritDoc}
     */
    public function getPackagesNames(): array
    {
        return array_keys($this->packages);
    }

    /**
     * {@inheritDoc}
     * @throws \LogicException
     */
    public function getPackage(string $name): PackageInterface
    {
        if (!array_key_exists($name, $this->packages)) {
            throw new \LogicException(sprintf('Отсутствует пакет для %s', $name));
        }

        return $this->packages[$name];
    }
}
