<?php

namespace AppBundle\Analysis\Package;

/**
 * Менеджер пакетов методик анализа
 *
 * Содержит все пакеты методик анализа по всем продуктам.
 */
interface PackageManagerInterface
{
    /**
     * Добавить пакет
     */
    public function addPackage(PackageInterface $package);

    /**
     * Получить список имен пакетов
     */
    public function getPackagesNames(): array;

    /**
     * Получить пакет по имени
     */
    public function getPackage(string $name): PackageInterface;
}
