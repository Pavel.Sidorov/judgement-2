<?php

namespace AppBundle\Analysis\Report;

/**
 * Отчет
 */
class Report implements ReportInterface
{
    /**
     * MIME-тип
     *
     * @var string
     */
    private $mimeType;

    /**
     * Расширение файла
     *
     * @var string
     */
    private $fileExtension;

    /**
     * Контент
     *
     * @var string
     */
    private $content;

    /**
     * Конструктор
     *
     * @param string $mimeType
     * @param string $extension
     * @param string $content
     */
    public function __construct($mimeType, $fileExtension, $content)
    {
        $this->mimeType = $mimeType;
        $this->fileExtension = $fileExtension;
        $this->content = $content;
    }

    /**
     * {@inheritDoc}
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * {@inheritDoc}
     */
    public function getFileExtension(): string
    {
        return $this->fileExtension;
    }

    /**
     * {@inheritDoc}
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
