<?php

namespace AppBundle\Analysis\Report;

use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Dictionary\DictionaryInterface;
use AppBundle\Analysis\Report\ReportInterface;

/**
 * Построитель отчетов
 */
interface ReportBuilderInterface
{
    /**
     * Построить отчет
     *
     * @param InitialDataInterface $initial Исходные данные
     * @param ResultDataInterface $result Результаты расчета
     * @param DictionaryInterface $dictionary Справочник
     *
     * @return ReportInterface Отчет
     */
    public function build(InitialDataInterface $initial,
            ResultDataInterface $result,
            DictionaryInterface $dictionary): ReportInterface;
}
