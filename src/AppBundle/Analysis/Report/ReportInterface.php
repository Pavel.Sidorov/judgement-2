<?php

namespace AppBundle\Analysis\Report;

/**
 * Отчет
 */
interface ReportInterface
{
    /**
     * Получить MIME-тип
     */
    public function getMimeType(): string;

    /**
     * Получить расширение файла
     */
    public function getFileExtension(): string;

    /**
     * Получить контент
     */
    public function getContent(): string;
}
