<?php

namespace AppBundle\Analysis\Report;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Bundle\TwigBundle\Loader\FilesystemLoader;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Dictionary\DictionaryInterface;
use AppBundle\Analysis\Report\ReportInterface;
use AppBundle\Analysis\Report\Report;

/**
 * Построитель отчетов на базе Twig
 */
class TwigReportBuilder implements ReportBuilderInterface
{
    /**
     * Twig
     *
     * @var TwigEngine
     */
    private $twig;

    /**
     * Загрузчик шаблонов
     *
     * @var FilesystemLoader
     */
    private $loader;
    
    /**
     * Файл шаблона
     *
     * @var string
     */
    private $template;

    /**
     * MIME-тип
     *
     * @var string
     */
    private $mimeType;

    /**
     * Расширение файла
     *
     * @var string
     */
    private $fileExtension;

    /**
     * Конструктор
     *
     * @param TwigEngine $twig
     * @param FilesystemLoader $loader
     * @param string $template Полный путь к файлу шаблона
     * @param string $mimeType
     */
    public function __construct(TwigEngine $twig, FilesystemLoader $loader, string $template, string $mimeType, string $fileExtension)
    {
        $this->twig = $twig;
        $this->loader = $loader;
        $this->template = basename($template);
        $this->mimeType = $mimeType;
        $this->fileExtension = $fileExtension;

        $path = dirname($template);
        if (!in_array($path, $this->loader->getPaths(), true)) {
            $this->loader->addPath($path);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function build(InitialDataInterface $initial,
            ResultDataInterface $result,
            DictionaryInterface $dictionary): ReportInterface
    {
        $content = $this->twig->render($this->template, [
            'initial' => $initial,
            'result' => $result,
            'dictionary' => $dictionary,
        ]);

        return new Report($this->mimeType, $this->fileExtension, $content);
    }
}
