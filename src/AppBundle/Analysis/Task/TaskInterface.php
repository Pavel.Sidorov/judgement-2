<?php

namespace AppBundle\Analysis\Task;

use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;

/**
 * Задание на расчет
 */
interface TaskInterface
{
    /**
     * Получить идентификатор
     *
     * @return int
     */
    public function getId();

    /**
     * Получить описание
     *
     * @return string
     */
    public function getDescription();

    /**
     * Установить описание
     *
     * @param string $description
     */
    public function setDescription(string $description = null);

    /**
     * Получить родительское задание
     *
     * @return TaskInterface
     */
    public function getParent();

    /**
     * Установить родительское задание
     *
     * @param TaskInterface $parent
     */
    public function setParent(TaskInterface $parent = null);

    /**
     * Получить дочерние задания
     *
     * @return array
     */
    public function getChildren(): array;

    /**
     * Получить название методики анализа
     *
     * @return string
     */
    public function getAnalysisName();

    /**
     * Установить название методики анализа
     *
     * @param string $name
     */
    public function setAnalysisName(string $name);

    /**
     * Получить тип методики анализа
     *
     * @return string
     */
    public function getAnalysisType();

    /**
     * Установить тип методики анализа
     *
     * @param string $type
     */
    public function setAnalysisType(string $type);

    /**
     * Получить версию методики анализа
     *
     * @return int
     */
    public function getAnalysisVersion();

    /**
     * Установить версию методики анализа
     *
     * @param int $version
     */
    public function setAnalysisVersion(int $version);

    /**
     * Получить ID скоринговой заявки
     *
     * @return int
     */
    public function getScoringId();

    /**
     * Установить ID скоринговой заявки
     *
     * @param int $id
     */
    public function setScoringId(int $id);

    /**
     * Получить исходные данные
     *
     * @return InitialDataInterface
     */
    public function getInitialData(): InitialDataInterface;

    /**
     * Установить исходные данные
     *
     * @return $initial InitialDataInterface
     */
    public function setInitialData(InitialDataInterface $initial);

    /**
     * Получить результаты расчета
     *
     * @return ResultDataInterface
     */
    public function getResultData(): ResultDataInterface;

    /**
     * Установить результаты расчета
     *
     * @param $result ResultDataInterface
     */
    public function setResultData(ResultDataInterface $result);

    /**
     * Получить дату создания
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Пометить задание обновленным
     */
    public function markUpdated();

    /**
     * Получить дату обновления
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Пометить задание выполненным
     */
    public function markCompleted();

    /**
     * Является ли задание выполненным
     *
     * @return bool
     */
    public function isCompleted(): bool;

    /**
     * Получить дату обновления
     *
     * @return \DateTime
     */
    public function getCompletedAt();

    /**
     * Пометить задание как итоговое
     */
    public function markFinal();

    /**
     * Снять отметку итогового задания
     */
    public function unmarkFinal();

    /**
     * Является ли задание итоговым
     *
     * @return bool
     */
    public function isFinal(): bool;

    /**
     * Получить имя клиента
     *
     * @return string
     */
    public function getClientName();

    /**
     * Установить имя клиента
     *
     * @param string
     */
    public function setClientName(string $clientName = null);
}
