<?php

namespace AppBundle\Analysis\Task;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Analysis\Package\PackageManagerInterface;
use AppBundle\Analysis\Data\InitialData;
use AppBundle\Analysis\Report\ReportInterface;
use AppBundle\Analysis\Task\TaskManagerInterface;
use AppBundle\Analysis\Task\TaskInterface;
use AppBundle\Security\Voter\TaskVoter;
use AppBundle\Integration\Scoring\ScoringInterface;
use AppBundle\Security\Roles;
use AppBundle\Entity\Task;

/**
 * Менеджер заданий
 */
class TaskManager implements TaskManagerInterface
{
    /**
     * Doctrine entity manager
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Проверка авторизации
     *
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    /**
     * Менеджер пакетов методик
     *
     * @var PackageManagerInterface
     */
    private $packageManager;

    /**
     * Скоринг
     *
     * @var ScoringInterface
     */
    private $scoring;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     * @param AuthorizationCheckerInterface $authChecker
     * @param PackageManagerInterface $packageManager
     * @param ScoringInterface $scoring
     */
    public function __construct(EntityManagerInterface $entityManager,
            AuthorizationCheckerInterface $authChecker,
            PackageManagerInterface $packageManager,
            ScoringInterface $scoring)
    {
        $this->entityManager = $entityManager;
        $this->authChecker = $authChecker;
        $this->packageManager = $packageManager;
        $this->scoring = $scoring;
    }

    /**
     * Проверка авторизации
     *
     * @param mixed  $attributes
     * @param mixed  $object
     * @param string $message
     *
     * @throws AccessDeniedException
     */
    protected function denyAccessUnlessGranted($attributes, $object = null)
    {
        if (!$this->authChecker->isGranted($attributes, $object)) {
            $exception = new AccessDeniedException('Access denied');
            $exception->setAttributes($attributes);
            $exception->setSubject($object);

            throw $exception;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function countRootTasks(string $analysisName = null,
            string $analysisType = null, int $scoringId = null,
            string $clientName = null): int
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);

        return $this->entityManager
            ->getRepository('AppBundle:Task')
            ->countRootTasks($analysisName, $analysisType, $scoringId, $clientName);
    }

    /**
     * {@inheritDoc}
     */
    public function getRootTasks(string $analysisName = null,
            string $analysisType = null, int $scoringId = null,
            string $clientName = null, int $offset = null,
            int $limit = null): array
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);

        return $this->entityManager
            ->getRepository('AppBundle:Task')
            ->getRootTasks($analysisName, $analysisType, $scoringId, $clientName, $offset, $limit);
    }

    /**
     * {@inheritDoc}
     */
    public function createTask(TaskInterface $task)
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_CREATE);
        $this->denyAccessUnlessGranted(TaskVoter::CREATE, $task);

        // Подгружаем имя клиента
        $meta = $this->scoring->getAppMeta($task->getScoringId());
        if ($meta !== null) {
            $task->setClientName($meta->getExtraValue('clientName'));
        }

        // Подготавливаем исходные данные
        $analysis = $this->packageManager
            ->getPackage($task->getAnalysisName())
            ->getAnalysis(
                $task->getAnalysisType(),
                $task->getAnalysisVersion()
            );
        $initial = $analysis->prepareInitialData($task->getInitialData(), [
            'scoringId' => $task->getScoringId(),
        ]);
        $task->setInitialData($initial);

        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function readTask(int $id): TaskInterface
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_READ);

        $task = $this->entityManager
            ->getRepository('AppBundle:Task')
            ->find($id);

        $this->denyAccessUnlessGranted(TaskVoter::READ, $task);

        return $task;
    }

    /**
     * {@inheritDoc}
     */
    public function updateTask(TaskInterface $task)
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_UPDATE);
        $this->denyAccessUnlessGranted(TaskVoter::UPDATE, $task);

        $task->markUpdated();

        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getInputForm(TaskInterface $task, string $targetUrl): string
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_INPUT);
        $this->denyAccessUnlessGranted(TaskVoter::INPUT, $task);

        $analysis = $this->packageManager
            ->getPackage($task->getAnalysisName())
            ->getAnalysis(
                $task->getAnalysisType(),
                $task->getAnalysisVersion()
            );

        return $analysis->renderInputForm($task->getInitialData(), $targetUrl);
    }

    /**
     * {@inheritDoc}
     */
    public function updateInitialData(TaskInterface $task, array $data)
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_INPUT);
        $this->denyAccessUnlessGranted(TaskVoter::INPUT, $task);

        $initial = new InitialData();
        $initial->setData($data);

        $task->setInitialData($initial);
        $task->markUpdated();

        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function completeTask(TaskInterface $task)
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_COMPLETE);
        $this->denyAccessUnlessGranted(TaskVoter::COMPLETE, $task);

        $analysis = $this->packageManager
            ->getPackage($task->getAnalysisName())
            ->getAnalysis(
                $task->getAnalysisType(),
                $task->getAnalysisVersion()
            );

        $result = $analysis->calculate($task->getInitialData());

        $task->setResultData($result);
        $task->markCompleted();

        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getReport(TaskInterface $task): ReportInterface
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_REPORT);
        $this->denyAccessUnlessGranted(TaskVoter::REPORT, $task);

        $analysis = $this->packageManager
            ->getPackage($task->getAnalysisName())
            ->getAnalysis(
                $task->getAnalysisType(),
                $task->getAnalysisVersion()
            );

        return $analysis->buildReport($task->getInitialData(), $task->getResultData());
    }

    /**
     * {@inheritDoc}
     */
    public function prepareForkTask(TaskInterface $task): TaskInterface
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_FORK);
        $this->denyAccessUnlessGranted(TaskVoter::FORK, $task);

        $new = new Task();
        $new->setAnalysisName($task->getAnalysisName());
        $new->setAnalysisType($task->getAnalysisType());
        $new->setAnalysisVersion($task->getAnalysisVersion());
        $new->setScoringId($task->getScoringId());
        $new->setInitialData($task->getInitialData());
        $new->setClientName($task->getClientName());
        $new->setDescription($task->getDescription());
        $new->setParent($task->getParent() ? $task->getParent() : $task);

        return $new;
    }

    /**
     * {@inheritDoc}
     */
    public function forkTask(TaskInterface $task)
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_FORK);
        $this->denyAccessUnlessGranted(TaskVoter::FORK, $task);

        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function summarizeTask(TaskInterface $task)
    {
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK);
        $this->denyAccessUnlessGranted(Roles::ROLE_TASK_SUMMARIZE);
        $this->denyAccessUnlessGranted(TaskVoter::SUMMARIZE, $task);

        // Все задания в группе
        if ($task->getParent() === null) {
            $group = $task->getChildren();
            $group[] = $task;
        } else {
            $group = $task->getParent()->getChildren();
            $group[] = $task->getParent();
        }

        foreach ($group as $next) {
            if ($next->getId() === $task->getId()) {
                // Выбранное задание помечаем итоговым
                $next->markFinal();
                $this->entityManager->persist($next);
            } elseif ($next->isFinal()) {
                // С остальных снимаем отметку
                $next->unmarkFinal();
                $this->entityManager->persist($next);
            }
        }

        $this->entityManager->flush();
    }
}
