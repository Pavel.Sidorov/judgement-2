<?php

namespace AppBundle\Analysis\Task;

use AppBundle\Analysis\Task\TaskInterface;
use AppBundle\Analysis\Report\ReportInterface;

/**
 * Менеджер заданий
 */
interface TaskManagerInterface
{
    /**
     * Получить количество корневых заданий
     *
     * @param string $analysisName
     * @param string $analysisType
     * @param int $scoringId
     * @param string $clientName
     *
     * @return int
     */
    public function countRootTasks(string $analysisName = null,
            string $analysisType = null, int $scoringId = null,
            string $clientName = null): int;

    /**
     * Получить корневые задания
     *
     * @param string $analysisName
     * @param string $analysisType
     * @param int $scoringId
     * @param string $clientName
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function getRootTasks(string $analysisName = null,
            string $analysisType = null, int $scoringId = null,
            string $clientName = null, int $offset = null,
            int $limit = null): array;

    /**
     * Создать задание
     *
     * @param TaskInterface $task
     */
    public function createTask(TaskInterface $task);

    /**
     * Считать задание
     *
     * @param int $id
     *
     * @return TaskInterface
     */
    public function readTask(int $id): TaskInterface;

    /**
     * Изменить задание
     *
     * @param TaskInterface $task
     */
    public function updateTask(TaskInterface $task);

    /**
     * Получить форму ввода данных
     *
     * @param TaskInterface $task
     * @param string $targetUrl
     *
     * @return string
     */
    public function getInputForm(TaskInterface $task, string $targetUrl): string;

    /**
     * Изменить исходные данные
     *
     * @param TaskInterface $task
     * @param array $data
     */
    public function updateInitialData(TaskInterface $task, array $data);

    /**
     * Завершить задание
     *
     * @param TaskInterface $task
     */
    public function completeTask(TaskInterface $task);

    /**
     * Отчет по заданию
     *
     * @param TaskInterface $task
     *
     * @return ReportInterface
     */
    public function getReport(TaskInterface $task): ReportInterface;

    /**
     * Подготовить копию задание
     *
     * @param TaskInterface $task
     *
     * @return TaskInterface
     */
    public function prepareForkTask(TaskInterface $task): TaskInterface;

    /**
     * Сохранить копию задания
     *
     * @param TaskInterface $task
     */
    public function forkTask(TaskInterface $task);

    /**
     * Сделать задание итоговым
     *
     * @param TaskInterface $task
     */
    public function summarizeTask(TaskInterface $task);
}
