<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\Criteria;
use AppBundle\Entity\Group;
use AppBundle\Form\GroupType;

/**
 * Группы пользователей
 *
 * @Route("/group")
 *
 * @Security("has_role('ROLE_GROUP')")
 */
class GroupController extends Controller
{
    /**
     * Список групп
     *
     * @Route("/", name="group")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Group');

        $entities = $repo->findBy([], [
            'title' => Criteria::ASC,
        ]);

        return $this->render('group/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Создать группу
     *
     * @Route("/create", name="group_create")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createGroup(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Group();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $message = $this->prepareEventMessage($entity, 'group.message.created');
            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('group_read', ['id' => $entity->getId()]);
        }

        return $this->render('group/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Форма создания группы
     *
     * @param Group $entity
     *
     * @return Form
     */
    private function createCreateForm(Group $entity)
    {
        $form = $this->createForm(GroupType::class, $entity, [
            'action' => $this->generateUrl('group_create'),
            'method' => Request::METHOD_POST,
            'package_manager' => $this->get('app.analysis.packageManager'),
        ]);

        return $form;
    }

    /**
     * Прочитать информацию о группе
     *
     * @Route("/read/{id}", name="group_read")
     * @ParamConverter("entity", class="AppBundle:Group")
     *
     * @param Request $request
     * @param Group $entity
     *
     * @return Response
     */
    public function readGroup(Request $request, Group $entity)
    {
        return $this->render('group/read.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Редактировать группу
     *
     * @Route("/update/{id}", name="group_update")
     * @ParamConverter("entity", class="AppBundle:Group")
     *
     * @param Request $request
     * @param Group $entity
     *
     * @return Response
     */
    public function updateGroup(Request $request, Group $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createUpdateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $message = $this->prepareEventMessage($entity, 'group.message.updated');
            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('group_update', ['id' => $entity->getId()]);
        }

        return $this->render('group/update.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * Форма редактирования группы
     *
     * @param Group $entity
     *
     * @return Form
     */
    private function createUpdateForm(Group $entity)
    {
        $form = $this->createForm(GroupType::class, $entity, [
            'action' => $this->generateUrl('group_update', ['id' => $entity->getId()]),
            'method' => Request::METHOD_POST,
            'package_manager' => $this->get('app.analysis.packageManager'),
        ]);

        return $form;
    }

    /**
     * Удалить группу
     *
     * @Route("/delete/{id}", name="group_delete")
     * @ParamConverter("entity", class="AppBundle:Group")
     *
     * @param Request $request
     * @param Group $entity
     *
     * @return Response
     */
    public function deleteGroup(Request $request, Group $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createDeleteForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = $this->prepareEventMessage($entity, 'group.message.deleted');

            $em->remove($entity);
            $em->flush();

            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('group');
        }

        return $this->render('group/delete.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * Форма удаления группы
     *
     * @param Group $entity
     *
     * @return Form
     */
    private function createDeleteForm(Group $entity)
    {
        return $this->createFormBuilder()
                ->setAction($this->generateUrl('group_delete', ['id' => $entity->getId()]))
                ->setMethod(Request::METHOD_DELETE)
                ->getForm();
    }

    /**
     * Подготовить сообщение о событии
     *
     * @param Group $group
     * @param string $message
     */
    protected function prepareEventMessage(Group $group, string $message)
    {
        return $this->get('translator')->trans($message, ['%id%' => $group->getId()]);
    }

    /**
     * Логировать событие
     *
     * @param Group $group
     * @param string $message
     */
    protected function logEvent(Group $group, string $message)
    {
        $this->get('logger')->notice($message, $this->getEntityContext($group));
    }

    /**
     * Контекст для логирования
     *
     * @param Group $entity
     *
     * @return array
     */
    private function getEntityContext(Group $entity): array
    {
        return [
            'group_id' => $entity->getId(),
            'group_title' => $entity->getTitle(),
            'group_roles' => $entity->getRoles(),
            'group_analysis' => $entity->getAnalysisNames(),
        ];
    }
}
