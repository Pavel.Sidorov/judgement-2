<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\LogFilterType;
use AppBundle\Entity\Log;

/**
 * Журнал
 *
 * @Route("/log")
 *
 * @Security("has_role('ROLE_LOG')")
 */
class LogController extends Controller
{
    const LOGS_PER_PAGE = 20;

    /**
     * Список записей
     *
     * @Route("/", name="log")
     * @Security("has_role('ROLE_LOG')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Log');

        // Фильтры
        $form = $this->createFilterForm();
        $form->handleRequest($request);
        $filters = $form->getData();

        // Параметры пагинации
        $count = $repo->countLogs(
            $filters['dateFrom'],
            $filters['dateTo'],
            $filters['username']
        );
        $pagesCount = (int) ceil($count / self::LOGS_PER_PAGE);
        $page = (int) $request->query->get('page');
        if ($page < 1 || $page > $pagesCount) {
            $page = 1;
        }
        $offset = ($page - 1) * self::LOGS_PER_PAGE;

        // Выборка записей с учетом пагинации
        $entities = $repo->getLogs(
            $filters['dateFrom'],
            $filters['dateTo'],
            $filters['username'],
            $offset,
            self::LOGS_PER_PAGE
        );

        return $this->render('log/index.html.twig', [
            'form' => $form->createView(),
            'entities' => $entities,
            'page' => $page,
            'pagesCount' => $pagesCount,
        ]);
    }

    /**
     * Форма фильтрации списка заданий
     *
     * @return Form
     */
    private function createFilterForm()
    {
        $form = $this->createForm(LogFilterType::class, null, [
            'action' => $this->generateUrl('log'),
            'method' => Request::METHOD_GET,
            'csrf_protection' => false,
        ]);

        return $form;
    }

    /**
     * Прочитать информацию о событии
     *
     * @Route("/read/{id}", name="log_read")
     * @ParamConverter("entity", class="AppBundle:Log")
     *
     * @param Request $request
     * @param Log $entity
     *
     * @return Response
     */
    public function readLog(Request $request, Log $entity)
    {
        return $this->render('log/read.html.twig', [
            'entity' => $entity,
        ]);
    }
}
