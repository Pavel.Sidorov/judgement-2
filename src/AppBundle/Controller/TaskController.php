<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\User\UserInterface;
use AppBundle\Security\UserInterface as AppUser;
use AppBundle\Analysis\Task\TaskInterface;
use AppBundle\Entity\Task;
use AppBundle\Form\TaskFilterType;
use AppBundle\Form\TaskCreateType;
use AppBundle\Form\TaskUpdateType;
use AppBundle\Form\TaskForkType;

/**
 * Задания на расчет
 *
 * @Route("/task")
 *
 * @Security("has_role('ROLE_TASK')")
 */
class TaskController extends Controller
{
    const TASKS_PER_PAGE = 10;

    /**
     * Быстрый переход либо к поиску заявок, либо к созданию заявки
     *
     * @Route("/find-or-create/{scoringId}", name="task_find_or_create")
     * @Security("has_role('ROLE_TASK')")
     *
     * @param int $scoringId
     *
     * @return Response
     */
    public function findOrCreateAction(int $scoringId)
    {
        $manager = $this->get('app.analysis.taskManager');
        $count = $manager->countRootTasks(null, null, $scoringId);

        if ($count > 0) {
            $filters = [
                'analysisName' => '',
                'analysisType' => '',
                'scoringId' => $scoringId,
                'clientName' => '',
            ];

            return $this->redirectToRoute('task', ['task_filter' => $filters]);
        } else {
            return $this->redirectToRoute('task_create_prefill', ['scoringId' => $scoringId]);
        }
    }

    /**
     * Список заданий
     *
     * @Route("/", name="task")
     * @Security("has_role('ROLE_TASK')")
     *
     * @param Request $request
     * @param UserInterface $currentUser Текущий пользователь
     *
     * @return Response
     */
    public function indexAction(Request $request, UserInterface $currentUser)
    {
        if (!$currentUser instanceof AppUser) {
            throw $this->createAccessDeniedException('Неподдерживаемый тип пользователя');
        }

        $manager = $this->get('app.analysis.taskManager');

        // Фильтры
        $form = $this->createFilterForm($currentUser);
        $form->handleRequest($request);
        $filters = $form->getData();

        // Параметры пагинации
        $count = $manager->countRootTasks(
            $filters['analysisName'],
            $filters['analysisType'],
            $filters['scoringId'],
            $filters['clientName']
        );
        $pagesCount = (int) ceil($count / self::TASKS_PER_PAGE);
        $page = (int) $request->query->get('page');
        if ($page < 1 || $page > $pagesCount) {
            $page = 1;
        }
        $offset = ($page - 1) * self::TASKS_PER_PAGE;

        // Выборка записей с учетом пагинации
        $entities = $manager->getRootTasks(
            $filters['analysisName'],
            $filters['analysisType'],
            $filters['scoringId'],
            $filters['clientName'],
            $offset,
            self::TASKS_PER_PAGE
        );

        return $this->render('task/index.html.twig', [
            'form' => $form->createView(),
            'entities' => $entities,
            'page' => $page,
            'pagesCount' => $pagesCount,
        ]);
    }

    /**
     * Форма фильтрации списка заданий
     *
     * @param AppUser $user
     *
     * @return Form
     */
    private function createFilterForm(AppUser $user)
    {
        $form = $this->createForm(TaskFilterType::class, null, [
            'action' => $this->generateUrl('task'),
            'method' => Request::METHOD_GET,
            'csrf_protection' => false,
            'user' => $user,
        ]);

        return $form;
    }

    /**
     * Создать задание на расчет
     *
     * @Route("/create", name="task_create")
     * @Route("/create/{scoringId}", name="task_create_prefill")
     * @Security("has_role('ROLE_TASK_CREATE')")
     *
     * @param Request $request
     * @param UserInterface $currentUser Текущий пользователь
     *
     * @return Response
     */
    public function createAction(Request $request, UserInterface $currentUser, int $scoringId = null)
    {
        if (!$currentUser instanceof AppUser) {
            throw $this->createAccessDeniedException('Неподдерживаемый тип пользователя');
        }

        $manager = $this->get('app.analysis.taskManager');
        $entity = new Task();
        if ($scoringId !== null) {
            $entity->setScoringId((int) $scoringId);
        }

        $form = $this->createCreateForm($entity, $currentUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->createTask($entity);

            $message = $this->prepareEventMessage($entity, 'task.message.created');
            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('task_read', ['id' => $entity->getId()]);
        }

        return $this->render('task/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Форма создания задания на расчет
     *
     * @param TaskInterface $task
     * @param AppUser $user
     *
     * @return Form
     */
    private function createCreateForm(TaskInterface $task, AppUser $user)
    {
        $form = $this->createForm(TaskCreateType::class, $task, [
            'action' => $this->generateUrl('task_create'),
            'method' => Request::METHOD_POST,
            'user' => $user,
            'package_manager' => $this->get('app.analysis.packageManager'),
            'translator' => $this->get('translator'),
        ]);

        return $form;
    }

    /**
     * Прочитать информацию о задании
     *
     * @Route("/read/{id}", name="task_read")
     * @Security("has_role('ROLE_TASK_READ')")
     *
     * @param Request $request
     * @param string $id
     *
     * @return Response
     */
    public function readAction(Request $request, string $id)
    {
        $manager = $this->get('app.analysis.taskManager');
        $entity = $manager->readTask($id);

        if (!$entity) {
            throw $this->createNotFoundException('Task not found');
        }

        return $this->render('task/read.html.twig', [
            'entity' => $entity,
            'scoringUrl' => str_replace('%ID', $entity->getScoringId(), $this->getParameter('scoring_app_url')),
        ]);
    }

    /**
     * Редактировать задание
     *
     * @Route("/update/{id}", name="task_update")
     * @Security("has_role('ROLE_TASK_UPDATE')")
     *
     * @param Request $request
     * @param string $id
     *
     * @return Response
     */
    public function updateAction(Request $request, string $id)
    {
        $manager = $this->get('app.analysis.taskManager');
        $entity = $manager->readTask($id);

        if (!$entity) {
            throw $this->createNotFoundException('Task not found');
        }

        $form = $this->createUpdateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->updateTask($entity);

            $message = $this->prepareEventMessage($entity, 'task.message.updated');
            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);
        }

        return $this->render('task/update.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * Форма редактирования задания на расчет
     *
     * @param TaskInterface $entity
     *
     * @return Form
     */
    private function createUpdateForm(TaskInterface $task)
    {
        $form = $this->createForm(TaskUpdateType::class, $task, [
            'action' => $this->generateUrl('task_update', ['id' => $task->getId()]),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Ввод исходных данных
     *
     * @Route("/input/{id}", name="task_input")
     * @Security("has_role('ROLE_TASK_INPUT')")
     *
     * @param Request $request
     * @param string $id
     *
     * @return Response
     */
    public function inputAction(Request $request, string $id)
    {
        $manager = $this->get('app.analysis.taskManager');
        $entity = $manager->readTask($id);

        if (!$entity) {
            throw $this->createNotFoundException('Task not found');
        }

        $inputForm = $manager->getInputForm(
            $entity,
            $this->generateUrl('task_initial', ['id' => $entity->getId()])
        );

        return $this->render('task/input.html.twig', [
            'entity' => $entity,
            'inputForm' => $inputForm,
        ]);
    }

    /**
     * Редактировать исходные данные
     *
     * @Route("/initial/{id}", name="task_initial")
     * @Security("has_role('ROLE_TASK_INPUT')")
     *
     * @param Request $request
     * @param string $id
     *
     * @return JsonResponse
     */
    public function initialAction(Request $request, string $id)
    {
        $manager = $this->get('app.analysis.taskManager');
        $entity = $manager->readTask($id);

        if (!$entity) {
            throw $this->createNotFoundException('Task not found');
        }

        $data = json_decode($request->getContent(), true);
        $manager->updateInitialData($entity, $data);

        $message = $this->prepareEventMessage($entity, 'task.message.inputData');
        $this->logEvent($entity, $message);

        return new JsonResponse([
            'status' => true,
            'message' => $message,
        ]);
    }

    /**
     * Завершить задание (выполнить расчет)
     *
     * @Route("/complete/{id}", name="task_complete")
     * @Security("is_granted('ROLE_TASK_COMPLETE')")
     *
     * @param Request $request
     * @param string $id
     *
     * @return Response
     */
    public function completeAction(Request $request, string $id)
    {
        $manager = $this->get('app.analysis.taskManager');
        $entity = $manager->readTask($id);

        if (!$entity) {
            throw $this->createNotFoundException('Task not found');
        }

        $form = $this->createCompleteForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->completeTask($entity);

            $message = $this->prepareEventMessage($entity, 'task.message.completed');
            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('task_read', ['id' => $entity->getId()]);
        }

        return $this->render('task/complete.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * Форма завершения задания
     *
     * @param TaskInterface $entity
     *
     * @return Form
     */
    private function createCompleteForm(TaskInterface $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('task_complete', ['id' => $entity->getId()]))
            ->setMethod(Request::METHOD_POST)
            ->getForm();
    }

    /**
     * Построить отчет
     *
     * @Route("/report/{id}", name="task_report")
     * @Security("is_granted('ROLE_TASK_REPORT')")
     *
     * @param Request $request
     * @param string $id
     *
     * @return Response
     */
    public function reportAction(Request $request, string $id)
    {
        $manager = $this->get('app.analysis.taskManager');
        $entity = $manager->readTask($id);

        if (!$entity) {
            throw $this->createNotFoundException('Task not found');
        }

        $report = $manager->getReport($entity);

        $message = $this->prepareEventMessage($entity, 'task.message.reported');
        $this->logEvent($entity, $message);

        $filename = implode('', [
            $entity->getId(),
            '_',
            $entity->getCompletedAt()->format('Ymd'),
            '.',
            $report->getFileExtension(),
        ]);

        $response = new Response($report->getContent());

        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);

        $response->headers->set('Content-Type', $report->getMimeType().'; charset=UTF-8');
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * Создать дочернее задание на расчет
     *
     * @Route("/fork/{id}", name="task_fork")
     * @Security("is_granted('ROLE_TASK_FORK')")
     *
     * @param Request $request
     * @param string $id
     *
     * @return Response
     */
    public function forkAction(Request $request, string $id)
    {
        $manager = $this->get('app.analysis.taskManager');
        $entity = $manager->readTask($id);

        if (!$entity) {
            throw $this->createNotFoundException('Task not found');
        }

        $fork = $manager->prepareForkTask($entity);

        $form = $this->createForkForm($entity, $fork);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->forkTask($fork);

            $message = $this->prepareEventMessage($fork, 'task.message.forked');
            $this->logEvent($fork, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('task_read', ['id' => $fork->getId()]);
        }

        return $this->render('task/fork.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * Форма создания дочернего задания на расчет
     *
     * @param TaskInterface $original
     * @param TaskInterface $fork
     *
     * @return Form
     */
    private function createForkForm(TaskInterface $original, TaskInterface $fork)
    {
        $form = $this->createForm(TaskForkType::class, $fork, [
            'action' => $this->generateUrl('task_fork', ['id' => $original->getId()]),
            'method' => Request::METHOD_POST,
            'package_manager' => $this->get('app.analysis.packageManager'),
            'translator' => $this->get('translator'),
        ]);

        return $form;
    }

    /**
     * Сделать задание итоговым
     *
     * @Route("/summarize/{id}", name="task_summarize")
     * @Security("is_granted('ROLE_TASK_SUMMARIZE')")
     *
     * @param Request $request
     * @param string $id
     *
     * @return Response
     */
    public function summarizeAction(Request $request, string $id)
    {
        $manager = $this->get('app.analysis.taskManager');
        $entity = $manager->readTask($id);

        if (!$entity) {
            throw $this->createNotFoundException('Task not found');
        }

        $form = $this->createSummarizeForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->summarizeTask($entity);

            $message = $this->prepareEventMessage($entity, 'task.message.summarized');
            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('task_read', ['id' => $entity->getId()]);
        }

        return $this->render('task/summarize.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * Форма для пометки задания итоговым
     *
     * @param TaskInterface $entity
     *
     * @return Form
     */
    private function createSummarizeForm(TaskInterface $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('task_summarize', ['id' => $entity->getId()]))
            ->setMethod(Request::METHOD_POST)
            ->getForm();
    }

    /**
     * Подготовить сообщение о событии
     *
     * @param TaskInterface $task
     * @param string $message
     */
    protected function prepareEventMessage(TaskInterface $task, string $message)
    {
        return $this->get('translator')->trans($message, ['%id%' => $task->getId()]);
    }

    /**
     * Логировать событие
     *
     * @param TaskInterface $task
     * @param string $message
     */
    protected function logEvent(TaskInterface $task, string $message)
    {
        $this->get('logger')->notice($message, $this->getEntityContext($task));
    }

    /**
     * Контекст для логирования
     *
     * @param TaskInterface $entity
     *
     * @return array
     */
    private function getEntityContext(TaskInterface $entity): array
    {
        return [
            'task_id' => $entity->getId(),
            'task_parent_id' => $entity->getParent() ? $entity->getParent()->getId() : null,
            'task_analysis_scoring_id' => $entity->getScoringId(),
            'task_analysis_name' => $entity->getAnalysisName(),
            'task_analysis_type' => $entity->getAnalysisType(),
            'task_analysis_version' => $entity->getAnalysisVersion(),
            'task_completed' => $entity->isCompleted(),
            'task_final' => $entity->isFinal(),
        ];
    }
}
