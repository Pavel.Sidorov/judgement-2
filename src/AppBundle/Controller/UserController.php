<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Form\UserFilterType;

/**
 * Пользователи
 *
 * @Route("/user")
 *
 * @Security("has_role('ROLE_USER')")
 */
class UserController extends Controller
{
    const USERS_PER_PAGE = 20;

    /**
     * Список пользователей
     *
     * @Route("/", name="user")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // Фильтры
        $form = $this->createFilterForm();
        $form->handleRequest($request);
        $filters = $form->getData();

        // Выборки с учетом фильтров
        $repo = $this->getDoctrine()->getRepository('AppBundle:User');

        // Параметры пагинации
        $count = $repo->countUsers(
            $filters['enabled'],
            $filters['username'],
            $filters['name'],
            $filters['email']
        );
        $pagesCount = (int) ceil($count / self::USERS_PER_PAGE);
        $page = (int) $request->query->get('page');
        if ($page < 1 || $page > $pagesCount) {
            $page = 1;
        }
        $offset = ($page - 1) * self::USERS_PER_PAGE;

        // Выборка записей с учетом пагинации
        $entities = $repo->getUsers(
            $filters['enabled'],
            $filters['username'],
            $filters['name'],
            $filters['email'],
            $offset,
            self::USERS_PER_PAGE
        );

        return $this->render('user/index.html.twig', [
            'form' => $form->createView(),
            'entities' => $entities,
            'page' => $page,
            'pagesCount' => $pagesCount,
        ]);
    }

    /**
     * Форма фильтрации списка пользователей
     *
     * @return Form
     */
    private function createFilterForm()
    {
        $form = $this->createForm(UserFilterType::class, null, [
            'action' => $this->generateUrl('user'),
            'method' => Request::METHOD_GET,
            'csrf_protection' => false,
        ]);

        return $form;
    }

    /**
     * Создать пользователя
     *
     * @Route("/create", name="user_create")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createUser(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $message = $this->prepareEventMessage($entity, 'user.message.created');
            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('user_read', ['id' => $entity->getId()]);
        }

        return $this->render('user/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Форма создания пользователя
     *
     * @param User $entity
     *
     * @return Form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(UserType::class, $entity, [
            'action' => $this->generateUrl('user_create'),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Прочитать информацию о пользователе
     *
     * @Route("/read/{id}", name="user_read")
     * @ParamConverter("entity", class="AppBundle:User")
     *
     * @param Request $request
     * @param User $entity
     *
     * @return Response
     */
    public function readUser(Request $request, User $entity)
    {
        return $this->render('user/read.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Редактировать пользователя
     *
     * @Route("/update/{id}", name="user_update")
     * @ParamConverter("entity", class="AppBundle:User")
     *
     * @param Request $request
     * @param User $entity
     * @param UserInterface $currentUser
     *
     * @return Response
     */
    public function updateUser(Request $request, User $entity, UserInterface $currentUser)
    {
        // Нельзя отредактировать свою учетку
        if ($currentUser->getId() === $entity->getId()) {
            $message = $this->prepareEventMessage($entity, 'user.message.selfEdit');
            $this->addFlash('warning', $message);

            return $this->redirectToRoute('user_read', ['id' => $entity->getId()]);
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createUpdateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $message = $this->prepareEventMessage($entity, 'user.message.updated');
            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('user_update', ['id' => $entity->getId()]);
        }

        return $this->render('user/update.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * Форма редактирования пользователя
     *
     * @param User $entity
     *
     * @return Form
     */
    private function createUpdateForm(User $entity)
    {
        $form = $this->createForm(UserType::class, $entity, [
            'action' => $this->generateUrl('user_update', ['id' => $entity->getId()]),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Удалить пользователя
     *
     * @Route("/delete/{id}", name="user_delete")
     * @ParamConverter("entity", class="AppBundle:User")
     *
     * @param Request $request
     * @param User $entity
     * @param UserInterface $currentUser
     *
     * @return Response
     */
    public function deleteUser(Request $request, User $entity, UserInterface $currentUser)
    {
        // Нельзя удалить свою учетку
        if ($currentUser->getId() === $entity->getId()) {
            $message = $this->prepareEventMessage($entity, 'user.message.selfDelete');
            $this->addFlash('warning', $message);

            return $this->redirectToRoute('user_read', ['id' => $entity->getId()]);
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createDeleteForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = $this->prepareEventMessage($entity, 'user.message.deleted');

            $em->remove($entity);
            $em->flush();

            $this->logEvent($entity, $message);
            $this->addFlash('notice', $message);

            return $this->redirectToRoute('user');
        }

        return $this->render('user/delete.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * Форма удаления пользователя
     *
     * @param User $entity
     *
     * @return Form
     */
    private function createDeleteForm(User $entity)
    {
        return $this->createFormBuilder()
                ->setAction($this->generateUrl('user_delete', ['id' => $entity->getId()]))
                ->setMethod(Request::METHOD_DELETE)
                ->getForm();
    }

    /**
     * Подготовить сообщение о событии
     *
     * @param User $task
     * @param string $message
     */
    protected function prepareEventMessage(User $task, string $message)
    {
        return $this->get('translator')->trans($message, ['%id%' => $task->getId()]);
    }

    /**
     * Логировать событие
     *
     * @param User $task
     * @param string $message
     */
    protected function logEvent(User $task, string $message)
    {
        $this->get('logger')->notice($message, $this->getEntityContext($task));
    }

    /**
     * Контекст для логирования
     *
     * @param User $entity
     *
     * @return array
     */
    private function getEntityContext(User $entity): array
    {
        return [
            'user_id' => $entity->getId(),
            'user_username' => $entity->getUsername(),
            'user_enabled' => $entity->getEnabled(),
            'user_groups' => array_map(function($val){
                return [
                    'group_id' => $val->getId(),
                    'group_title' => $val->getTitle(),
                ];
            }, $entity->getGroups()->toArray()),
        ];
    }
}
