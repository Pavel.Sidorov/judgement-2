<?php

namespace AppBundle\Doctrine\Types;

use Doctrine\DBAL\Types\JsonArrayType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\InitialData;

/**
 * Хранение InitialData в БД как JSON
 */
class InitialDataType extends JsonArrayType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'initial_data';
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        if (!$value instanceof InitialDataInterface) {
            throw new \LogicException('Неожиданный тип данных, ожидался InitialDataInterface');
        }

        return json_encode($value->getData());
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $output = new InitialData();

        if ($value === null || $value === '') {
            return $output;
        }

        $value = (is_resource($value)) ? stream_get_contents($value) : $value;

        $output->setData(json_decode($value, true));

        return $output;
    }
}