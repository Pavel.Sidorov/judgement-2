<?php

namespace AppBundle\Doctrine\Types;

use Doctrine\DBAL\Types\JsonArrayType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Data\ResultData;

/**
 * Хранение ResultData в БД как JSON
 */
class ResultDataType extends JsonArrayType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'result_data';
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        if (!$value instanceof ResultDataInterface) {
            throw new \LogicException('Неожиданный тип данных, ожидался ResultDataInterface');
        }

        return json_encode($value->getData());
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $output = new ResultData();

        if ($value === null || $value === '') {
            return $output;
        }

        $value = (is_resource($value)) ? stream_get_contents($value) : $value;

        $output->setData(json_decode($value, true));

        return $output;
    }
}