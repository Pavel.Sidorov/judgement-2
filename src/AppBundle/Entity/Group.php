<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Security\GroupInterface;

/**
 * Группа пользователей
 *
 * @ORM\Entity
 * @ORM\Table(name="groups")
 * @ORM\HasLifecycleCallbacks
 *
 * @UniqueEntity(fields="title")
 */
class Group implements GroupInterface
{
    /**
     * Идентификатор записи
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * Заголовок
     *
     * @ORM\Column(type="string", name="title", length=100, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min=2, max=100)
     *
     * @var string
     */
    private $title;

    /**
     * Описание
     *
     * @ORM\Column(type="string", name="description", length=1000, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max=1000)
     *
     * @var string
     */
    private $description;

    /**
     * Дата создания
     *
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Дата последнего редактирования
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     *
     * @Assert\DateTime()
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Список ролей
     *
     * @ORM\Column(type="json_array", name="roles")
     *
     * @Assert\Type("array")
     *
     * @var array
     */
    private $roles;

    /**
     * Список разрешенных имен анализов
     *
     * @ORM\Column(type="json_array", name="analysis_names")
     *
     * @Assert\Type("array")
     *
     * @var array
     */
    private $analysisNames;

    /**
     * Пользователи в группе
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groups")
     * @ORM\OrderBy({"name" = "ASC", "username" = "ASC"})
     * 
     * @var Collection
     */
    private $users;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->roles = [];
        $this->analysisNames = [];
        $this->users = new ArrayCollection();
    }

    /**
     * Получить идентификатор
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Получить заголовок
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Установить заголовок
     *
     * @return string
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * Получить описание
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Установить описание
     *
     * @return string
     */
    public function setDescription(string $description = null)
    {
        $this->description = $description;
    }

    /**
     * Получить дату создания
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Установить дату создания
     *
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Получить дату обновления
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Установить дату обновления
     *
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Получить список ролей
     *
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * Установить список ролей
     *
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Получить список разрешенных имен анализов
     */
    public function getAnalysisNames(): array
    {
        return $this->analysisNames;
    }

    /**
     * Установить список продуктов
     *
     * @param array $analysisNames
     */
    public function setAnalysisNames(array $analysisNames)
    {
        $this->analysisNames = $analysisNames;
    }

    /**
     * Добавить пользователя
     *
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;
    }

    /**
     * Удалить пользователя
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Получить пользователей
     *
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * Обновление меток времени
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps()
    {
        if ($this->getId()) {
            $this->updatedAt = new \DateTime();
        } else {
            $this->createdAt = new \DateTime();
        }
    }
}
