<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Запись в журнале
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LogRepository")
 * @ORM\Table(name="logs")
 * @ORM\HasLifecycleCallbacks
 */
class Log
{
    /**
     * Идентификатор записи
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * Канал
     *
     * @ORM\Column(type="text", name="channel", length=50, nullable=true)
     *
     * @var string
     */
    private $channel;

    /**
     * Уровень важности
     *
     * @ORM\Column(type="integer", name="level", nullable=true)
     *
     * @var int
     */
    private $level;

    /**
     * Уровень важности (название)
     *
     * @ORM\Column(type="string", name="level_name", length=50, nullable=true)
     *
     * @var string
     */
    private $levelName;

    /**
     * Сообщение
     *
     * @ORM\Column(type="string", name="message", length=2000, nullable=true)
     *
     * @var string
     */
    private $message;

    /**
     * Дата записи
     *
     * @ORM\Column(type="datetime", name="date", nullable=true)
     *
     * @var \DateTime
     */
    private $date;

    /**
     * Контекст
     *
     * @ORM\Column(type="json_array", name="context", nullable=true)
     *
     * @var array
     */
    private $context;

    /**
     * Доп. информация
     *
     * @ORM\Column(type="json_array", name="extra", nullable=true)
     *
     * @var array
     */
    private $extra;

    /**
     * Имя пользователя
     *
     * @ORM\Column(type="text", name="username", length=200, nullable=true)
     *
     * @var string
     */
    private $username;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Получить идентификатор
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Получить канал
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Установить канал
     *
     * @param string $channel
     */
    public function setChannel(string $channel = null)
    {
        $this->channel = $channel;
    }

    /**
     * Получить уровень важности
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Установить уровень важности
     *
     * @param int $level
     */
    public function setLevel(int $level = null)
    {
        $this->level = $level;
    }

    /**
     * Получить уровень важности (название)
     *
     * @return string
     */
    public function getLevelName()
    {
        return $this->levelName;
    }

    /**
     * Установить уровень важности (название)
     *
     * @param string $levelName
     */
    public function setLevelName(string $levelName = null)
    {
        $this->levelName = $levelName;
    }

    /**
     * Получить сообщение
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Установить сообщение
     *
     * @param string $message
     */
    public function setMessage(string $message = null)
    {
        $this->message = $message;
    }

    /**
     * Получить дату записи
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Установить дату записи
     *
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Получить контекст
     *
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Установить контекст
     *
     * @param array $context
     */
    public function setContext(array $context = null)
    {
        $this->context = $context;
    }

    /**
     * Получить доп. информацию
     *
     * @return array
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Установить доп. информацию
     *
     * @param array $extra
     */
    public function setExtra(array $extra = null)
    {
        $this->extra = $extra;
    }

    /**
     * Получить имя пользователя
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Установить имя пользователя
     *
     * @param string $username
     */
    public function setUsername($username = null)
    {
        $this->username = $username;
    }
}
