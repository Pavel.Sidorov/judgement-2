<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Analysis\Task\TaskInterface;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\InitialData;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Data\ResultData;
use AppBundle\Validator\Constraints\AnalysisForScoringApp;
use AppBundle\Validator\Constraints\TasksPerScoringApp;

/**
 * Задание на расчет
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
 * @ORM\Table(name="tasks")
 * @ORM\HasLifecycleCallbacks
 *
 * @AnalysisForScoringApp
 * @TasksPerScoringApp
 */
class Task implements TaskInterface
{
    /**
     * Идентификатор записи
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * Описание
     *
     * @ORM\Column(type="string", name="description", length=2000, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max=2000)
     *
     * @var string
     */
    private $description;

    /**
     * Родительская запись
     *
     * Используется для сохранения истории расчетов.
     *
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     *
     * @var TaskInterafce
     */
    private $parent;

    /**
     * Дочерние записи
     *
     * Используется для сохранения истории расчетов.
     *
     * @ORM\OneToMany(targetEntity="Task", mappedBy="parent")
     * @ORM\OrderBy({"createdAt": "ASC"})
     *
     * @var Collection
     */
    private $children;

    /**
     * Название методики анализа
     *
     * @ORM\Column(type="string", name="analysis_name", length=20)
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max=20)
     *
     * @var string
     */
    private $analysisName;

    /**
     * Тип анализа: первичный, вторичный
     *
     * @ORM\Column(type="string", name="analysis_type", length=20)
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max=20)
     *
     * @var string
     */
    private $analysisType;

    /**
     * Версия методики анализа
     *
     * @ORM\Column(type="integer", name="analysis_version")
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     *
     * @var int
     */
    private $analysisVersion;

    /**
     * ID привязанной скоринговой заявки
     *
     * @ORM\Column(type="integer", name="scoring_id")
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     *
     * @var integer
     */
    private $scoringId;

    /**
     * Статус выполнения задания
     *
     * @ORM\Column(type="boolean", name="completed")
     *
     * @Assert\Type("boolean")
     *
     * @var bool
     */
    private $completed;

    /**
     * Исходные данные для анализа
     *
     * @ORM\Column(type="initial_data", name="initial_data", nullable=true)
     *
     * @Assert\Type("AppBundle\Analysis\Data\InitialDataInterface")
     *
     * @var InitialDataInterface
     */
    private $initialData;

    /**
     * Результаты расчета
     *
     * @ORM\Column(type="result_data", name="result_data", nullable=true)
     *
     * @Assert\Type("AppBundle\Analysis\Data\ResultDataInterface")
     *
     * @var ResultDataInterface
     */
    private $resultData;

    /**
     * Дата создания задания
     *
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Дата последнего редактирования задания
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     *
     * @Assert\DateTime()
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Дата выполнения задания
     *
     * @ORM\Column(type="datetime", name="completed_at", nullable=true)
     *
     * @Assert\DateTime()
     *
     * @var \DateTime
     */
    private $completedAt;

    /**
     * Окончательное
     *
     * @ORM\Column(type="boolean", name="final")
     *
     * @Assert\Type("boolean")
     *
     * @var bool
     */
    private $final;

    /**
     * Имя клиента
     *
     * @ORM\Column(type="string", name="clientName", length=200, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max=200)
     *
     * @var string
     */
    private $clientName;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->initialData = new InitialData();
        $this->resultData = new ResultData();
        $this->createdAt = new \DateTime();
        $this->completed = false;
        $this->final = false;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * {@inheritDoc}
     */
    public function setDescription(string $description = null)
    {
        $this->description = $description;
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * {@inheritDoc}
     */
    public function setParent(TaskInterface $parent = null)
    {
        $this->parent = $parent;
    }

    /**
     * {@inheritDoc}
     */
    public function getChildren(): array
    {
        return $this->children->toArray();
    }

    /**
     * {@inheritDoc}
     */
    public function getAnalysisName()
    {
        return $this->analysisName;
    }

    /**
     * {@inheritDoc}
     */
    public function setAnalysisName(string $analysisName)
    {
        $this->analysisName = $analysisName;
    }

    /**
     * {@inheritDoc}
     */
    public function getAnalysisType()
    {
        return $this->analysisType;
    }

    /**
     * {@inheritDoc}
     */
    public function setAnalysisType(string $analysisType)
    {
        $this->analysisType = $analysisType;
    }

    /**
     * {@inheritDoc}
     */
    public function getAnalysisVersion()
    {
        return $this->analysisVersion;
    }

    /**
     * {@inheritDoc}
     */
    public function setAnalysisVersion(int $analysisVersion)
    {
        $this->analysisVersion = $analysisVersion;
    }

    /**
     * {@inheritDoc}
     */
    public function getScoringId()
    {
        return $this->scoringId;
    }

    /**
     * {@inheritDoc}
     */
    public function setScoringId(int $scoringId)
    {
        $this->scoringId = $scoringId;
    }

    /**
     * {@inheritDoc}
     */
    public function getInitialData(): InitialDataInterface
    {
        return $this->initialData;
    }

    /**
     * {@inheritDoc}
     */
    public function setInitialData(InitialDataInterface $initialData)
    {
        $this->initialData = $initialData;
    }

    /**
     * {@inheritDoc}
     */
    public function getResultData(): ResultDataInterface
    {
        return $this->resultData;
    }

    /**
     * {@inheritDoc}
     */
    public function setResultData(ResultDataInterface $resultData)
    {
        $this->resultData = $resultData;
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritDoc}
     */
    public function markUpdated()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * {@inheritDoc}
     */
    public function markCompleted()
    {
        $this->completed = true;
        $this->completedAt = new \DateTime();
    }

    /**
     * {@inheritDoc}
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * Получить дату завершения
     *
     * @return \DateTime
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }

    /**
     * {@inheritDoc}
     */
    public function markFinal()
    {
        $this->final = true;
    }

    /**
     * {@inheritDoc}
     */
    public function unmarkFinal()
    {
        $this->final = false;
    }

    /**
     * {@inheritDoc}
     */
    public function isFinal(): bool
    {
        return $this->final;
    }

    /**
     * Форсируем обновление InitialData и ResultData
     *
     * @ORM\PreUpdate
     */
    public function forceUpdateData()
    {
        if ($this->initialData) {
            $this->initialData = clone $this->initialData;
        }

        if ($this->resultData) {
            $this->resultData = clone $this->resultData;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * {@inheritDoc}
     */
    public function setClientName(string $clientName = null)
    {
        $this->clientName = $clientName;
    }
}
