<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Security\GroupInterface;
use AppBundle\Security\UserInterface;

/**
 * Пользователь
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @UniqueEntity(fields="username")
 */
class User implements UserInterface, \Serializable
{
    /**
     * Идентификатор записи
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * Имя пользователя
     *
     * @ORM\Column(name="username", type="string", length=25, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min=2, max=25)
     *
     * @var string
     */
    private $username;

    /**
     * ФИО
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max=255)
     *
     * @var string
     */
    private $name;

    /**
     * Email
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max=100)
     *
     * @var string
     */
    private $email;

    /**
     * Телефон
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max=100)
     *
     * @var string
     */
    private $phone;

    /**
     * Подразделение компании
     *
     * @ORM\Column(name="department", type="string", length=255, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max=255)
     *
     * @var string
     */
    private $department;

    /**
     * Статус активности записи
     *
     * @ORM\Column(name="enabled", type="boolean")
     *
     * @Assert\Type("bool")
     *
     * @var bool
     */
    private $enabled;

    /**
     * Дата создания
     *
     * @ORM\Column(name="created_at", type="datetime", name="created_at")
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Дата последнего редактирования
     *
     * @ORM\Column(name="updated_at", type="datetime", name="updated_at", nullable=true)
     *
     * @Assert\DateTime()
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Группы пользователя
     *
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users")
     * @ORM\JoinTable(name="users_groups")
     *
     * @var Collection
     */
    private $groups;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->enabled = true;
        $this->groups = new ArrayCollection();
    }

    /**
     * Получить идентификатор
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Установить имя пользователя
     *
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * Получить ФИО
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Установить ФИО
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Получить email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Установить email
     *
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * Получить телефон
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Установить телефон
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Получить подразделение
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Установить подразделение
     *
     * @param string $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * Получить статус активности записи
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * {@inheritDoc}
     */
    public function isEnabled()
    {
        return $this->getEnabled();
    }

    /**
     * Установить статус активности записи
     *
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = (boolean) $enabled;
    }

    /**
     * Получить дату создания
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Установить дату создания
     *
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Получить дату обновления
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Установить дату обновления
     *
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Добавить группу
     *
     * @param GroupInterface $group
     */
    public function addGroup(GroupInterface $group)
    {
        $this->groups[] = $group;
    }

    /**
     * Удалить группу
     *
     * @param GroupInterface $group
     */
    public function removeGroup(GroupInterface $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * {@inheritDoc}
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles(): array
    {
        $tmp = ['ROLE_AUTH']; // Минимальная роль для авторизации

        foreach ($this->getGroups() as $group) {
            $tmp = array_merge($tmp, $group->getRoles());
        }

        return array_unique($tmp);
    }

    /**
     * {@inheritDoc}
     */
    public function getAnalysisNames(): array
    {
        $tmp = [];

        foreach ($this->getGroups() as $group) {
            $tmp = array_merge($tmp, $group->getAnalysisNames());
        }

        return array_unique($tmp);
    }

    /**
     * {@inheritDoc}
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->name,
            $this->email,
            $this->phone,
            $this->department,
            $this->enabled,
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->name,
            $this->email,
            $this->phone,
            $this->department,
            $this->enabled,
        ) = unserialize($serialized);
    }
}
