<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Analysis\Package\PackageManagerInterface;
use AppBundle\Entity\Group;
use AppBundle\Security\Roles;

class GroupType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $manager = $options['package_manager'];

        $builder->add('title', TextType::class, [
                'label' => 'group.entity.title',
                'help' => 'group.help.title',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'group.entity.description',
                'help' => 'group.help.description',
                'required' => false,
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'group.entity.roles',
                'help' => 'group.help.roles',
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'choice_translation_domain' => 'roles',
                'choices' => array_combine(
                    Roles::ROLES,
                    Roles::ROLES
                ),
            ])
            ->add('analysisNames', ChoiceType::class, [
                'label' => 'group.entity.analysisNames',
                'help' => 'group.help.analysisNames',
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'choice_translation_domain' => 'analysis',
                'choices' => array_combine(
                    $manager->getPackagesNames(),
                    $manager->getPackagesNames()
                ),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['package_manager']);
        $resolver->setAllowedTypes('package_manager', PackageManagerInterface::class);
        $resolver->setDefaults([
            'data_class' => Group::class,
            'package_manager' => null,
        ]);
    }
}
