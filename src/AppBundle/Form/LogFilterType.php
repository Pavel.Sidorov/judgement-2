<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class LogFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dateFrom', DateTimeType::class, [
            'label' => 'log.filter.dateFrom',
            'widget' => 'single_text',
            'required' => false,
            'attr' => [
                'placeholder' => 'log.filter.dateFrom',
            ],
        ]);

        $builder->add('dateTo', DateTimeType::class, [
            'label' => 'log.filter.dateTo',
            'widget' => 'single_text',
            'required' => false,
            'attr' => [
                'placeholder' => 'log.filter.dateTo',
            ],
        ]);

        $builder->add('username', TextType::class, [
            'label' => 'log.filter.username',
            'required' => false,
            'attr' => [
                'placeholder' => 'log.filter.username',
            ],
        ]);
    }
}
