<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use AppBundle\Analysis\Package\PackageManagerInterface;
use AppBundle\Analysis\Analysis\AnalysisInterface;
use AppBundle\Security\UserInterface;

class TaskCreateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $trans = $options['translator'];

        $builder->add('scoringId', TextType::class, [
            'label' => 'task.entity.scoringId',
        ]);

        $builder->add('analysis', ChoiceType::class, [
            'mapped' => false,
            'label' => 'task.other.createAnalysis',
            'placeholder' => 'task.other.createAnalysisPlaceholder',
            'choices' => $this->getAnalysis($options),
            'choice_label' => function($analysis) use ($trans) {
                return $trans->trans('task.other.createAnalysisLabel', [
                    '%name%' => $trans->trans($analysis->getName(), [], 'analysis'),
                    '%type%' => $trans->trans($analysis->getType(), [], 'analysis'),
                    '%version%' => $analysis->getVersion(),
                ]);
            },
            'group_by' => function($analysis) use ($trans) {
                return $trans->trans($analysis->getName(), [], 'analysis');
            },
        ]);

        $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event){
            $analysis = $event->getForm()->get('analysis')->getData();
            $entity = $event->getData();

            $entity->setAnalysisName($analysis->getName());
            $entity->setAnalysisType($analysis->getType());
            $entity->setAnalysisVersion($analysis->getVersion());
        });

        $builder->add('description', TextareaType::class, [
            'label' => 'task.entity.description',
            'required' => false,
        ]);
    }

    /**
     * Получить список методик
     *
     * @param array $options
     *
     * @return array
     */
    private function getAnalysis(array $options): array
    {
        $manager = $options['package_manager'];
        $user = $options['user'];
        $analysis = [];

        // Оставляем из всех имен методик только разрешенные этому пользователю
        $names = array_intersect($manager->getPackagesNames(), $user->getAnalysisNames());

        // Составляем список методик со статусом "действующая"
        foreach ($names as $name) {
            $package = $manager->getPackage($name);

            foreach ($package->getAnalysisList() as $next) {
                if ($next->getStatus() === AnalysisInterface::STATUS_ACTIVE) {
                    $analysis[] = $next;
                }
            }
        }

        return $analysis;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['package_manager', 'user', 'translator']);
        $resolver->setAllowedTypes('package_manager', PackageManagerInterface::class);
        $resolver->setAllowedTypes('user', UserInterface::class);
        $resolver->setAllowedTypes('translator', TranslatorInterface::class);
        $resolver->setDefault('package_manager', null);
        $resolver->setDefault('user', null);
        $resolver->setDefault('translator', null);
    }
}
