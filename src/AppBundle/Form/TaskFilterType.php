<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Analysis\Analysis\AnalysisInterface;
use AppBundle\Security\UserInterface;

class TaskFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $analysisNames = $options['user']->getAnalysisNames();

        $builder->add('analysisName', ChoiceType::class, [
                'label' => 'task.entity.analysisName',
                'required' => false,
                'placeholder' => 'task.filter.analysisName',
                'choice_translation_domain' => 'analysis',
                'choices' => array_combine(
                    $analysisNames,
                    $analysisNames
                ),
            ])
            ->add('analysisType', ChoiceType::class, [
                'label' => 'task.entity.analysisType',
                'required' => false,
                'placeholder' => 'task.filter.analysisType',
                'choice_translation_domain' => 'analysis',
                'choices' => array_combine(
                    AnalysisInterface::TYPES,
                    AnalysisInterface::TYPES
                ),
            ])
            ->add('scoringId', TextType::class, [
                'label' => 'task.entity.scoringId',
                'required' => false,
                'attr' => [
                    'placeholder' => 'task.filter.scoringId',
                ],
            ])
            ->add('clientName', TextType::class, [
                'label' => 'task.entity.clientName',
                'required' => false,
                'attr' => [
                    'placeholder' => 'task.filter.clientName',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['user']);
        $resolver->setAllowedTypes('user', UserInterface::class);
        $resolver->setDefaults([
            'user' => null,
        ]);
    }
}
