<?php

namespace AppBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use AppBundle\Analysis\Package\PackageManagerInterface;
use AppBundle\Analysis\Analysis\AnalysisInterface;
use AppBundle\Form\TaskUpdateType;

class TaskForkType extends TaskUpdateType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $trans = $options['translator'];

        $builder->add('analysis', ChoiceType::class, [
            'mapped' => false,
            'label' => 'task.other.forkAnalysis',
            'placeholder' => 'task.other.forkAnalysisPlaceholder',
            'choices' => $this->getAnalysis($options),
            'choice_label' => function($analysis) use ($trans) {
                return $trans->trans('task.other.forkAnalysisLabel', [
                    '%name%' => $trans->trans($analysis->getName(), [], 'analysis'),
                    '%type%' => $trans->trans($analysis->getType(), [], 'analysis'),
                    '%version%' => $analysis->getVersion(),
                ]);
            },
        ]);

        $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event){
            $analysis = $event->getForm()->get('analysis')->getData();
            $entity = $event->getData();

            $entity->setAnalysisName($analysis->getName());
            $entity->setAnalysisType($analysis->getType());
            $entity->setAnalysisVersion($analysis->getVersion());
        });
    }

    /**
     * Получить список методик
     *
     * @param array $options
     *
     * @return array
     */
    private function getAnalysis(array $options): array
    {
        $manager = $options['package_manager'];
        $task = $options['data'];
        $name = $task->getAnalysisName();
        $type = $task->getAnalysisType();
        $analysis = [];

        // Составляем список методик со статусом "действующая"
        $package = $manager->getPackage($name);
        $versions = $package->getAnalysisVersions($type);

        foreach ($versions as $version) {
            $next = $package->getAnalysis($type, $version);

            if ($next->getStatus() === AnalysisInterface::STATUS_ACTIVE) {
                $analysis[] = $next;
            }
        }

        return $analysis;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['package_manager', 'translator']);
        $resolver->setAllowedTypes('package_manager', PackageManagerInterface::class);
        $resolver->setAllowedTypes('translator', TranslatorInterface::class);
        $resolver->setDefault('package_manager', null);
        $resolver->setDefault('translator', null);
    }
}
