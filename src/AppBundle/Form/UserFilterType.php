<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', ChoiceType::class, [
                'label' => 'user.entity.enabled',
                'required' => false,
                'placeholder' => 'user.filter.enabled',
                'choices' => [
                    'user.enabled.yes' => true,
                    'user.enabled.no' => false,
                ],
            ])
            ->add('username', TextType::class, [
                'label' => 'user.entity.username',
                'required' => false,
                'attr' => [
                    'placeholder' => 'user.filter.username',
                ],
            ])
            ->add('name', TextType::class, [
                'label' => 'user.entity.name',
                'required' => false,
                'attr' => [
                    'placeholder' => 'user.filter.name',
                ],
            ])
            ->add('email', TextType::class, [
                'label' => 'user.entity.email',
                'required' => false,
                'attr' => [
                    'placeholder' => 'user.filter.email',
                ],
            ]);
    }
}
