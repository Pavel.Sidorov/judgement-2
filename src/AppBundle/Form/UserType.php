<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;
use AppBundle\Entity\User;
use AppBundle\Entity\Group;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, [
                'label' => 'user.entity.username',
                'help' => 'user.help.username',
            ])
            ->add('enabled', ChoiceType::class, [
                'label' => 'user.entity.enabled',
                'help' => 'user.help.enabled',
                'required' => true,
                'expanded' => true,
                'choices' => [
                    'user.enabled.yes' => true,
                    'user.enabled.no' => false,
                ],
            ])
            ->add('groups', EntityType::class, [
                'class' => Group::class,
                'label' => 'user.entity.groups',
                'help' => 'user.help.groups',
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.title', Criteria::ASC);
                },
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}