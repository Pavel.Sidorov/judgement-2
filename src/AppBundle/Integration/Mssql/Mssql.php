<?php

namespace AppBundle\Integration\Mssql;

/**
 * Класс для работы с MSSQL
 */
class Mssql implements MssqlInterface
{
    const CHARSET_UTF8 = 'utf-8';

    /**
     * Имя источника данных
     *
     * @var string
     */
    private $dsn;

    /**
     * Имя пользователя
     *
     * @var string
     */
    private $username;

    /**
     * Пароль
     *
     * @var string
     */
    private $password;

    /**
     * Кодировка
     *
     * @var string
     */
    private $charset;

    /**
     * PDO
     *
     * @var string
     */
    private $pdoFqcn = \PDO::class;

    /**
     * Запросы для настройки соединения
     *
     * @var array
     */
    private $initQueries = [
        'SET ANSI_WARNINGS ON',
        'SET ANSI_PADDING ON',
        'SET ANSI_NULLS ON',
        'SET QUOTED_IDENTIFIER ON',
        'SET CONCAT_NULL_YIELDS_NULL ON',
    ];

    /**
     * Подключение
     *
     * @var \PDO
     */
    private $connection;

    /**
     * Конструктор
     *
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param string $charset
     * @param string $pdoFqcn
     * @param array $initQueries
     */
    public function __construct(string $dsn, string $username, string $password,
            string $charset, string $pdoFqcn = null, array $initQueries = null)
    {
        $this->dsn = $dsn;
        $this->username = $username;
        $this->password = $password;
        $this->charset = $charset;

        if ($pdoFqcn !== null) {
            $this->pdoFqcn = $pdoFqcn;
        }

        if ($initQueries !== null) {
            $this->initQueries = $initQueries;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function execProcedure(string $name, array $params, array $cols): array
    {
        $conn = $this->getConnection();

        $sql = $this->buildProcedureQuery($name, array_keys($params));

        try {
            $st = $conn->prepare($sql);
        } catch (\Exception $ex) {
            throw new MssqlException(
                sprintf('Preparation of SQL statement failed. SQL "%s"', $sql),
                null, $ex
            );
        }

        $this->bindProcedureParams($st, $name, $params);

        $res = $st->execute();

        if ($res === false) {
            $error = $st->errorInfo();

            throw new MssqlException(sprintf(
                'Execution of SQL statement failed. SQL "%s", SQLSTATE "%s", code "%s", message "%s"',
                $sql, $error[0], $error[1], $error[2]
            ));
        }

        return $this->fetchProcedureData($st, $name, $cols);
    }

    /**
     * Получить подключение к БД
     *
     * @return \PDO
     */
    protected function getConnection(): \PDO
    {
        if ($this->connection === null) {
            $this->connection = $this->createConnection(
                $this->dsn,
                $this->username,
                $this->password,
                $this->pdoFqcn,
                $this->initQueries
            );
        }

        return $this->connection;
    }

    /**
     * Создать подключение к БД
     *
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param string $pdoFqcn
     * @param array $initQueries
     *
     * @return \PDO
     * @throws MssqlException
     */
    protected function createConnection(string $dsn, string $username,
            string $password, string $pdoFqcn, array $initQueries): \PDO
    {
        if ($pdoFqcn !== \PDO::class && !is_subclass_of($pdoFqcn, \PDO::class)) {
            throw new MssqlException(sprintf(
                'Unexpected PDO FQCN. Expected "%s", given "%s"',
                \PDO::class,
                $pdoFqcn
            ));
        }

        try {
            $conn = new $pdoFqcn($dsn, $username, $password);
        } catch (\Exception $ex) {
            throw new MssqlException('Can\'t connect to database', null, $ex);
        }

        foreach ($initQueries as $query) {
            $res = $conn->exec($query);

            if (!$res && $conn->errorCode() !== '00000') {
                $error = $conn->errorInfo();

                throw new MssqlException(sprintf(
                    'Execution of configuration query failed. SQL "%s", SQLSTATE "%s", code "%s", message "%s"',
                    $query, $error[0], $error[1], $error[2]
                ));
            }
        }

        return $conn;
    }

    /**
     * Составить SQL выражение для выполнения процедуры
     *
     * @param string $name
     * @param array $params
     * 
     * @return string
     * @throws MssqlException
     */
    protected function buildProcedureQuery(string $name, array $params): string
    {
        if (!preg_match('/^[A-Za-z0-9_]+$/u', $name)) {
            throw new MssqlException(sprintf(
                'Procedure name must contain only latin characters, digits and underscore. Procedure "%s"',
                $name
            ));
        }

        $chunks = [];

        foreach ($params as $param) {
            if (!preg_match('/^[A-Za-z0-9_]+$/u', $param)) {
                throw new MssqlException(sprintf(
                    'Parameter name must contain only latin characters, digits and underscore. Param "%s", procedure "%s"',
                    $param, $name
                ));
            }

            $chunks[] = '@'.$param.' = :'.$param;
        }

        return 'EXEC '.$name.' '.implode(', ', $chunks);
    }

    /**
     * Связывание параметров процедуры
     *
     * @param \PDOStatement $statement
     * @param string $procedure
     * @param array $params
     *
     * @throws MssqlException
     */
    protected function bindProcedureParams(\PDOStatement $statement,
            string $procedure, array $params)
    {
        foreach ($params as $param => $value) {
            if ($value === null) {
                $type = \PDO::PARAM_NULL;
            } elseif (is_bool($value)) {
                $type = \PDO::PARAM_BOOL;
            } elseif (is_int($value)) {
                $type = \PDO::PARAM_INT;
            } elseif (is_string($value)) {
                $type = \PDO::PARAM_STR;

                if ($this->charset !== self::CHARSET_UTF8) {
                    $value = iconv(self::CHARSET_UTF8, $this->charset, $value);

                    if ($value === false) {
                        throw new MssqlException(sprintf(
                            'Charset conversion failed for parameter value. Parameter "%s", procedure "%s"',
                            $param, $procedure
                        ));
                    }
                }
            } else {
                throw new MssqlException(sprintf(
                    'Unsupported data type of parameter value. Type "%s", parameter "%s", procedure "%s"',
                    gettype($value), $param, $procedure
                ));
            }

            $res = $statement->bindValue(':'.$param, $value, $type);
            
            if ($res === false) {
                throw new MssqlException(sprintf(
                    'Binding failed. Parameter "%s", procedure "%s"',
                    $param, $procedure
                ));
            }
        }
    }

    /**
     * Разбор результатов процедуры
     *
     * @param \PDOStatement $statement
     * @param string $procedure
     * @param array $cols
     *
     * @return array
     * @throws MssqlException
     */
    protected function fetchProcedureData(\PDOStatement $statement,
            string $procedure, array $cols): array
    {
        $output = [];

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $outputRow = [];

            foreach ($cols as $col) {
                if (!array_key_exists($col, $row)) {
                    throw new MssqlException(sprintf(
                        'Column not found in result row. Expected column "%s", available columns "%s", procedure "%s"',
                        $col, implode(', ', array_keys($row)), $procedure
                    ));
                }

                $value = $row[$col];

                if (is_string($value) && ($this->charset !== self::CHARSET_UTF8)) {
                    $value = iconv($this->charset, self::CHARSET_UTF8, $value);

                    if ($value === false) {
                        throw new MssqlException(sprintf(
                            'Charset conversion failed for column value. Column "%s", procedure "%s"',
                            $col, $procedure
                        ));
                    }
                }

                $outputRow[$col] = $value;
            }

            $output[] = $outputRow;
        }

        return $output;
    }
}
