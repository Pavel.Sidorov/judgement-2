<?php

namespace AppBundle\Integration\Mssql;

/**
 * Интерфейс для работы с MSSQL
 */
interface MssqlInterface
{
    /**
     * Выполнить процедуру
     *
     * @param string $name Название процедуры
     * @param array $params Ассоциативный массив параметров процедуры и их значений
     * @param array $сols Список колонок для выборки
     *
     * @return array
     * @throws MssqlException
     */
    public function execProcedure(string $name, array $params, array $сols): array;
}
