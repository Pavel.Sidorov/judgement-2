<?php

namespace AppBundle\Integration\Scoring\AppData\Answer;

/**
 * Ответ на вопрос в скоринге
 */
class Answer implements AnswerInterface
{
    /**
     * ID вопроса
     *
     * @var integer
     */
    private $id;

    /**
     * Тип вопроса
     *
     * @var string
     */
    private $type;

    /**
     * Формулировка вопроса
     *
     * @var string
     */
    private $question;

    /**
     * Ответ на вопрос
     *
     * @var mixed
     */
    private $value;

    /**
     * Конструктор
     *
     * @param int $id
     * @param string $type
     * @param string $question
     * @param type $value
     */
    public function __construct(int $id, string $type, string $question, $value)
    {
        $this->id = $id;
        $this->type = $type;
        $this->question = $question;
        $this->value = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * {@inheritDoc}
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * {@inheritDoc}
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * {@inheritDoc}
     */
    public function getArrayValue(): array
    {
        return explode('^', rtrim($this->value, '^'));
    }
}
