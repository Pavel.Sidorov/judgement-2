<?php

namespace AppBundle\Integration\Scoring\AppData\Answer;

/**
 * Ответ на вопрос в скоринге
 */
interface AnswerInterface
{
    /**
     * Получить ID вопроса
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Получить тип вопроса
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Получить формулировку вопроса
     *
     * @param string
     */
    public function getQuestion(): string;

    /**
     * Получить ответ на вопрос
     *
     * @return mixed
     */
    public function getValue();

    /**
     * Получить ответ на вопрос в виде массива
     *
     * @return array
     */
    public function getArrayValue(): array;
}
