<?php

namespace AppBundle\Integration\Scoring\AppData;

use AppBundle\Integration\Scoring\AppData\Answer\AnswerInterface;

/**
 * Данные заявки
 */
class AppData implements AppDataInterface
{
    /**
     * ID заявки
     *
     * @var int
     */
    private $id;

    /**
     * Ответы
     *
     * @var array
     */
    private $answers;

    /**
     * Конструктор
     *
     * @param int $id
     * @param array $answers
     */
    public function __construct(int $id, array $answers)
    {
        $this->id = $id;
        $this->answers = [];

        foreach ($answers as $answer) {
            if (!$answer instanceof AnswerInterface) {
                throw new \InvalidArgumentException(sprintf(
                    'Unexpected answer type. Type "%s", expected "%s"',
                    gettype($answer), AnswerInterface::class
                ));
            }

            $this->answers[$answer->getId()] = $answer;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getAnswers(): array
    {
        return $this->answers;
    }

    /**
     * {@inheritDoc}
     */
    public function getAnswerByQuestionId(int $id)
    {
        if (array_key_exists($id, $this->answers)) {
            return $this->answers[$id];
        } else {
            return null;
        }
    }
}
