<?php

namespace AppBundle\Integration\Scoring\AppData;

/**
 * Данные заявки
 */
interface AppDataInterface
{
    /**
     * Получить ID заявки
     */
    public function getId(): int;

    /**
     * Получить ответы на вопросы
     */
    public function getAnswers(): array;

    /**
     * Получить ответ на вопрос
     */
    public function getAnswerByQuestionId(int $id);
}
