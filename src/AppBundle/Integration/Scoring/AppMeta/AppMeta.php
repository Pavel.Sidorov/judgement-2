<?php

namespace AppBundle\Integration\Scoring\AppMeta;

use AppBundle\Integration\Scoring\Office\OfficeInterface;
use AppBundle\Integration\Scoring\AppType\AppTypeInterface;

/**
 * Информация по заявке
 */
class AppMeta implements AppMetaInterface
{
    /**
     * ID заявки
     *
     * @var int
     */
    private $id;

    /**
     * Тип скоринговой заявки
     *
     * @var AppTypeInterface
     */
    private $appType;

    /**
     * Статус заявки
     *
     * @var int
     */
    private $status;

    /**
     * Расшифровка статуса заявки
     *
     * @var string
     */
    private $statusMessage;

    /**
     * Офис банка
     *
     * @var OfficeInterface
     */
    private $office;

    /**
     * Доп. информация
     *
     * @var array
     */
    private $extra;

    /**
     * Конструктор
     *
     * @param int $id
     * @param AppTypeInterface $appType
     * @param int $status
     * @param string $statusMessage
     * @param OfficeInterface $office
     * @param array $extra
     */
    public function __construct(int $id, AppTypeInterface $appType = null,
            int $status = 0, string $statusMessage = '',
            OfficeInterface $office = null, array $extra = [])
    {
        $this->id = $id;
        $this->appType = $appType;
        $this->status = $status;
        $this->statusMessage = $statusMessage;
        $this->office = $office;
        $this->extra = $extra;
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getAppType()
    {
        return $this->appType;
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * {@inheritDoc}
     */
    public function getStatusMessage(): string
    {
        return $this->statusMessage;
    }

    /**
     * {@inheritDoc}
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * {@inheritDoc}
     */
    public function getExtra(): array
    {
        return $this->extra;
    }

    /**
     * {@inheritDoc}
     */
    public function getExtraValue(string $key)
    {
        if (array_key_exists($key, $this->extra)) {
            return $this->extra[$key];
        } else {
            return null;
        }
    }
}
