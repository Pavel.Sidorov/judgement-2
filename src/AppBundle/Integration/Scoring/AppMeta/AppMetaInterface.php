<?php

namespace AppBundle\Integration\Scoring\AppMeta;

use AppBundle\Integration\Scoring\Office\OfficeInterface;
use AppBundle\Integration\Scoring\AppType\AppTypeInterface;

/**
 * Информация по заявке
 */
interface AppMetaInterface
{
    /**
     * Получить ID заявки
     */
    public function getId(): int;

    /**
     * Получить тип скоринговой заявки
     *
     * @return AppTypeInterface
     */
    public function getAppType();

    /**
     * Получить статус заявки
     *
     * @return int
     */
    public function getStatus(): int;

    /**
     * Получить расшифровку статуса заявки
     *
     * @return string
     */
    public function getStatusMessage(): string;

    /**
     * Получить офис банка
     *
     * @return OfficeInterface
     */
    public function getOffice();

    /**
     * Получить доп. параметры
     *
     * @return array
     */
    public function getExtra(): array;

    /**
     * Получить значение доп. параметра
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getExtraValue(string $key);
}
