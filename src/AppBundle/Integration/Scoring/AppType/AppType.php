<?php

namespace AppBundle\Integration\Scoring\AppType;

/**
 * Тип скоринговой заявки
 */
class AppType implements AppTypeInterface
{
    /**
     * Идентификатор
     *
     * @var int
     */
    private $id;

    /**
     * Заголовок
     *
     * @var string
     */
    private $title;

    /**
     * Признак актуальности
     *
     * @var bool
     */
    private $active;

    /**
     * Конструктор
     *
     * @param int $id
     * @param string $title
     * @param bool $active
     */
    public function __construct(int $id, string $title, bool $active)
    {
        $this->id = $id;
        $this->title = $title;
        $this->active = $active;
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * {@inheritDoc}
     */
    public function isActive(): bool
    {
        return $this->active;
    }
}
