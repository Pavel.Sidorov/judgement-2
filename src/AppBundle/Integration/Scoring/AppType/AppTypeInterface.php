<?php

namespace AppBundle\Integration\Scoring\AppType;

/**
 * Тип скоринговой заявки
 */
interface AppTypeInterface
{
    /**
     * Получить идентификатор
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Получить название
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Проверить, является ли действующим
     *
     * @return bool
     */
    public function isActive(): bool;
}
