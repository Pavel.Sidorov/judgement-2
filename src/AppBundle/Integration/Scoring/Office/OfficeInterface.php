<?php

namespace AppBundle\Integration\Scoring\Office;

/**
 * Офис банка
 */
interface OfficeInterface
{
    /**
     * Получить идентификатор офиса
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Получить название офиса
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Проверить, является ли офис действующим
     *
     * @return bool
     */
    public function isActive(): bool;
}
