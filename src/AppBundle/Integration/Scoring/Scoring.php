<?php

namespace AppBundle\Integration\Scoring;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use AppBundle\Integration\Mssql\MssqlInterface;
use AppBundle\Integration\Scoring\Office\OfficeInterface;
use AppBundle\Integration\Scoring\Office\Office;
use AppBundle\Integration\Scoring\AppType\AppTypeInterface;
use AppBundle\Integration\Scoring\AppType\AppType;
use AppBundle\Integration\Scoring\AppMeta\AppMeta;
use AppBundle\Integration\Scoring\AppData\AppData;
use AppBundle\Integration\Scoring\AppData\Answer\Answer;

/**
 * Класс для работы со Скорингом
 */
class Scoring implements ScoringInterface
{
    /**
     * Работа с MSSQL
     *
     * @var MssqlInterface
     */
    private $mssql;

    /**
     * Кеширование
     *
     * @var AdapterInterface
     */
    private $cache;

    /**
     * Время жизни кеша
     *
     * @var int
     */
    private $cacheLifetime;

    /**
     * Конструктор
     *
     * @param MssqlInterface $mssql
     * @param AdapterInterface $cache
     * @param int $cacheLifetime
     */
    public function __construct(MssqlInterface $mssql, AdapterInterface $cache,
            int $cacheLifetime)
    {
        $this->mssql = $mssql;
        $this->cache = $cache;
        $this->cacheLifetime = $cacheLifetime;
    }

    /**
     * Считать офисы из БД без кеширования
     *
     * @return array
     */
    protected function readOffices(): array
    {
        $proc = 'SP_JUDGMENT_GET_INFO';
        $params = ['anketaId' => null, 'num' => 1];
        $keys = ['id', 'title', 'closed'];

        try {
            $result = $this->mssql->execProcedure($proc, $params, $keys);
        } catch (\Exception $ex) {
            throw new ScoringException('Fetching offices failed', null, $ex);
        }

        $tmp = [];
        foreach ($result as $row) {
            $id = (int) $row['id'];
            $title = $row['title'];
            $active = !($row['closed'] == '1');
            $tmp[$id] = new Office($id, $title, $active);
        }

        return $tmp;
    }

    /**
     * {@inheritDoc}
     */
    public function getOffices(): array
    {
        $cache = $this->cache->getItem('scoring.offices');

        if (!$cache->isHit()) {
            $cache->set($this->readOffices());
            $cache->expiresAfter($this->cacheLifetime);
            $this->cache->save($cache);
        }

        return $cache->get();
    }

    /**
     * {@inheritDoc}
     */
    public function getActiveOffices(): array
    {
        return array_filter($this->getOffices(), function(OfficeInterface $office){
            return $office->isActive();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function getOfficeById(int $id)
    {
        $offices = $this->getOffices();

        if (array_key_exists($id, $offices)) {
            return $offices[$id];
        } else {
            return null;
        }
    }

    /**
     * Считать типы скоринговых форм из БД без кеширования
     *
     * @return array
     */
    protected function readAppTypes(): array
    {
        $proc = 'SP_JUDGMENT_GET_INFO';
        $params = ['anketaId' => null, 'num' => 4];
        $keys = ['id', 'title', 'is_active'];

        try {
            $result = $this->mssql->execProcedure($proc, $params, $keys);
        } catch (\Exception $ex) {
            throw new ScoringException('Fetching appTypes failed', null, $ex);
        }

        $tmp = [];
        foreach ($result as $row) {
            $id = (int) $row['id'];
            $title = $row['title'];
            $active = $row['is_active'] == '1';
            $tmp[$id] = new AppType($id, $title, $active);
        }

        return $tmp;
    }

    /**
     * {@inheritDoc}
     */
    public function getAppTypes(): array
    {
        $cache = $this->cache->getItem('scoring.appTypes');

        if (!$cache->isHit()) {
            $cache->set($this->readAppTypes());
            $cache->expiresAfter($this->cacheLifetime);
            $this->cache->save($cache);
        }

        return $cache->get();
    }

    /**
     * {@inheritDoc}
     */
    public function getActiveAppTypes(): array
    {
        return array_filter($this->getAppTypes(), function(AppTypeInterface $prod){
            return $prod->isActive();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function getAppTypeById(int $id)
    {
        $prods = $this->getAppTypes();

        if (array_key_exists($id, $prods)) {
            return $prods[$id];
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAppMeta(int $id)
    {
        $proc = 'SP_JUDGMENT_GET_INFO';
        $params = ['anketaId' => $id, 'num' => 3];
        $keys = [
            'fio', 'ul_name', 'id_client_ubs', 'anketa_type_id',
            'summa', 'userfio', 'num_division', 'state_id', 'state_name',
            'client_type', 'prod_id', 'prod_name', 'type_of_issue_id',
            'type_of_issue', 'related_companies_cnt', 'komis_pred_kred', 'DAT_CREATE'
        ];

        try {
            $result = $this->mssql->execProcedure($proc, $params, $keys);
        } catch (\Exception $ex) {
            throw new ScoringException('Fetching application meta-data failed', null, $ex);
        }

        if (count($result) > 0) {
            $row = $result[0];
            $appType = $this->getAppTypeById($row['anketa_type_id']);
            $status = (int) $row['state_id'];
            $statusMessage = (string) $row['state_name'];
            $office = $this->getOfficeById($row['num_division']);
            $extra = [
                'inspectorName' => trim($row['userfio']), // ФИО инспектора
                'clientType' => trim($row['client_type']), // Тип клиента
                'clientName' => (string) (trim($row['client_type']) == 'ul' ? $row['ul_name'] : $row['fio']), // Имя клиента
                'clientAbsId' => (int) $row['id_client_ubs'], // ID клиента в АБС
                'creditAmount' => (int) $row['summa'], // Сумма кредита
                'productId' => (int) $row['prod_id'], // Продукт
                'productTitle' => trim($row['prod_name']), // Продукт
                'issueTypeId' => $row['type_of_issue_id'], // Тип выдачи кредита
                'issueTypeTitle' => trim($row['type_of_issue']), // Тип выдачи кредита
                'relatedCompaniesCount' => (int) $row['related_companies_cnt'], // Кол-во связанных компаний
                'komisPredKred' => (int) $row['komis_pred_kred'], // Кол-во связанных компаний
                'dateCreated' => (string) $row['DAT_CREATE'], // Кол-во связанных компаний
            ];

            return new AppMeta($id, $appType, $status, $statusMessage,
                    $office, $extra);
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAppData(int $id)
    {
        $proc = 'SP_JUDGMENT_GET_INFO';
        $params = ['anketaId' => $id, 'num' => 2];
        $keys = ['q_id', 'q_type', 'q_title', 'q_value'];

        try {
            $result = $this->mssql->execProcedure($proc, $params, $keys);
        } catch (\Exception $ex) {
            throw new ScoringException('Fetching application data failed', null, $ex);
        }

        if (count($result) > 0) {
            $answers = [];

            foreach ($result as $row) {
                $qid = (int) $row['q_id'];
                $type = strtolower($row['q_type']);
                $question = $row['q_title'];
                $value = $row['q_value'];

                $answers[] = new Answer($qid, $type, $question, $value);
            }

            return new AppData($id, $answers);
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function customProcedure(string $procedure, array $arguments, array $result)
    {
        try {
            return $this->mssql->execProcedure($procedure, $arguments, $result);
        } catch (\Exception $ex) {
            throw new ScoringException('Fetching custom procedure data failed', null, $ex);
        }
    }
}
