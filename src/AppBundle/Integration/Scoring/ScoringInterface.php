<?php

namespace AppBundle\Integration\Scoring;

use AppBundle\Integration\Scoring\Office\OfficeInterface;
use AppBundle\Integration\Scoring\AppType\AppTypeInterface;
use AppBundle\Integration\Scoring\App\AppMetaInterface;
use AppBundle\Integration\Scoring\App\AppDataInterface;

/**
 * Интерфейс для работы со Скорингом
 */
interface ScoringInterface
{
    /**
     * Получить список офисов банка
     *
     * @return array
     */
    public function getOffices(): array;

    /**
     * Получить список действующих офисов банка
     *
     * @return array
     */
    public function getActiveOffices(): array;

    /**
     * Получить офис по ID
     *
     * @return OfficeInterface
     */
    public function getOfficeById(int $id);

    /**
     * Получить список типов скоринговых форм
     *
     * @return array
     */
    public function getAppTypes(): array;

    /**
     * Получить список действующих типов скоринговых форм
     *
     * @return array
     */
    public function getActiveAppTypes(): array;

    /**
     * Получить тип скоринговой формы по ID
     *
     * @return AppTypeInterface
     */
    public function getAppTypeById(int $id);

    /**
     * Получить информацию по заявке
     *
     * @param int $id
     *
     * @return AppMetaInterface
     */
    public function getAppMeta(int $id);

    /**
     * Получить данные заявки
     *
     * @param int $id
     *
     * @return AppDataInterface
     */
    public function getAppData(int $id);

    /**
     * Выполнить процедуру
     *
     * @param string $procedure
     * @param array $arguments
     * @param array $result
     *
     * @return array
     */
    public function customProcedure(string $procedure, array $arguments, array $result);
}
