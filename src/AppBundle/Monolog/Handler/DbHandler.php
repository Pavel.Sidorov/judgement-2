<?php

namespace AppBundle\Monolog\Handler;

use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use AppBundle\Entity\Log;

/**
 * Логирование в БД
 */
class DbHandler extends AbstractProcessingHandler
{
    /**
     * Doctrine entity manager
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, $level = Logger::INFO)
    {
        parent::__construct($level);

        $this->em = $em;
    }

    /**
     * Записать в БД
     *
     * @param array $record
     */
    protected function write(array $record)
    {
        $e = $this->createLogRecord($record);
        $this->em->persist($e);
        $this->em->flush();
    }

    /**
     * Создать запись
     *
     * @param array $record
     *
     * @return Log
     */
    protected function createLogRecord(array $record): Log
    {
        $e = new Log();
        $e->setChannel($record['channel']);
        $e->setLevel($record['level']);
        $e->setLevelName($record['level_name']);
        $e->setMessage($record['message']);
        $e->setContext($record['context']);
        $e->setExtra($record['extra']);

        if ($record['datetime'] instanceof \DateTime) {
            $e->setDate($record['datetime']);
        }

        $username = null;

        if (array_key_exists('username', $record)) {
            $username = $record['username'];
        } elseif ($record['channel'] == 'security') {
            if (isset($record['context']['username'])) {
                $username = $record['context']['username'];
            } elseif (isset($record['context']['exception'])) {
                $ex = $record['context']['exception'];
                if ($ex instanceof AuthenticationException) {
                    $username = $ex->getToken()->getUsername();
                }
            }
        }

        if ($username !== null) {
            $e->setUsername($username);
        }

        return $e;
    }
}
