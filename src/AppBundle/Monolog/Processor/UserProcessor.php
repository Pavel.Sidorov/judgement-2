<?php

namespace AppBundle\Monolog\Processor;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Добавление информации о пользователе
 */
class UserProcessor
{
    /**
     * Хранилище токенов
     *
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * Конструктор
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Добавить доп. информацию
     *
     * @param array $record
     *
     * @return array
     */
    public function processRecord(array $record)
    {
        $token = $this->tokenStorage->getToken();

        if ($token !== null) {
            $user = $token->getUser();

            if (is_object($user)) {
                $record['username'] = $user->getUsername();
            } else {
                $record['username'] = $user;
            }
        }

        return $record;
    }
}
