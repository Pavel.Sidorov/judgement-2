<?php

namespace AppBundle\Product\Microcredit\Primary\V2;

use AppBundle\Analysis\Algorithm\ScoringAwareAlgorithm;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Data\ResultData;
use AppBundle\Analysis\Dictionary\DictionaryInterface;
use AppBundle\Product\Microcredit\Primary\V2\Helper;

/**
 * Первичный расчет "Микрокредит", версия 2
 */
class Algorithm extends ScoringAwareAlgorithm
{
    /**
     * {@inheritDoc}
     */
    public function prepareInitialData(InitialDataInterface $in,
            DictionaryInterface $dict, array $context): InitialDataInterface
    {
        // Скоринговые данные
        $data = $this->getAppData($context);
        $meta = $this->getAppMeta($context);

        // Выборка ответа на вопрос по ID
        $get = function($id, bool $array = false) use ($data) {
            $answer = $data->getAnswerByQuestionId($id);
            if ($answer) {
                return $array ? $answer->getArrayValue() : $answer->getValue();
            } else {
                return $array ? [] : null;
            }
        };

        // Дата анализа
        $in->date = (new \DateTime('today'))
            ->format('d.m.Y');

        // ФИО инспектора
        $in->inspectorName = $meta->getExtraValue('inspectorName');

        // Офис
        $office = $meta->getOffice();
        $in->office = $office ? (string) $office->getId() : null;

        // Список офисов
        $offices = [];
        foreach ($this->scoring->getActiveOffices() as $next) {
            $offices[(string) $next->getId()] = $next->getTitle();
        }
        if ($office) {
            $offices[(string) $office->getId()] = $office->getTitle().($office->isActive() ? '' : ' (закрыт)');
        }
        $in->offices = $offices;

        // Юр. статус
        $in->clientType = 'business';

        // Наименование заемщика и компании, возраст клиента
        $appType = $meta->getAppType();
        if ($appType) {
            if ($appType->getId() === 100) {
                // УКЮЛ ЮЛ микро
                $in->clientName = $get(328);
                $in->clientAge = null;
                $in->companyName = null;
            } elseif (in_array($appType->getId(), [89, 101], true)) {
                // УКЮЛ ИП микро
                $in->clientName = implode(' ', [$get(1), $get(2), $get(3)]);
                $in->clientAge = Helper::clientAge($get(4));
                $in->companyName = implode(' ', ['ИП', $get(1), $get(2), $get(3)]);
            }
        }

        // Семейное положение
        $in->clientMaritalStatus = Helper::maritalStatus($get(31));

        // Количество иждивенцев
        $in->clientDependentCount = Helper::clientDependentCount($get(33));

        // Цель кредита
        if ($appType && $appType->getId() === 89) {
            $in->creditPurpose = 'consumption';
        } else {
            $in->creditPurpose = Helper::creditPurpose($get(939, true));
        }

        // Порядок выдачи
        $in->creditIssueType = Helper::creditIssueType($meta->getExtraValue('productId'));

        // Сумма кредита
        $in->creditAmount = $get(313) ? $get(313) : $get(167);

        // Срок кредита
        $in->creditPeriod = $get(314) ? $get(314) : intval($get(170));

        // Порядок погашения
        $in->creditPaymentType = Helper::creditPaymentType(
            $meta->getExtraValue('issueTypeId'),
            $in->creditIssueType
        );

        // Процентная ставка
        $in->creditRate = $get(776);

        // Основной вид деятельности
        $in->clientActivity = Helper::clientActivity(
            $get(942, true), // Торговля
            $get(944, true), // Услуги
            $get(945) // Производство
        );

        // Учредитель и доля
        $in->companyOwners = Helper::companyOwners($get(969, true));

        // Количество связанных компаний
        $in->relatedCompaniesCount = $meta->getExtraValue('relatedCompaniesCount');

        // Адреса ведения бизнеса
        $in->companyAddresses = Helper::companyAddresses($get(965, true));

        // Доп. информация о бизнесе
        $in->companyInfo = $get(30);

        // Срок ведения бизнеса
        $in->companyAge = Helper::companyAge($get(947));

        // Количество мест ведения бизнеса
        $in->companyAddressesCount = Helper::companyAddressesCount(is_array($in->companyAddresses) ? $in->companyAddresses : []);

        // Численность персонала
        $in->companyStaffCount = Helper::companyStaffCount($get(960));

        // Собственный капитал / Сумма кредита
        $in->propertyCreditRatio = Helper::propertyCreditRatio($get(999), $in->creditAmount);

        // Кредитная история Заемщика и связанных лиц, компаний
        $tmp = ['company' => null, 'bank' => null, 'dateOfIssue' => null,
            'rate' => null, 'amount' => null, 'dateOfRepaymentPlan' => null,
            'dateOfRepaymentFact' => null, 'payment' => null, 'balance' => null];
        $in->creditHistoryList = [$tmp, $tmp, $tmp, $tmp, $tmp, $tmp];

        // Имущественное положение собственников бизнеса (личное имущество)
        $tmp = ['type' => null, 'info' => null, 'price' => null,
            'yearOfPurchase' => null, 'owner' => null];
        $in->ownersPropertyList = [$tmp, $tmp, $tmp, $tmp, $tmp];

        // Активы
        $in->companyAssets = Helper::companyAssets($get(995, true));
        $in->companyAssets2 = ['cash' => null, 'bankAccount' => null,
            'deposits' => null, 'receivableAccounts' => null, 'other' => null,
            'prepaid' => null, 'materials' => null, 'products' => null,
            'goods' => null, 'goodsShipped' => null, 'equipment' => null,
            'vehicles' => null, 'realEstate' => null, 'loans' => null];

        // Пассивы
        $in->companyLiabilities = Helper::companyLiabilities($get(996, true));
        $in->companyLiabilities2 = ['budget' => null, 'wage' => null, 'rent' => null,
            'payableAccounts' => null, 'advances' => null, 'loansPbank' => null,
            'loans' => null, 'borrowings' => null, 'longTermLoans' => null,
            'other' => null];

        // Выручка
        $income = [];
        foreach ($dict->months as $num => $title) {
            $income[$num] = ['month' => '', 1 => null, 2 => null, 3 => null, 4 => null];
        }
        $in->income = $income;

        // Наценка
        $in->incomeExtra = [1 => null, 2 => null, 3 => null, 4 => null];

        // Расходы
        $in->expense = ['wage' => null, 'rent' => null, 'utilities' => null,
            'transport' => null, 'taxes' => null, 'other' => null, 'family' => null,
            'credits' => null, 'credits2' => null, 'household' => null];

        // Обороты по р/с
        $accountTurnover = [];
        foreach ($dict->months as $num => $title) {
            $accountTurnover[$num] = [
                'month' => '',
                'value' => null,
            ];
        }
        $in->accountTurnover = $accountTurnover;

        // Аккаунты
        $tmp = ['bank' => null, 'currency' => null, 'comment' => null];
        $in->accounts = [$tmp, $tmp, $tmp];

        // Обеспечение
        $in->provision = Helper::provision($get(972, true));

        // Поручительство
        $in->guarantee = Helper::guarantee($get(973, true));

        return $in;
    }

    /**
     * {@inheritDoc}
     */
    public function calculate(InitialDataInterface $in,
            DictionaryInterface $dict): ResultDataInterface
    {
        return new ResultData();
    }
}
