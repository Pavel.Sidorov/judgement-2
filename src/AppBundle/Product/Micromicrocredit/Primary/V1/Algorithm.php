<?php

namespace AppBundle\Product\Micromicrocredit\Primary\V1;

use AppBundle\Analysis\Algorithm\ScoringAwareAlgorithm;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Data\ResultData;
use AppBundle\Analysis\Dictionary\DictionaryInterface;
use AppBundle\Product\Micromicrocredit\Primary\V1\Helper;

/**
 * Первичный расчет "Микрокредит", версия 1
 */
class Algorithm extends ScoringAwareAlgorithm
{
    /**
     * {@inheritDoc}
     */
    public function prepareInitialData(InitialDataInterface $in, DictionaryInterface $dict,
        array $context): InitialDataInterface
    {
        // Скоринговые данные
        $data = $this->getAppData($context);
        $meta = $this->getAppMeta($context);

        // Выборка ответа на вопрос по ID
        $get = function($id, bool $array = false) use ($data) {
            $answer = $data->getAnswerByQuestionId($id);
            if ($answer) {
                return $array ? $answer->getArrayValue() : $answer->getValue();
            } else {
                return $array ? [] : null;
            }
        };

        // Дата анализа
        $in->date = (new \DateTime($meta->getExtraValue('dateCreated')))
            ->format('d.m.Y');

        // ФИО инспектора
        $in->inspectorName = $meta->getExtraValue('inspectorName');

        // Офис
        $office = $meta->getOffice();
        $in->office = $office ? (string) $office->getId() : null;

        // Список офисов
        $offices = [];
        foreach ($this->scoring->getActiveOffices() as $next) {
            $offices[(string) $next->getId()] = $next->getTitle();
        }
        if ($office) {
            $offices[(string) $office->getId()] = $office->getTitle().($office->isActive() ? '' : ' (закрыт)');
        }
        $in->offices = $offices;

        $in->absId = $meta->getExtraValue('clientAbsId');

        // Наименование заемщика и компании, возраст клиента
        $appType = $meta->getAppType();
        if ($appType) {
            if ($appType->getId() === 100) {
                // УКЮЛ ЮЛ микро
                $in->clientName = $get(328);
                // $in->companyName = null;
            } elseif (in_array($appType->getId(), [89, 101], true)) {
                // УКЮЛ ИП микро
                $in->clientName = implode(' ', [$get(1), $get(2), $get(3)]);
                // $in->companyName = implode(' ', ['ИП', $get(1), $get(2), $get(3)]);
            }
        }

        //  Вид кредитного продукта
        $in->productAsked = Helper::productType($meta->getExtraValue('prodId'));
        $in->productAsked = Helper::productType(99);

        //  Cумма / Лимит
        $in->limitAsked = $meta->getExtraValue('creditAmount');

        //  Срок кредитования, мес
        $in->periodAsked = (int) $get(314);

        //  Процентная ставка (годовых)
        $in->persentAsked = (int) $get(776);

        //  Комиссия за предоставление кредита
        $in->commissionAsked = (int) $meta->getExtraValue('komisPredKred');
        //  «Комисcия за предоставление кредита» (если не заполнено – проставить значение 3)
        if ($in->commissionAsked == 0) {
            $in->commissionAsked = 3;
        }

        // Порядок погашения
        $in->creditPaymentAsked = Helper::creditPaymentType($meta->getExtraValue('issueTypeId'));

        //  Цель кредита
        $in->purposeOfLending = $get(939);

        // Основной вид деятельности
        $in->clientActivity = Helper::clientActivity(
            $get(942, true), // Торговля
            $get(944, true), // Услуги
            $get(945) // Производство
        );

        //  Связанные компании/лица
        $in->companyAffiliated = Helper::companyAffiliated($get(971, true));

        // Адреса ведения бизнеса
        $in->companyAddresses = Helper::companyAddresses($get(965, true));

        // Адреса (юридический, фактический)
        if ($meta->getExtraValue('clientType') == 'ip') {
            $in->addressRegistrationReal = Helper::addressRegistrationReal($get(985, true), $get(986, true));
        }
        else {
            $in->addressRegistrationReal = Helper::addressRegistrationReal($get(7, true), $get(13, true));
        }

        // Описание бизнеса
        $in->companyDescription = $get(30);

        // Учредитель и доля
        $in->companyOwners = Helper::companyOwners($get(969, true));

        // Количество связанных компаний
        $in->relatedCompaniesCount = $meta->getExtraValue('relatedCompaniesCount');

        // Кредитная история Заемщика и связанных лиц, компаний
        $tmp = ['company' => null, 'bank' => null, 'dateOfIssue' => null,
            'rate' => null, 'amount' => null, 'dateOfRepaymentPlan' => null,
            'dateOfRepaymentFact' => null, 'payment' => null, 'balance' => null];
        $in->creditHistoryList = [$tmp, $tmp, $tmp];

        // Активы
        $in->companyAssets = Helper::companyAssets($get(995, true));

        // Пассивы
        $in->companyLiabilities = Helper::companyLiabilities($get(996, true));

        return $in;
    }

    /**
     * {@inheritDoc}
     */
    public function calculate(InitialDataInterface $in, DictionaryInterface $dict): ResultDataInterface
    {
        return new ResultData();
    }
}
