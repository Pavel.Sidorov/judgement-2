<?php

namespace AppBundle\Product\Micromicrocredit\Primary\V1;

/**
 * Помощник
 */
class Helper
{
    /**
     * Определить возраст клиента
     *
     * @param string $birthday Дата в формате дд.мм.гггг
     *
     * @return int|null
     */
    public static function clientAge($birthday)
    {
        // Проверяем дату
        if ($birthday !== null
                && preg_match_all('/^(\d{2})\.(\d{2})\.(\d{4})$/u', $birthday, $matches)
                && checkdate($matches[2][0], $matches[1][0], $matches[3][0])) {
            $birthday = new \DateTime($birthday);
            $today = new \DateTime();
            $age = $today->diff($birthday);

            return $age->y;
        }

        return null;
    }

    /**
     * Определить цель кредита
     *
     * Потребительские цели (consumption) - если выбрано что-то не равное 1, 2 и 3.
     * Пополнение оборотных средста (replenishment) - если выбрано только 1.
     * Вложения в основные средства (investment) - если выбрано 2 или 3.
     * Смешанные цели (mixed) - если выбраны пункты из двух или более предыдущих категорий.
     *
     * @param array $purposes
     *
     * @return string|null
     */
    public static function creditPurpose(array $purposes = [])
    {
        // Упрощаем - оставляем только цифру
        $purposes = array_map(function($val){
            return (int) preg_replace('/^(\d+).*$/u', '$1', $val);
        }, $purposes);

        // Пополнение оборотных средста
        if (count($purposes) === 1
                && $purposes[0] === 1) {
            return 'replenishment';

        // Вложения в основные средства
        } elseif (count($purposes) === 1
                && in_array($purposes[0], [2, 3], true)) {
            return 'investment';

        // Потребительские цели
        } elseif (count($purposes) > 0
                && !in_array(1, $purposes, true)
                && !in_array(2, $purposes, true)
                && !in_array(3, $purposes, true)) {
            return 'consumption';
        } else {
            $cats = array_map(function($val){
                if ($val === 1) {
                    return 'replenishment';
                } elseif ($val === 2 || $val === 3) {
                    return 'investment';
                } else {
                    return 'consumption';
                }
            }, $purposes);

            // Смешанные цели
            if (count($cats) >= 2) {
                return 'mixed';
            }
        }

        return null;
    }

    /**
     * Определить порядок выдачи кредита
     *
     * @param mixed $product
     *
     * @return string|null
     */
    public static function productType($product = null)
    {
        $map = [
            'overdraft' => [209, 210, 211, 212, 215],
            'kllv' => [98],
            'kllz' => [99],
            'credit' => [206, 207],
        ];

        foreach ($map as $type => $ids) {
            if (in_array($product, $ids, true)) {
                return $type;
            }
        }

        return null;
    }

    /**
     * Определить порядок погашения кредита
     *
     * @param mixed $fromScoring Скоринговое значение
     *
     * @return string|null
     */
    public static function creditPaymentType($fromScoring = null)
    {
        // Аннуитет
        if ($fromScoring === 1) {
            return 'annuity';

        // Сезонный график
        } elseif ($fromScoring === 0) {
            return 'season';

        // Дифференцированные платежи
        } else {
            return 'differential';
        }
    }

    /**
     * Определить порядок погашения кредита
     *
     * @param array $trading Торговля
     * @param array $service Услуги
     * @param mixed $production Производство
     *
     * @return string|null
     */
    public static function clientActivity(array $trading = [], array $service = [], $production = null)
    {
        // Проверка
        $inArray = function(array $subj, array $array){
            foreach ($subj as $s) {
                if (in_array($s, $array, true)) {
                    return true;
                }
            }
            return false;
        };

        // Оптовая торговля
        $test = [
            'Оптовая торговля',
            'Оптово-розничная торговля'
        ];
        if ($inArray($test, $trading)) {
            return 'wholesaleTrading';
        }

        // Розничная торговля
        $test = ['Розничная торговля'];
        if ($inArray($test, $trading)) {
            return 'retailTrading';
        }

        // Услуги
        $test = [
            'Бытовые услуги',
            'Реклама',
            'Транспорт/логистика',
            'Информационные технологии/Связь',
            'Общественное питание',
            'Аренда',
            'Гостиничный бизнес, туризм',
            'Другое'
        ];
        if ($inArray($test, $service)) {
            return 'services';
        }

        // Аренда
        $test = ['Аренда'];
        if ($inArray($test, $service)) {
            return 'rental';
        }

        // Производство
        if (trim($production) !== '') {
            return 'production';
        }

        return null;
    }


    /**
     * Связанные компании/лица
     *
     * @param array $val Скориновый ответ
     *
     * @return array
     */
    public static function companyAffiliated(array $val = [])
    {
        $output = [];

        for ($i = 0; $i < 5; $i++) {
            $ind1 = $i * 2;
            $ind2 = $i * 2 + 1;
            if (isset($val[$ind1])) {
                $output[] = $val[$ind1];
            }
        }

        return implode('; ', $output);
    }


    /**
     * Определить учредителей
     *
     * @param array $owners
     *
     * @return array
     */
    public static function companyOwners(array $owners = [])
    {
        $output = [];

        for ($i = 0; $i < 10; $i++) {
            $ind1 = $i * 3;
            $ind2 = $i * 3 + 2;
            $output[] = [
                'name' => isset($owners[$ind1]) ? $owners[$ind1] : null,
                'share' => isset($owners[$ind2]) ? $owners[$ind2] : null,
            ];
        }

        return $output;
    }

    /**
     * Определить адреса
     *
     * @param array $addresses
     *
     * @return array
     */
    public static function companyAddresses(array $addresses = [])
    {
        $output = [];

        $map = [
            'Собственность' => 'property',
            'Аренда' => 'rent',
        ];

        for ($i = 0; $i < 6; $i++) {
            $ind1 = $i * 4;
            $ind2 = $i * 4 + 1;
            $ind3 = $i * 4 + 2;
            $status = isset($addresses[$ind3]) ? $addresses[$ind3] : null;

            $output[] = [
                'address' => isset($addresses[$ind1]) ? $addresses[$ind1] : null,
                'square' => isset($addresses[$ind2]) ? $addresses[$ind2] : null,
                'status' => isset($map[$status]) ? $map[$status] : null,
            ];
        }

        return $output;
    }

    /**
     * Адрес регистрации бизнеса и адрес ведения бизнеса
     *
     * @param array $addressRegistration
     * @param array $addressReal
     *
     * @return string
     */
    public static function addressRegistrationReal(array $addressRegistration = [], array $addressReal = [])
    {
        $output = (isset($addressRegistration[0])) ? $addressRegistration[0] : null;
        $output .= (isset($addressReal[0])) ? "; " . $addressReal[0] : null;

        return $output;
    }

    /**
     * Срок ведения бизнеса в соответствии с категориями формуляра
     *
     * @param string $age
     *
     * @return string|null
     */
    public static function companyAge($age = null)
    {
        $map = [
            '3-5 месяцев' => '3_6',
            '6-11 месяцев' => '6_12',
            '12-36 месяцев' => '12_36',
        ];

        if (isset($map[$age])) {
            return $map[$age];
        } else {
            return null;
        }
    }

    /**
     * Количество мест ведения бизнеса
     *
     * @param array $addresses
     *
     * @return string|null
     */
    public static function companyAddressesCount(array $addresses = [])
    {
        $count = array_filter($addresses, function($a){
            return ($a && $a['address'] !== null);
        });
        $count = count($count);

        if ($count === 1) {
            return '1';
        } elseif ($count === 2) {
            return '2';
        } elseif ($count >= 3 && $count <= 5) {
            return '3_5';
        } elseif ($count > 5) {
            return '5_';
        }

        return null;
    }

    /**
     * Численность персонала в соответствии с категориями формуляра
     *
     * @param string $count
     *
     * @return string|null
     */
    public static function companyStaffCount($count = null)
    {
        $count = (float) $count;
        if ($count == 0) {
            return 'no';
        } elseif ($count >= 1 && $count <= 2) {
            return '1_2';
        } elseif ($count >= 3 && $count <= 4) {
            return '3_5';
        } elseif ($count >= 5 && $count <= 9) {
            return '5_10';
        } elseif ($count >= 10) {
            return '10_';
        } else {
            return null;
        }
    }

    /**
     * Собственный капитал / Сумма кредита в соответствии с категориями формуляра
     *
     * @param string $capital Собственный капитал
     * @param string $credit Сумма кредита
     *
     * @return string|null
     */
    public static function propertyCreditRatio($capital = null, $credit = null)
    {
        if ($capital === null || $credit === null) {
            return null;
        }

        $capital = (float) $capital;
        $credit = (float) $credit;

        if ($credit == 0) {
            return null;
        }

        $div = $capital / $credit;

        if ($div < 0.05) {
            return '0_5';
        } elseif ($div >= 0.05 && $div <= 0.4) {
            return '5_40';
        } elseif ($div > 0.4 && $div <= 0.7) {
            return '40_70';
        } elseif ($div > 0.7 && $div <= 1) {
            return '70_100';
        } elseif ($div > 1) {
            return '100_';
        }
    }

    /**
     * Активы компании
     *
     * @param array $val Скориновый ответ
     *
     * @return array
     */
    public static function companyAssets(array $val = [])
    {
        return [
            'cash'                  => isset($val[0]) ? (int) $val[0] : 0, // Касса
            'bankAccount'           => isset($val[1]) ? (int) $val[1] : 0, // Банковский счет
            'deposits'              => isset($val[2]) ? (int) $val[2] : 0, // Депозиты
            'receivableAccounts'    => isset($val[3]) ? (int) $val[3] : 0, // Счета к получению
            'other'                 => 0, // Прочие денежные средства
            'prepaid'               => isset($val[4]) ? (int) $val[4] : 0, // Предоплаты (авансы выданные)
            'materials'             => isset($val[5]) ? (int) $val[5] : 0, // Сырье и полуфабрикаты
            'products'              => isset($val[6]) ? (int) $val[6] : 0, // Готовая продукция
            'goods'                 => isset($val[7]) ? (int) $val[7] : 0, // Товары
            'goodsShipped'          => 0, // Товары отгруженные (реализация)
            'equipment'             => isset($val[8]) ? (int) $val[8] : 0, // Оборудование и мебель
            'vehicles'              => isset($val[9]) ? (int) $val[9] : 0, // Транспортные средства
            'realEstate'            => isset($val[10]) ? (int) $val[10] : 0, // Недвижимость
            'loans'                 => isset($val[11]) ? (int) $val[11] : 0, // Займы выданные
        ];
    }

    /**
     * Пассивы компании
     *
     * @param array $val Скориновый ответ
     *
     * @return array
     */
    public static function companyLiabilities(array $val = [])
    {
        return [
            'budget'            => isset($val[0]) ? (int) $val[0] : 0, // Расчеты с бюджетом
            'wage'              => isset($val[1]) ? (int) $val[1] : 0, // Задолженности по заработной плате
            'rent'              => isset($val[2]) ? (int) $val[2] : 0, // Аренда и коммунальные платежи
            'payableAccounts'   => isset($val[3]) ? (int) $val[3] : 0, // Счета к оплате
            'advances'          => isset($val[4]) ? (int) $val[4] : 0, // Авасы полученные
            'loansPbank'        => 0, // Кредиты в Банке Первомайский
            'creditTotal'       => isset($val[5]) ? (int) $val[5] : 0, // Кредиты
            'borrowingsTotal'   => isset($val[6]) ? (int) $val[6] : 0, // Займы
            'borrowingsLong'    => 0, // Долгосрочные займы
            'creditsLong'       => 0, // Долгосрочные кредиты
            'other'             => isset($val[7]) ? (int) $val[7] : 0, // Прочие обязательства
        ];
    }

    /**
     * Обеспечение
     *
     * @param array $val Скориновый ответ
     *
     * @return array
     */
    public static function provision(array $val = [])
    {
        $output = [];
        $map = [
            'Автотранспорт' => 'vehicle',
            'Недвижимость' => 'realEstate',
            'Оборудование/Спецтехника' => 'equipment',
            'ТМЦ' => 'products',
        ];

        for ($i = 0; $i < 11; $i++) {
            $ind1 = $i * 5;
            $ind2 = $i * 5 + 1;
            $ind3 = $i * 5 + 2;
            $ind4 = $i * 5 + 3;
            $ind5 = $i * 5 + 4;
            $type = isset($val[$ind1]) ? $val[$ind1] : null;
            $output[] = [
                'type' => isset($map[$type]) ? $map[$type] : null,
                'description' => isset($val[$ind2]) ? $val[$ind2] : null,
                'name' => isset($val[$ind3]) ? $val[$ind3] : null,
                'phone' => isset($val[$ind4]) ? $val[$ind4] : null,
                'price' => isset($val[$ind5]) ? $val[$ind5] : null,
                'address' => null,
                'infoSource' => null,
                'discount' => null,
                'category' => null,
                'collateralValue' => null,
            ];
        }

        return $output;
    }

    /**
     * Поручительство
     *
     * @param array $val Скориновый ответ
     *
     * @return array
     */
    public static function guarantee(array $val = [])
    {
        $output = [];

        for ($i = 0; $i < 5; $i++) {
            $ind1 = $i * 2;
            $ind2 = $i * 2 + 1;
            $output[] = [
                'name' => isset($val[$ind1]) ? $val[$ind1] : null,
                'phone' => isset($val[$ind2]) ? $val[$ind2] : null,
                'comment' => null,
            ];
        }

        return $output;
    }

    /**
     * Семейное положение в соответствии со словарем
     *
     * @param string $val Значение из скоринга
     *
     * @return string|null
     */
    public static function maritalStatus($val) {
        $map = [
            'Не выбрано' => null,
            'Женат/Замужем' => 'maried',
            'Гражданский брак' => 'civilMarriage',
            'В браке не состоял' => 'single',
            'В браке, проживаем отдельно' => 'maried',
            'Вдова/Вдовец' => 'widowed',
            'Разведен(а)' => 'divorced',
        ];

        return isset($map[$val]) ? $map[$val] : null;
    }

    /**
     * Семейное иждивенцев заемщика в соответствии со словарем
     *
     * @param string $val Значение из скоринга
     *
     * @return string|null
     */
    public static function clientDependentCount($val) {
        if ($val === 0) {
            return 'no';
        } elseif ($val >= 1 && $val <= 2) {
            return '1_2';
        } elseif ($val >= 3 && $val <= 4) {
            return '3_4';
        } elseif ($val >= 5) {
            return '5_';
        }

        return null;
    }

}
