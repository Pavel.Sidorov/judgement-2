<?php

namespace AppBundle\Product\Overdraft\Primary\V1;

use AppBundle\Analysis\Algorithm\ScoringAwareAlgorithm;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Data\ResultData;
use AppBundle\Analysis\Dictionary\DictionaryInterface;

/**
 * Первичный расчет "Овердрафт", версия 1
 */
class Algorithm extends ScoringAwareAlgorithm
{
    /**
     * {@inheritDoc}
     */
    public function prepareInitialData(InitialDataInterface $in,
            DictionaryInterface $dict, array $context): InitialDataInterface
    {
        // Скоринговые данные
        $appData = $this->getAppData($context);
        $appMeta = $this->getAppMeta($context);

        // Выборка ответа на вопрос по ID
        $get = function($id) use ($appData) {
            $answer = $appData->getAnswerByQuestionId($id);
            if ($answer) {
                return $answer->getValue();
            } else {
                return null;
            }
        };

        // Наименование клиента
        $type = $appMeta->getExtraValue('clientType');

        if ($type == 'ul') {
            $in->clientName = $get(328);
        } elseif ($type == 'ip') {
            $in->clientName = implode(' ', [  'ИП', $get(1), $get(2), $get(3)]);
        } else {
            $in->clientName = null;
        }

        // Расчетный счет
        $in->clientAccount = $get(166);

        // Дата анализа
        $in->date = (new \DateTime('today'))
            ->format('d.m.Y');

        // Период 1
        $period1 = (new \DateTime('today'))
            ->modify('first day of this month')
            ->modify('-3 month');
        $in->period1 = $period1->format('m.Y');

        // Период 2
        $period2 = (new \DateTime('today'))
            ->modify('first day of this month')
            ->modify('-2 month');
        $in->period2 = $period2->format('m.Y');

        // Период 3
        $period3 = (new \DateTime('today'))
            ->modify('first day of this month')
            ->modify('-1 month');
        $in->period3 = $period3->format('m.Y');

        // Данные за период 1
        $data1 = $this->scoring->customProcedure('SP_JUDGMENT_GET_INFO', [
                'anketaId' => $appMeta->getExtraValue('clientAbsId'),
		'num' => 5,
		'date' => $period1->format('Y-m-d')
        ], ['cnt_operations', 'cnt_partners', 'summa']);

        if (count($data1)) {
            $in->period1ActivityIncome = $data1[0]['cnt_operations'];
            $in->period1ContractorsCount = $data1[0]['cnt_partners'];
            $in->period1CleanIncome = $data1[0]['summa'];
        } else {
            $in->period1ActivityIncome = null;
            $in->period1ContractorsCount = null;
            $in->period1CleanIncome = null;
        }

        // Данные за период 2
        $data2 = $this->scoring->customProcedure('SP_JUDGMENT_GET_INFO', [
                'anketaId' => $appMeta->getExtraValue('clientAbsId'),
		'num' => 5,
		'date' => $period2->format('Y-m-d')
        ], ['cnt_operations', 'cnt_partners', 'summa']);

        if (count($data2)) {
            $in->period2ActivityIncome = $data2[0]['cnt_operations'];
            $in->period2ContractorsCount = $data2[0]['cnt_partners'];
            $in->period2CleanIncome = $data2[0]['summa'];
        } else {
            $in->period2ActivityIncome = null;
            $in->period2ContractorsCount = null;
            $in->period2CleanIncome = null;
        }

        // Данные за период 3
        $data3 = $this->scoring->customProcedure('SP_JUDGMENT_GET_INFO', [
                'anketaId' => $appMeta->getExtraValue('clientAbsId'),
		'num' => 5,
		'date' => $period3->format('Y-m-d')
        ], ['cnt_operations', 'cnt_partners', 'summa']);

        if (count($data3)) {
            $in->period3ActivityIncome = $data3[0]['cnt_operations'];
            $in->period3ContractorsCount = $data3[0]['cnt_partners'];
            $in->period3CleanIncome = $data3[0]['summa'];
        } else {
            $in->period3ActivityIncome = null;
            $in->period3ContractorsCount = null;
            $in->period3CleanIncome = null;
        }

        // Расчетный лимит овердрафта
        $in->cond1Val = ($in->period1CleanIncome + $in->period2CleanIncome + $in->period3CleanIncome) / 2 / 3;
        if ($in->cond1Val > 200000) {
            $in->cond1Val = 200000;
        }

        // Справка п.3 - количество поступлений
        $in->cond3Val = min([$in->period1ActivityIncome, $in->period2ActivityIncome, $in->period3ActivityIncome]);

        // Справка п.5 - количество контрагентов
        $in->cond5Val = min([$in->period1ContractorsCount, $in->period2ContractorsCount, $in->period3ContractorsCount]);

        return $in;
    }

    /**
     * {@inheritDoc}
     */
    public function calculate(InitialDataInterface $in,
            DictionaryInterface $dict): ResultDataInterface
    {
        $res = new ResultData();

        // 50% от среднемесячных кредитовых оборотов
        $res->period1CleanIncomeHalf = $in->period1CleanIncome * 0.5;
        $res->period2CleanIncomeHalf = $in->period2CleanIncome * 0.5;
        $res->period3CleanIncomeHalf = $in->period3CleanIncome * 0.5;

        // Итого
        $res->activityIncomeSummary = $in->period1ActivityIncome
                + $in->period2ActivityIncome
                + $in->period3ActivityIncome;

        $res->contractorsCountSummary = $in->period1ContractorsCount
                + $in->period2ContractorsCount
                + $in->period3ContractorsCount;

        $res->cleanIncomeSummary = $in->period1CleanIncome
                + $in->period2CleanIncome
                + $in->period3CleanIncome;

        $res->cleanIncomeHalfSummary = $res->period1CleanIncomeHalf
                + $res->period2CleanIncomeHalf
                + $res->period3CleanIncomeHalf;

        // Расчетный лимит овердрафта
        $res->overdraftLimit = $res->cleanIncomeHalfSummary / 3;

        return $res;
    }
}
