<?php

namespace AppBundle\Product\Overdraft\Secondary\V1;

use AppBundle\Analysis\Algorithm\ScoringAwareAlgorithm;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Data\ResultData;
use AppBundle\Analysis\Dictionary\DictionaryInterface;

/**
 * Вторичный расчет "Мониторинг лимита овердрафта", версия 1
 */
class Algorithm extends ScoringAwareAlgorithm
{
    /**
     * {@inheritDoc}
     */
    public function prepareInitialData(InitialDataInterface $in,
            DictionaryInterface $dict, array $context): InitialDataInterface
    {
        // Скоринговые данные
        $appData = $this->getAppData($context);
        $appMeta = $this->getAppMeta($context);

        if ($appData === null || $appMeta === null) {
            throw new \Exception('Скоринговая заявка не найдена');
        }

        // Выборка ответа на вопрос по ID
        $get = function($id) use ($appData) {
            $answer = $appData->getAnswerByQuestionId($id);
            if ($answer) {
                return $answer->getValue();
            } else {
                return null;
            }
        };

        // Наименование клиента
        $type = $appMeta->getExtraValue('clientType');

        if ($type == 'ul') {
            $in->clientName = $get(328);
        } elseif ($type == 'ip') {
            $in->clientName = implode(' ', [  'ИП', $get(1), $get(2), $get(3)]);
        } else {
            $in->clientName = null;
        }

        // Расчетный счет
        $in->clientAccount = $get(166);

        // Дата анализа
        $in->date = (new \DateTime('today'))
            ->format('d.m.Y');

        // Период 1
        $period1 = (new \DateTime('today'))
            ->modify('first day of this month')
            ->modify('-3 month');
        $in->period1 = $period1->format('m.Y');

        // Период 2
        $period2 = (new \DateTime('today'))
            ->modify('first day of this month')
            ->modify('-2 month');
        $in->period2 = $period2->format('m.Y');

        // Период 3
        $period3 = (new \DateTime('today'))
            ->modify('first day of this month')
            ->modify('-1 month');
        $in->period3 = $period3->format('m.Y');

        // Данные за период 1
        $data1 = $this->scoring->customProcedure('SP_JUDGMENT_GET_INFO', [
                'anketaId' => $appMeta->getExtraValue('clientAbsId'),
		'num' => 6,
		'date' => $period1->format('Y-m-d')
        ], ['cnt_operations', 'cnt_partners', 'summa', 'limit_overdraft']);

        if (count($data1)) {
            $in->limitOverdraft = $data1[0]['limit_overdraft'];
            $in->period1ActivityIncome = $data1[0]['cnt_operations'];
            $in->period1ContractorsCount = $data1[0]['cnt_partners'];
            $in->period1CleanIncome = $data1[0]['summa'];
        } else {
            $in->period1ActivityIncome = null;
            $in->period1ContractorsCount = null;
            $in->period1CleanIncome = null;
            $in->limitOverdraft = null;
        }

        // Данные за период 2
        $data2 = $this->scoring->customProcedure('SP_JUDGMENT_GET_INFO', [
                'anketaId' => $appMeta->getExtraValue('clientAbsId'),
		'num' => 6,
		'date' => $period2->format('Y-m-d')
        ], ['cnt_operations', 'cnt_partners', 'summa', 'limit_overdraft']);

        if (count($data2)) {
            $in->limitOverdraft = $data2[0]['limit_overdraft'];
            $in->period2ActivityIncome = $data2[0]['cnt_operations'];
            $in->period2ContractorsCount = $data2[0]['cnt_partners'];
            $in->period2CleanIncome = $data2[0]['summa'];
        } else {
            $in->period2ActivityIncome = null;
            $in->period2ContractorsCount = null;
            $in->period2CleanIncome = null;
        }

        // Данные за период 3
        $data3 = $this->scoring->customProcedure('SP_JUDGMENT_GET_INFO', [
                'anketaId' => $appMeta->getExtraValue('clientAbsId'),
		'num' => 6,
		'date' => $period3->format('Y-m-d')
        ], ['cnt_operations', 'cnt_partners', 'summa', 'limit_overdraft']);

        if (count($data3)) {
            $in->limitOverdraft = $data3[0]['limit_overdraft'];
            $in->period3ActivityIncome = $data3[0]['cnt_operations'];
            $in->period3ContractorsCount = $data3[0]['cnt_partners'];
            $in->period3CleanIncome = $data3[0]['summa'];
        } else {
            $in->period3ActivityIncome = null;
            $in->period3ContractorsCount = null;
            $in->period3CleanIncome = null;
        }

        return $in;
    }

    /**
     * {@inheritDoc}
     */
    public function calculate(InitialDataInterface $in,
            DictionaryInterface $dict): ResultDataInterface
    {
        return new ResultData();
    }
}
