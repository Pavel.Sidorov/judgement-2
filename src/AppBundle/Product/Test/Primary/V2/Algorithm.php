<?php

namespace AppBundle\Product\Test\Primary\V2;

use AppBundle\Analysis\Algorithm\Base;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Data\ResultData;
use AppBundle\Analysis\Dictionary\DictionaryInterface;

class Algorithm extends Base
{
    /**
     * {@inheritDoc}
     */
    public function calculate(InitialDataInterface $initial,
            DictionaryInterface $dictionary): ResultDataInterface
    {
        $result = new ResultData();
        $result->set('test', 'test_primary_2_result');

        return $result;
    }
}
