<?php

namespace AppBundle\Product\Test\Secondary\V2;

use AppBundle\Analysis\Algorithm\Algorithm as Base;
use AppBundle\Analysis\Data\InitialDataInterface;
use AppBundle\Analysis\Data\ResultDataInterface;
use AppBundle\Analysis\Data\ResultData;
use AppBundle\Analysis\Dictionary\DictionaryInterface;

class Algorithm extends Base
{
    /**
     * {@inheritDoc}
     */
    public function calculate(InitialDataInterface $initial,
            DictionaryInterface $dictionary): ResultDataInterface
    {
        $result = new ResultData();
        $result->set('test', 'test_secondary_2_result');

        return $result;
    }
}
