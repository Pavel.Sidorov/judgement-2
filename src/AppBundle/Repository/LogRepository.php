<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;

class LogRepository extends EntityRepository
{
    /**
     * Подсчитать количество записей, подходящих под параметры
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param string $username
     *
     * @return int
     */
    public function countLogs(\DateTime $dateFrom = null,
            \DateTime $dateTo = null, string $username = null): int
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select($qb->expr()->count('e.id'));

        if ($dateFrom !== null) {
            $qb->andWhere($qb->expr()->gte('e.date', ':dateFrom'));
            $qb->setParameter(':dateFrom', $dateFrom);
        }

        if ($dateTo !== null) {
            $qb->andWhere($qb->expr()->lt('e.date', ':dateTo'));
            $qb->setParameter(':dateTo', $dateTo);
        }

        if ($username !== null) {
            $qb->andWhere($qb->expr()->like('e.username', ':username'));
            $qb->setParameter(':username', $username);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Сделать выборку записей, подходящих под параметры
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param string $username
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function getLogs(\DateTime $dateFrom = null,
            \DateTime $dateTo = null, string $username = null,
            int $offset = null, int $limit = null): array
    {
        $qb = $this->createQueryBuilder('e');
        $qb->addOrderBy('e.date', Criteria::DESC);

        if ($dateFrom !== null) {
            $qb->andWhere($qb->expr()->gte('e.date', ':dateFrom'));
            $qb->setParameter(':dateFrom', $dateFrom);
        }

        if ($dateTo !== null) {
            $qb->andWhere($qb->expr()->lt('e.date', ':dateTo'));
            $qb->setParameter(':dateTo', $dateTo);
        }

        if ($username !== null) {
            $qb->andWhere($qb->expr()->like('e.username', ':username'));
            $qb->setParameter(':username', $username);
        }

        if ($offset !== null) {
            $qb->setFirstResult($offset);
        }

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }
}
