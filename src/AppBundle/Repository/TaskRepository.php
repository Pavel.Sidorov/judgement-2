<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;

class TaskRepository extends EntityRepository
{
    /**
     * Подсчитать количество заданий, подходящих под параметры
     *
     * @param string $analysisName
     * @param string $analysisType
     * @param int $scoringId
     * @param string $clientName
     * @param int $excludeId
     *
     * @return int
     */
    public function countRootTasks(string $analysisName = null,
            string $analysisType = null, int $scoringId = null,
            string $clientName = null, int $excludeId = null): int
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select($qb->expr()->count('e.id'));
        $qb->andWhere($qb->expr()->isNull('e.parent'));

        if ($analysisName !== null) {
            $qb->andWhere($qb->expr()->eq('e.analysisName', ':analysisName'));
            $qb->setParameter(':analysisName', $analysisName);
        }

        if ($analysisType !== null) {
            $qb->andWhere($qb->expr()->eq('e.analysisType', ':analysisType'));
            $qb->setParameter(':analysisType', $analysisType);
        }

        if ($scoringId !== null) {
            $qb->andWhere($qb->expr()->eq('e.scoringId', ':scoringId'));
            $qb->setParameter(':scoringId', $scoringId);
        }

        if ($clientName !== null) {
            $qb->andWhere($qb->expr()->like('e.clientName', ':clientName'));
            $qb->setParameter(':clientName', '%'.$clientName.'%');
        }

        if ($excludeId !== null) {
            $qb->andWhere($qb->expr()->neq('e.id', ':excludeId'));
            $qb->setParameter(':excludeId', $excludeId);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Сделать выборку заданий, подходящих под параметры
     *
     * @param string $analysisName
     * @param string $analysisType
     * @param int $scoringId
     * @param string $clientName
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function getRootTasks(string $analysisName = null,
            string $analysisType = null, int $scoringId = null,
            string $clientName = null, int $offset = null,
            int $limit = null): array
    {
        $qb = $this->createQueryBuilder('e');
        $qb->andWhere($qb->expr()->isNull('e.parent'));
        $qb->addOrderBy('e.createdAt', Criteria::DESC);

        if ($analysisName !== null) {
            $qb->andWhere($qb->expr()->eq('e.analysisName', ':analysisName'));
            $qb->setParameter(':analysisName', $analysisName);
        }

        if ($analysisType !== null) {
            $qb->andWhere($qb->expr()->eq('e.analysisType', ':analysisType'));
            $qb->setParameter(':analysisType', $analysisType);
        }

        if ($scoringId !== null) {
            $qb->andWhere($qb->expr()->eq('e.scoringId', ':scoringId'));
            $qb->setParameter(':scoringId', $scoringId);
        }

        if ($clientName !== null) {
            $qb->andWhere($qb->expr()->like('e.clientName', ':clientName'));
            $qb->setParameter(':clientName', '%'.$clientName.'%');
        }

        if ($offset !== null) {
            $qb->setFirstResult($offset);
        }

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }
}
