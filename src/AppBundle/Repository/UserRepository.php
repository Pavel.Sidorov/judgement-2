<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;

class UserRepository extends EntityRepository
{
    /**
     * Подсчитать количество пользователей, подходящих под параметры
     *
     * @param bool $enabled
     * @param string $username
     * @param string $name
     * @param string $email
     *
     * @return int
     */
    public function countUsers(bool $enabled = null, string $username = null,
            string $name = null, string $email = null): int
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select($qb->expr()->count('e.id'));

        if ($enabled !== null) {
            $qb->andWhere($qb->expr()->eq('e.enabled', ':enabled'));
            $qb->setParameter(':enabled', $enabled);
        }

        if ($username !== null) {
            $qb->andWhere($qb->expr()->like('e.username', ':username'));
            $qb->setParameter(':username', $username);
        }

        if ($name !== null) {
            $qb->andWhere($qb->expr()->like('e.name', ':name'));
            $qb->setParameter(':name', $name);
        }

        if ($email !== null) {
            $qb->andWhere($qb->expr()->like('e.email', ':email'));
            $qb->setParameter(':email', $email);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Сделать выборку пользователей, подходящих под параметры
     *
     * @param bool $enabled
     * @param string $username
     * @param string $name
     * @param string $email
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function getUsers(bool $enabled = null, string $username = null,
            string $name = null, string $email = null, int $offset = null,
            int $limit = null): array
    {
        $qb = $this->createQueryBuilder('e');
        $qb->addOrderBy('e.name', Criteria::ASC);
        $qb->addOrderBy('e.username', Criteria::ASC);

        if ($enabled !== null) {
            $qb->andWhere($qb->expr()->eq('e.enabled', ':enabled'));
            $qb->setParameter(':enabled', $enabled);
        }

        if ($username !== null) {
            $qb->andWhere($qb->expr()->like('e.username', ':username'));
            $qb->setParameter(':username', $username);
        }

        if ($name !== null) {
            $qb->andWhere($qb->expr()->like('e.name', ':name'));
            $qb->setParameter(':name', $name);
        }

        if ($email !== null) {
            $qb->andWhere($qb->expr()->like('e.email', ':email'));
            $qb->setParameter(':email', $email);
        }

        if ($offset !== null) {
            $qb->setFirstResult($offset);
        }

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }
}