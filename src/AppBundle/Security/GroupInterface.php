<?php

namespace AppBundle\Security;

/**
 * Группа пользователей
 */
interface GroupInterface
{
    /**
     * Получить список ролей, присвоенных группе
     */
    public function getRoles(): array;

    /**
     * Получить список разрешенных для группы имен анализов
     */
    public function getAnalysisNames(): array;
}
