<?php

namespace AppBundle\Security\Listener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Ldap\LdapInterface;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\User;

/**
 * Слушатель аутентификации
 */
class AuthListener
{
    /**
     * @var LdapInterface $ldap
     */
    private $ldap;

    /**
     * @var string
     */
    private $baseDn;

    /**
     * @var string
     */
    private $searchQuery;

    /**
     * Entity manager
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     * @param LdapInterface $ldap
     * @param string $baseDn
     * @param string $searchQuery
     */
    public function __construct(EntityManagerInterface $entityManager,
            LdapInterface $ldap, string $baseDn, string $searchQuery)
    {
        $this->entityManager = $entityManager;
        $this->ldap = $ldap;
        $this->baseDn = $baseDn;
        $this->searchQuery = $searchQuery;
    }

    /**
     * Обработка успешной аутентификации
     *
     * Обновление данных пользователя
     *
     * @param InteractiveLoginEvent $event
     */
    public function onAuthenticationSuccess(InteractiveLoginEvent $event)
    {
        $token = $event->getAuthenticationToken();
        $user = $token->getUser();

        if (!$user instanceof User) {
            return;
        }

        $username = $user->getUsername();
        $username = $this->ldap->escape($username, '', LdapInterface::ESCAPE_FILTER);
        $query = str_replace('{username}', $username, $this->searchQuery);

        $result = $this->ldap
            ->query($this->baseDn, $query)
            ->execute()
            ->toArray();

        if (count($result) == 0) {
            return;
        }

        $result = array_shift($result);

        $name = $result->getAttribute('name');
        if ($name) {
            $user->setName($name[0]);
        }

        $email = $result->getAttribute('mail');
        if ($email) {
            $user->setEmail($email[0]);
        }

        $department = $result->getAttribute('department');
        if ($department) {
            $user->setDepartment($department[0]);
        }

        $phone = $result->getAttribute('telephoneNumber');
        if ($phone) {
            $user->setPhone($phone[0]);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
