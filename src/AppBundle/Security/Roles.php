<?php

namespace AppBundle\Security;

/**
 * Роли
 */
class Roles
{
    const ROLE_GROUP = 'ROLE_GROUP';
    const ROLE_USER = 'ROLE_USER';
    const ROLE_LOG = 'ROLE_LOG';
    const ROLE_TASK = 'ROLE_TASK';
    const ROLE_TASK_CREATE = 'ROLE_TASK_CREATE';
    const ROLE_TASK_READ = 'ROLE_TASK_READ';
    const ROLE_TASK_UPDATE = 'ROLE_TASK_UPDATE';
    const ROLE_TASK_INPUT = 'ROLE_TASK_INPUT';
    const ROLE_TASK_COMPLETE = 'ROLE_TASK_COMPLETE';
    const ROLE_TASK_REPORT = 'ROLE_TASK_REPORT';
    const ROLE_TASK_FORK = 'ROLE_TASK_FORK';
    const ROLE_TASK_SUMMARIZE = 'ROLE_TASK_SUMMARIZE';

    // Уведомления о "Мониторинге лимита овердрафта"
    const ROLE_OVERDRAFT_SECONDARY = 'ROLE_OVERDRAFT_SECONDARY';

    const ROLES = [
        self::ROLE_GROUP,
        self::ROLE_USER,
        self::ROLE_LOG,
        self::ROLE_TASK,
        self::ROLE_TASK_CREATE,
        self::ROLE_TASK_READ,
        self::ROLE_TASK_UPDATE,
        self::ROLE_TASK_INPUT,
        self::ROLE_TASK_COMPLETE,
        self::ROLE_TASK_REPORT,
        self::ROLE_TASK_FORK,
        self::ROLE_TASK_SUMMARIZE,
        self::ROLE_OVERDRAFT_SECONDARY,
    ];
}
