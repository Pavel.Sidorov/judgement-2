<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\Common\Collections\Collection;

/**
 * Пользователь
 */
interface UserInterface extends AdvancedUserInterface
{
    /**
     * Получить список групп пользователя
     */
    public function getGroups(): Collection;

    /**
     * Получить список разрешенных для пользователя имен анализов
     */
    public function getAnalysisNames(): array;
}
