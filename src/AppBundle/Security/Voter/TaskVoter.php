<?php

namespace AppBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use AppBundle\Analysis\Package\PackageManagerInterface;
use AppBundle\Analysis\Task\TaskInterface;
use AppBundle\Security\UserInterface;
use AppBundle\Security\Roles;

/**
 * Авторизация действий над заданиями
 */
class TaskVoter extends Voter
{
    // Создание нового задания
    const CREATE = 'CREATE';

    // Просмотр подробностей задания
    const READ = 'READ';

    // Редактирвование задания
    const UPDATE = 'UPDATE';

    // Ввод данных
    const INPUT = 'INPUT';

    // Завершение задания
    const COMPLETE = 'COMPLETE';

    // Построение отчета
    const REPORT = 'REPORT';

    // Создание новой версии задания
    const FORK = 'FORK';

    // Выбор итогового расчета в группе
    const SUMMARIZE = 'SUMMARIZE';

    /**
     * Менеджер пакетов методик
     *
     * @var PackageManagerInterface
     */
    private $packageManager;

    /**
     * Менеджер решений
     *
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    /**
     * Конструтор
     *
     * @param PackageManagerInterface $packageManager
     * @param AccessDecisionManagerInterface $decisionManager
     * @param ValidatorInterface $validator
     */
    public function __construct(PackageManagerInterface $packageManager,
            AccessDecisionManagerInterface $decisionManager)
    {
        $this->packageManager = $packageManager;
        $this->decisionManager = $decisionManager;
    }

    /**
     * {@inheritDoc}
     */
    protected function supports($attribute, $subject)
    {
        // Доступ к заданиям только для переопределенного UserInterface
        if (!$subject || !$subject instanceof TaskInterface) {
            return false;
        }

        $attrs = [
            self::CREATE,
            self::READ,
            self::UPDATE,
            self::INPUT,
            self::COMPLETE,
            self::REPORT,
            self::FORK,
            self::SUMMARIZE,
        ];

        if (!in_array($attribute, $attrs, true)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // Доступ к заданиям только для переопределенного UserInterface
        if (!$user || !$user instanceof UserInterface) {
            return false;
        }

        // Требуется базовая роль для работы с заданиями
        if (!$this->decisionManager->decide($token, [Roles::ROLE_TASK])) {
            return false;
        }

        // Требуется разрешение на работу с методикой, указанной в задании
        if (!in_array($subject->getAnalysisName(), $user->getAnalysisNames(), true)) {
            return false;
        }

        switch($attribute) {
            case self::CREATE:
                return $this->canCreate($subject, $user, $token);
            case self::READ:
                return $this->canRead($subject, $user, $token);
            case self::UPDATE:
                return $this->canUpdate($subject, $user, $token);
            case self::INPUT:
                return $this->canInput($subject, $user, $token);
            case self::COMPLETE:
                return $this->canComplete($subject, $user, $token);
            case self::REPORT:
                return $this->canReport($subject, $user, $token);
            case self::FORK:
                return $this->canFork($subject, $user, $token);
            case self::SUMMARIZE:
                return $this->canSummarize($subject, $user, $token);
        }

        throw new \LogicException(sprintf('Unexpected attribute %', $attribute));
    }

    /**
     * Разрешение на создание
     *
     * @param TaskInterface $task
     * @param UserInterface $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canCreate(TaskInterface $task = null, UserInterface $user, TokenInterface $token): bool
    {
        // Требуется соответствующая роль
        return $this->decisionManager->decide($token, [Roles::ROLE_TASK_CREATE]);
    }

    /**
     * Разрешение на просмотр
     *
     * @param TaskInterface $task
     * @param UserInterface $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canRead(TaskInterface $task, UserInterface $user, TokenInterface $token): bool
    {
        // Требуется соответствующая роль
        return $this->decisionManager->decide($token, [Roles::ROLE_TASK_READ]);
    }

    /**
     * Разрешение на редактирование задания
     *
     * @param TaskInterface $task
     * @param UserInterface $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canUpdate(TaskInterface $task, UserInterface $user, TokenInterface $token): bool
    {
        // Требуется соответствующая роль
        if ($this->decisionManager->decide($token, [Roles::ROLE_TASK_UPDATE])) {
            // Задание должно быть незавершенным
            return $task->isCompleted() === false;
        }

        return false;
    }

    /**
     * Разрешение на ввод исходных данных
     *
     * @param TaskInterface $task
     * @param UserInterface $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canInput(TaskInterface $task, UserInterface $user, TokenInterface $token): bool
    {
        // Требуется соответствующая роль
        if ($this->decisionManager->decide($token, [Roles::ROLE_TASK_INPUT])) {
            // Задание должно быть незавершенным
            return $task->isCompleted() === false;
        }

        return false;
    }

    /**
     * Разрешение на завершение задания
     *
     * @param TaskInterface $task
     * @param UserInterface $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canComplete(TaskInterface $task, UserInterface $user, TokenInterface $token): bool
    {
        // Требуется соответствующая роль
        if ($this->decisionManager->decide($token, [Roles::ROLE_TASK_COMPLETE])) {
            // Задание должно быть незавершенным
            return $task->isCompleted() === false;
        }

        return false;
    }

    /**
     * Разрешение на построение отчета
     *
     * @param TaskInterface $task
     * @param UserInterface $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canReport(TaskInterface $task, UserInterface $user, TokenInterface $token): bool
    {
        // Требуется соответствующая роль
        if ($this->decisionManager->decide($token, [Roles::ROLE_TASK_REPORT])) {
            // Задание должно быть завершенным
            return $task->isCompleted() === true;
        }

        return false;
    }

    /**
     * Разрешение на создание новой версии задания
     *
     * @param TaskInterface $task
     * @param UserInterface $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canFork(TaskInterface $task, UserInterface $user, TokenInterface $token): bool
    {
        // Требуется соответствующая роль
        return $this->decisionManager->decide($token, [Roles::ROLE_TASK_FORK]);
    }

    /**
     * Разрешение на выбор итогового расчета
     *
     * @param TaskInterface $task
     * @param UserInterface $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canSummarize(TaskInterface $task, UserInterface $user, TokenInterface $token): bool
    {
        // Требуется соответствующая роль
        if ($this->decisionManager->decide($token, [Roles::ROLE_TASK_SUMMARIZE])) {
            // Задание должно быть завершенным, но не финальным
            return ($task->isCompleted() === true) && ($task->isFinal() === false);
        }

        return false;
    }
}
