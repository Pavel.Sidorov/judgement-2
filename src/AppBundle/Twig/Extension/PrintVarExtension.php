<?php

namespace AppBundle\Twig\Extension;

/**
 * Вывод переменной в текстовом виде
 */
class PrintVarExtension extends \Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('print_var', [$this, 'printVar']),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'print_var_extension';
    }

    /**
     * Вывести переменную в текстовом виде
     *
     * @param mixed $var
     *
     * @return string
     */
    public function printVar($var)
    {
        return print_r($var, true);
    }
}