<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Применимость методики для скоринговой заявки
 *
 * @Annotation
 */
class AnalysisForScoringApp extends Constraint
{
    // Не выбрана методика анализа
    public $messageNoAnalysis = 'task.analysisVersion.noAnalysis';

    // Методика анализа является архивной
    public $messageArchivalAnalysis = 'task.analysisVersion.archivalAnalysis';

    // Ошибка скоринга
    public $messageScoringError = 'task.scoringId.scoringError';

    // Скоринговая заявка не найдена
    public $messageNoApp = 'task.scoringId.noApp';

    // Не найден тип формы, указанный в скоринговой заявке
    public $messageNoAppType = 'task.scoringId.noAppType';

    // В скоринговой заявке указан недействующий тип формы
    public $messageDisabledAppType = 'task.scoringId.disabledAppType';

    // Тип формы, указанный в скоринговой заявке, не поддерживается выбранной методикой
    public $messageUnsupportedAppType = 'task.scoringId.unsupportedAppType';

    // Для статуса, указанного в скоринговой заявке, запрещено создавать задания на расчет
    public $messageBadStatus = 'task.scoringId.badStatus';

    /**
     * {@inheritDoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
