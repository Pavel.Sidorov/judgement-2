<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use AppBundle\Analysis\Task\TaskInterface;
use AppBundle\Analysis\Package\PackageManagerInterface;
use AppBundle\Integration\Scoring\ScoringInterface;
use AppBundle\Analysis\Analysis\AnalysisInterface;

/**
 * Применимость методики для скоринговой заявки
 */
class AnalysisForScoringAppValidator extends ConstraintValidator
{
    /**
     * Менеджер пакетов методик
     *
     * @var PackageManagerInterface
     */
    private $packageManager;

    /**
     * Scoring
     *
     * @var ScoringInterface
     */
    private $scoring;

    /**
     * Конструктор
     *
     * @param PackageManagerInterface $packageManager
     * @param ScoringInterface $scoring
     */
    public function __construct(PackageManagerInterface $packageManager,
            ScoringInterface $scoring)
    {
        $this->packageManager = $packageManager;
        $this->scoring = $scoring;
    }

    /**
     * {@inheritDoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof AnalysisForScoringApp) {
            throw new UnexpectedTypeException($constraint, AnalysisForScoringApp::class);
        }

        if ($value === null) {
            return;
        }

        if (!$value instanceof TaskInterface) {
            throw new UnexpectedTypeException($constraint, TaskInterface::class);
        }

        $aName = $value->getAnalysisName();
        $aType = $value->getAnalysisType();
        $aVersion = $value->getAnalysisVersion();
        $scoringId = $value->getScoringId();

        // Если методика и ID заявки не указаны - пропускаем проверку
        if ($aName === null && $aType === null && $aVersion === null && $scoringId === null) {
            return;
        }

        // Ищем выбранную методику
        try {
            $analysis = $this->packageManager
                ->getPackage($aName)
                ->getAnalysis($aType, $aVersion);
        } catch (\Exception $ex) {
            // Методика на найдена
            $this->context
                ->buildViolation($constraint->messageNoAnalysis)
                ->atPath('analysisVersion')
                ->addViolation();

            return;
        }

        // Ищем скоринговую заявку
        try {
            $appMeta = $this->scoring->getAppMeta($scoringId);
        } catch (\Exception $ex) {
            // Ошибка работы со скорингом
            $this->context
                ->buildViolation($constraint->messageScoringError)
                ->atPath('scoringId')
                ->addViolation();

            return;
        }

        if ($appMeta === null) {
            // Заявка не найдена
            $this->context
                ->buildViolation($constraint->messageNoApp)
                ->atPath('scoringId')
                ->addViolation();

            return;
        }

        // Методика должна иметь статус "действующая"
        if ($analysis->getStatus() !== AnalysisInterface::STATUS_ACTIVE) {
            $this->context
                ->buildViolation($constraint->messageArchivalAnalysis)
                ->atPath('analysisVersion')
                ->addViolation();

            return;
        }

        // Тип формы скоринговой заявки должен существовать
        $appType = $appMeta->getAppType();
        if ($appType === null) {
            $this->context
                ->buildViolation($constraint->messageNoAppType)
                ->atPath('scoringId')
                ->addViolation();

            return;
        }

        // Тип формы скоринговой заявки должен быть действующим
        if (!$appType->isActive()) {
            $this->context
                ->buildViolation($constraint->messageDisabledAppType)
                ->setParameter('%appTypeId%', $appType->getId())
                ->setParameter('%appTypeTitle%', $appType->getTitle())
                ->atPath('scoringId')
                ->addViolation();

            return;
        }

        // Тип формы скоринговой заявки должен поддерживаться методикой
        if (!in_array($appType->getId(), $analysis->getScoringAppTypes(), true)) {
            $this->context
                ->buildViolation($constraint->messageUnsupportedAppType)
                ->setParameter('%appTypeId%', $appType->getId())
                ->setParameter('%appTypeTitle%', $appType->getTitle())
                ->setParameter('%supportedIds%', implode(', ', $analysis->getScoringAppTypes()))
                ->atPath('scoringId')
                ->addViolation();

            return;
        }

        // Скоринговая заявка должна находиться в допустимом статусе
        if (!in_array($appMeta->getStatus(), $analysis->getScoringStatuses(), true)) {
            $this->context
                ->buildViolation($constraint->messageBadStatus)
                ->setParameter('%statusId%', $appMeta->getStatus())
                ->setParameter('%statusTitle%', $appMeta->getStatusMessage())
                ->setParameter('%supportedIds%', implode(', ', $analysis->getScoringStatuses()))
                ->atPath('scoringId')
                ->addViolation();
        }
    }
}
