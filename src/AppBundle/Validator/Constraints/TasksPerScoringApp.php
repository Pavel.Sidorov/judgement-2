<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Количество заданий на скоринговую заявку
 *
 * @Annotation
 */
class TasksPerScoringApp extends Constraint
{
    public $message = 'task.scoringId.tasksPerScoringApp';

    /**
     * {@inheritDoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
