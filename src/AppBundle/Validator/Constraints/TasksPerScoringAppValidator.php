<?php

namespace AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use AppBundle\Analysis\Task\TaskInterface;
use AppBundle\Analysis\Analysis\AnalysisInterface;

/**
 * Количество заданий на скоринговую заявку
 */
class TasksPerScoringAppValidator extends ConstraintValidator
{
    /**
     * Doctrine entity manager
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof TasksPerScoringApp) {
            throw new UnexpectedTypeException($constraint, TasksPerScoringApp::class);
        }

        if ($value === null) {
            return;
        }

        if (!$value instanceof TaskInterface) {
            throw new UnexpectedTypeException($constraint, TaskInterface::class);
        }

        // Если scoringId не указан - пропускаем проверку
        if ($value->getScoringId() === null) {
            return;
        }

        // Для одного scoringId не может быть более одного primary задания.
        // Копии заданий не учитываются.
        if (($value->getAnalysisType() === AnalysisInterface::TYPE_PRIMARY)
                && ($value->getParent() === null)) {
            $count = $this->entityManager
                ->getRepository('AppBundle:Task')
                ->countRootTasks(
                    $value->getAnalysisName(),
                    AnalysisInterface::TYPE_PRIMARY,
                    $value->getScoringId(),
                    null,
                    $value->getId()
                );

            if ($count > 0) {
                $this->context
                    ->buildViolation($constraint->message)
                    ->atPath('scoringId')
                    ->addViolation();
            }
        }
    }
}
