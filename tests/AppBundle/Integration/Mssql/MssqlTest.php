<?php

namespace Tests\AppBundle\Integration\Mssql;

use PHPUnit\Framework\TestCase;
use AppBundle\Integration\Mssql\Mssql;

class DummyMssql extends Mssql
{
    public function getConnection(): \PDO
    {
        return parent::getConnection();
    }

    public function createConnection(string $dsn, string $username,
            string $password, string $pdoFqcn, array $initQueries): \PDO
    {
        return parent::createConnection($dsn, $username, $password,
                $pdoFqcn, $initQueries);
    }

    public function buildProcedureQuery(string $name, array $params): string
    {
        return parent::buildProcedureQuery($name, $params);
    }

    public function bindProcedureParams(\PDOStatement $statement,
            string $procedure, array $params)
    {
        parent::bindProcedureParams($statement, $procedure, $params);
    }

    public function fetchProcedureData(\PDOStatement $statement,
            string $procedure, array $cols): array
    {
        return parent::fetchProcedureData($statement, $procedure, $cols);
    }
}

class DummyPDO extends \PDO
{
    private $error = ['00000', '', ''];

    public function __construct(string $dsn, string $username = null, string $password = null, array $options = null)
    {
        if ($dsn === 'failed') {
            throw new \Exception();
        }
    }

    public function exec($statement)
    {
        if ($statement === 'failed') {
            $this->error = ['11111', '1', 'test'];

            return 0;
        } else {
            $this->error = ['00000', '', ''];

            return 1;
        }
    }

    public function errorCode()
    {
        return $this->error[0];
    }

    public function errorInfo()
    {
        return $this->error;
    }
}

/**
 * @group integration
 * @group integration.mssql
 */
class MssqlTest extends TestCase
{
    private $mssql;

    public function setUp()
    {
        parent::setUp();

        $this->mssql = $this->getMockBuilder(DummyMssql::class)
            ->setMethods(['__construct'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testBuildProcedureQuery()
    {
        $name = 'procedureName';
        $params = ['test1', 'test2'];
        $expected = 'EXEC procedureName @test1 = :test1, @test2 = :test2';

        $this->assertEquals($expected, $this->mssql->buildProcedureQuery($name, $params));
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testBuildProcedureQueryBadName()
    {
        $name = 'неверно';
        $params = ['test1', 'test2'];

        $this->mssql->buildProcedureQuery($name, $params);
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testBuildProcedureQueryBadParam()
    {
        $name = 'procedureName';
        $params = ['неверно', 'test2'];

        $this->mssql->buildProcedureQuery($name, $params);
    }

    public function testBindProcedureParams()
    {
        $st = $this->getMockBuilder(\PDOStatement::class)
            ->setMethods(['bindValue'])
            ->disableOriginalConstructor()
            ->getMock();

        $st->expects($this->exactly(4))
            ->method('bindValue')
            ->withConsecutive(
                [$this->equalTo(':test1'), $this->equalTo(null), $this->equalTo(\PDO::PARAM_NULL)],
                [$this->equalTo(':test2'), $this->equalTo(true), $this->equalTo(\PDO::PARAM_BOOL)],
                [$this->equalTo(':test3'), $this->equalTo(42), $this->equalTo(\PDO::PARAM_INT)],
                [$this->equalTo(':test4'), $this->equalTo('string'), $this->equalTo(\PDO::PARAM_STR)]
            );

        $name = 'procedureName';
        $params = [
            'test1' => null,
            'test2' => true,
            'test3' => 42,
            'test4' => 'string',
        ];

        $this->mssql->bindProcedureParams($st, $name, $params);
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testBindProcedureParamsBadType()
    {
        $st = $this->getMockBuilder(\PDOStatement::class)
            ->setMethods(['bindValue'])
            ->disableOriginalConstructor()
            ->getMock();

        $st->expects($this->any())
            ->method('bindValue')
            ->will($this->returnValue(true));

        $name = 'procedureName';
        $params = [new \DateTime(), 'test2'];

        $this->mssql->bindProcedureParams($st, $name, $params);
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testBindProcedureParamsFailed()
    {
        $st = $this->getMockBuilder(\PDOStatement::class)
            ->setMethods(['bindValue'])
            ->disableOriginalConstructor()
            ->getMock();

        $st->expects($this->any())
            ->method('bindValue')
            ->will($this->returnValue(false));

        $name = 'procedureName';
        $params = ['test1', 'test2'];

        $this->mssql->bindProcedureParams($st, $name, $params);
    }

    public function testFetchProcedureData()
    {
        $st = $this->getMockBuilder(\PDOStatement::class)
            ->setMethods(['fetch'])
            ->disableOriginalConstructor()
            ->getMock();

        $st->expects($this->exactly(3))
            ->method('fetch')
            ->with(\PDO::FETCH_ASSOC)
            ->will($this->onConsecutiveCalls(
                [
                    'col1' => 'value1-1',
                    'col2' => 'value2-1',
                    'col3' => 'value3-1',
                ],
                [
                    'col1' => 'value1-2',
                    'col2' => 'value2-2',
                    'col3' => 'value3-2',
                ],
                false
            ));

        $name = 'procedureName';
        $cols = ['col1', 'col2'];

        $result = $this->mssql->fetchProcedureData($st, $name, $cols);
        $this->assertEquals([
            ['col1' => 'value1-1', 'col2' => 'value2-1'],
            ['col1' => 'value1-2', 'col2' => 'value2-2'],
        ], $result);
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testFetchProcedureDataNoColumn()
    {
        $st = $this->getMockBuilder(\PDOStatement::class)
            ->setMethods(['fetch'])
            ->disableOriginalConstructor()
            ->getMock();

        $st->expects($this->once())
            ->method('fetch')
            ->with(\PDO::FETCH_ASSOC)
            ->will($this->returnValue([['col1' => 'value1-1']]));

        $name = 'procedureName';
        $cols = ['unknown'];

        $this->mssql->fetchProcedureData($st, $name, $cols);
    }

    public function testCreateConnection()
    {
        $pdo = $this->mssql->createConnection('dsn', 'user', 'pass',
                DummyPDO::class, ['query']);
        $this->assertInstanceOf(\PDO::class, $pdo);
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testCreateConnectionWrongClass()
    {
        $this->mssql->createConnection('dsn', 'user', 'pass',
            \DateTime::class, ['query']);
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testCreateConnectionFailed()
    {
        $this->mssql->createConnection('failed', 'user', 'pass',
            DummyPDO::class, ['query']);
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testCreateConnectionConfigFailed()
    {
        $this->mssql->createConnection('dsn', 'user', 'pass',
            DummyPDO::class, ['failed']);
    }

    public function testGetConnection()
    {
        $pdo = new DummyPDO('dsn');

        $mssql = $this->getMockBuilder(DummyMssql::class)
            ->setMethods(['createConnection'])
            ->setConstructorArgs(['dsn', 'username', 'password', 'cp1251'])
            ->getMock();

        $mssql->expects($this->once())
            ->method('createConnection')
            ->with(
                $this->equalTo('dsn'),
                $this->equalTo('username'),
                $this->equalTo('password'),
                $this->equalTo(\PDO::class),
                $this->equalTo([
                    'SET ANSI_WARNINGS ON',
                    'SET ANSI_PADDING ON',
                    'SET ANSI_NULLS ON',
                    'SET QUOTED_IDENTIFIER ON',
                    'SET CONCAT_NULL_YIELDS_NULL ON',
                ])
            )
            ->will($this->returnValue($pdo));

        $this->assertEquals($pdo, $mssql->getConnection());
        $this->assertEquals($pdo, $mssql->getConnection());
    }

    public function testExecProcedure()
    {
        $st = $this->getMockBuilder(\PDOStatement::class)
            ->setMethods(['execute'])
            ->disableOriginalConstructor()
            ->getMock();

        $st->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(true));

        $pdo = $this->getMockBuilder(\PDO::class)
            ->setMethods(['prepare'])
            ->disableOriginalConstructor()
            ->getMock();

        $pdo->expects($this->once())
            ->method('prepare')
            ->with($this->equalTo('query'))
            ->will($this->returnValue($st));

        $mssql = $this->getMockBuilder(DummyMssql::class)
            ->setMethods(['getConnection', 'buildProcedureQuery', 'bindProcedureParams', 'fetchProcedureData'])
            ->setConstructorArgs(['dsn', 'username', 'password', 'cp1251'])
            ->getMock();

        $mssql->expects($this->once())
            ->method('getConnection')
            ->will($this->returnValue($pdo));

        $mssql->expects($this->once())
            ->method('buildProcedureQuery')
            ->with($this->equalTo('name'), $this->equalTo(['key1', 'key2']))
            ->will($this->returnValue('query'));

        $mssql->expects($this->once())
            ->method('bindProcedureParams')
            ->with($this->equalTo($st), $this->equalTo('name'), $this->equalTo(['key1' => 'value1', 'key2' => 'value2']));

        $mssql->expects($this->once())
            ->method('fetchProcedureData')
            ->with($this->equalTo($st), $this->equalTo('name'), $this->equalTo(['key1']))
            ->will($this->returnValue([['key1' => 'result1']]));

        $result = $mssql->execProcedure('name', ['key1' => 'value1', 'key2' => 'value2'], ['key1']);
        $this->assertEquals([['key1' => 'result1']], $result);
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testExecProcedurePrepareFailed()
    {
        $pdo = $this->getMockBuilder(\PDO::class)
            ->setMethods(['prepare'])
            ->disableOriginalConstructor()
            ->getMock();

        $pdo->expects($this->once())
            ->method('prepare')
            ->with($this->equalTo('query'))
            ->will($this->throwException(new \Exception()));

        $mssql = $this->getMockBuilder(DummyMssql::class)
            ->setMethods(['getConnection', 'buildProcedureQuery', 'bindProcedureParams', 'fetchProcedureData'])
            ->setConstructorArgs(['dsn', 'username', 'password', 'cp1251'])
            ->getMock();

        $mssql->expects($this->once())
            ->method('getConnection')
            ->will($this->returnValue($pdo));

        $mssql->expects($this->once())
            ->method('buildProcedureQuery')
            ->with($this->equalTo('name'), $this->equalTo(['key1', 'key2']))
            ->will($this->returnValue('query'));

        $mssql->execProcedure('name', ['key1' => 'value1', 'key2' => 'value2'], ['key1']);
    }

    /**
     * @expectedException AppBundle\Integration\Mssql\MssqlException
     */
    public function testExecProcedureExecutionFailed()
    {
        $st = $this->getMockBuilder(\PDOStatement::class)
            ->setMethods(['execute', 'errorInfo'])
            ->disableOriginalConstructor()
            ->getMock();

        $st->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(false));

        $st->expects($this->once())
            ->method('errorInfo')
            ->will($this->returnValue(['sql', 'code', 'msg']));

        $pdo = $this->getMockBuilder(\PDO::class)
            ->setMethods(['prepare'])
            ->disableOriginalConstructor()
            ->getMock();

        $pdo->expects($this->once())
            ->method('prepare')
            ->with($this->equalTo('query'))
            ->will($this->returnValue($st));

        $mssql = $this->getMockBuilder(DummyMssql::class)
            ->setMethods(['getConnection', 'buildProcedureQuery', 'bindProcedureParams', 'fetchProcedureData'])
            ->setConstructorArgs(['dsn', 'username', 'password', 'cp1251'])
            ->getMock();

        $mssql->expects($this->once())
            ->method('getConnection')
            ->will($this->returnValue($pdo));

        $mssql->expects($this->once())
            ->method('buildProcedureQuery')
            ->with($this->equalTo('name'), $this->equalTo(['key1', 'key2']))
            ->will($this->returnValue('query'));

        $mssql->expects($this->once())
            ->method('bindProcedureParams')
            ->with($this->equalTo($st), $this->equalTo('name'), $this->equalTo(['key1' => 'value1', 'key2' => 'value2']));

        $mssql->execProcedure('name', ['key1' => 'value1', 'key2' => 'value2'], ['key1']);
    }
}
