<?php

namespace Tests\AppBundle\Integration\Scoring\AppData\Answer;

use PHPUnit\Framework\TestCase;
use AppBundle\Integration\Scoring\AppData\Answer\Answer;

/**
 * @group integration
 * @group integration.scoring
 * @group integration.scoring.appdata
 * @group integration.scoring.appdata.answer
 */
class AnswerTest extends TestCase
{
    public function testMethods()
    {
        $obj = new Answer(42, 'type', 'question', 'value1^value2^');

        $this->assertEquals(42, $obj->getId());
        $this->assertEquals('type', $obj->getType());
        $this->assertEquals('question', $obj->getQuestion());
        $this->assertEquals('value1^value2^', $obj->getValue());
        $this->assertEquals(['value1', 'value2'], $obj->getArrayValue());
    }
}
