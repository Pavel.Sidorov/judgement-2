<?php

namespace Tests\AppBundle\Integration\Scoring\AppData;

use PHPUnit\Framework\TestCase;
use AppBundle\Integration\Scoring\AppData\AppData;
use AppBundle\Integration\Scoring\AppData\Answer\Answer;

/**
 * @group integration
 * @group integration.scoring
 * @group integration.scoring.appdata
 */
class AppDataTest extends TestCase
{
    public function testMethods()
    {
        $a11 = new Answer(11, 'type1', 'question1', 'value1');
        $a12 = new Answer(12, 'type2', 'question2', 'value2');

        $data = new AppData(42, [$a11, $a12]);

        $this->assertEquals(42, $data->getId());
        $this->assertEquals([11 => $a11, 12 => $a12], $data->getAnswers());
        $this->assertEquals($a11, $data->getAnswerByQuestionId(11));
        $this->assertEquals($a12, $data->getAnswerByQuestionId(12));
        $this->assertEquals(null, $data->getAnswerByQuestionId(13));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testWrongAnswer()
    {
        $data = new AppData(42, ['wrong']);
    }
}
