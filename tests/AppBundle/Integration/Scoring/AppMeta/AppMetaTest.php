<?php

namespace Tests\AppBundle\Integration\Scoring\AppMeta;

use PHPUnit\Framework\TestCase;
use AppBundle\Integration\Scoring\AppMeta\AppMeta;
use AppBundle\Integration\Scoring\AppType\AppType;
use AppBundle\Integration\Scoring\Office\Office;

/**
 * @group integration
 * @group integration.scoring
 * @group integration.scoring.appmeta
 */
class AppMetaTest extends TestCase
{
    public function testMethods()
    {
        $appType = new AppType(1, 'title', true);
        $office = new Office(1, 'title', true);

        $obj = new AppMeta(42, $appType, 43, 'status', $office, ['extra' => 44]);

        $this->assertEquals(42, $obj->getId());
        $this->assertEquals($appType, $obj->getAppType());
        $this->assertEquals(43, $obj->getStatus());
        $this->assertEquals('status', $obj->getStatusMessage());
        $this->assertEquals($office, $obj->getOffice());
        $this->assertEquals(['extra' => 44], $obj->getExtra());
        $this->assertEquals(44, $obj->getExtraValue('extra'));
        $this->assertEquals(null, $obj->getExtraValue('unknown'));
    }
}
