<?php

namespace Tests\AppBundle\Integration\Scoring\AppType;

use PHPUnit\Framework\TestCase;
use AppBundle\Integration\Scoring\AppType\AppType;

/**
 * @group integration
 * @group integration.scoring
 * @group integration.scoring.apptype
 */
class AppTypeTest extends TestCase
{
    public function testMethods()
    {
        $obj = new AppType(42, 'title', true);

        $this->assertEquals(42, $obj->getId());
        $this->assertEquals('title', $obj->getTitle());
        $this->assertEquals(true, $obj->isActive());
    }
}
