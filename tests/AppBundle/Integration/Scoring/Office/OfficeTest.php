<?php

namespace Tests\AppBundle\Integration\Scoring\Office;

use PHPUnit\Framework\TestCase;
use AppBundle\Integration\Scoring\Office\Office;

/**
 * @group integration
 * @group integration.scoring
 * @group integration.scoring.office
 */
class OfficeTest extends TestCase
{
    public function testMethods()
    {
        $obj = new Office(42, 'title', true);

        $this->assertEquals(42, $obj->getId());
        $this->assertEquals('title', $obj->getTitle());
        $this->assertEquals(true, $obj->isActive());
    }
}
