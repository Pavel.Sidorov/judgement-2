<?php

namespace Tests\AppBundle\Integration\Scoring;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use AppBundle\Integration\Scoring\Scoring;
use AppBundle\Integration\Mssql\Mssql;
use AppBundle\Integration\Scoring\Office\OfficeInterface;
use AppBundle\Integration\Scoring\Office\Office;
use AppBundle\Integration\Scoring\AppType\AppTypeInterface;
use AppBundle\Integration\Scoring\AppType\AppType;
use AppBundle\Integration\Scoring\AppMeta\AppMetaInterface;
use AppBundle\Integration\Scoring\AppData\AppDataInterface;

class DummyScoring extends Scoring
{
    public function readOffices(): array
    {
        return parent::readOffices();
    }

    public function readAppTypes(): array
    {
        return parent::readAppTypes();
    }
}

/**
 * @group integration
 * @group integration.scoring
 */
class ScoringTest extends TestCase
{
    private $mssql;
    private $cache;

    public function setUp()
    {
        parent::setUp();

        $this->mssql = $this->getMockBuilder(Mssql::class)
            ->setMethods(['execProcedure'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->cache = new ArrayAdapter();
    }

    public function testReadOffices()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => null, 'num' => 1]),
                $this->equalTo(['id', 'title', 'closed'])
            )
            ->will($this->returnValue([
                ['id' => '1', 'title' => 'title1', 'closed' => '0'],
                ['id' => '2', 'title' => 'title2', 'closed' => '1'],
            ]));

        $scoring = new DummyScoring($this->mssql, $this->cache, 60);
        $res = $scoring->readOffices();

        $this->assertEquals(2, count($res));
        $this->assertEquals(1, $res[1]->getId());
        $this->assertEquals(2, $res[2]->getId());
        $this->assertEquals('title1', $res[1]->getTitle());
        $this->assertEquals('title2', $res[2]->getTitle());
        $this->assertEquals(true, $res[1]->isActive());
        $this->assertEquals(false, $res[2]->isActive());
    }

    /**
     * @expectedException AppBundle\Integration\Scoring\ScoringException
     */
    public function testReadOfficesFailed()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => null, 'num' => 1]),
                $this->equalTo(['id', 'title', 'closed'])
            )
            ->will($this->throwException(new \Exception()));

        $scoring = new DummyScoring($this->mssql, $this->cache, 60);
        $scoring->readOffices();
    }

    public function testGetOffices()
    {
        $data = [
            1 => new Office(1, 'title1', true),
            2 => new Office(2, 'title2', false),
        ];

        $scoring = $this->getMockBuilder(DummyScoring::class)
            ->setMethods(['readOffices'])
            ->setConstructorArgs([$this->mssql, $this->cache, 60])
            ->getMock();

        $scoring->expects($this->once())
            ->method('readOffices')
            ->will($this->returnValue($data));

        $res = $scoring->getOffices();

        $this->assertEquals(2, count($res));
        $this->assertEquals(1, $res[1]->getId());
        $this->assertEquals(2, $res[2]->getId());
        $this->assertEquals('title1', $res[1]->getTitle());
        $this->assertEquals('title2', $res[2]->getTitle());
        $this->assertEquals(true, $res[1]->isActive());
        $this->assertEquals(false, $res[2]->isActive());

        $res2 = $scoring->getOffices();
        $this->assertEquals($res, $res2);
    }

    public function testGetActiveOffices()
    {
        $data = [
            1 => new Office(1, 'title1', true),
            2 => new Office(2, 'title2', false),
        ];

        $scoring = $this->getMockBuilder(Scoring::class)
            ->setMethods(['getOffices'])
            ->disableOriginalConstructor()
            ->getMock();

        $scoring->expects($this->any())
            ->method('getOffices')
            ->will($this->returnValue($data));

        $res = $scoring->getActiveOffices();

        $this->assertEquals(1, count($res));
        $this->assertEquals(1, $res[1]->getId());
        $this->assertEquals('title1', $res[1]->getTitle());
        $this->assertEquals(true, $res[1]->isActive());
    }

    public function testGetOfficeById()
    {
        $data = [
            1 => new Office(1, 'title1', true),
            2 => new Office(2, 'title2', false),
        ];

        $scoring = $this->getMockBuilder(Scoring::class)
            ->setMethods(['getOffices'])
            ->disableOriginalConstructor()
            ->getMock();

        $scoring->expects($this->any())
            ->method('getOffices')
            ->will($this->returnValue($data));

        $res = $scoring->getOfficeById(1);

        $this->assertInstanceOf(OfficeInterface::class, $res);
        $this->assertEquals(1, $res->getId());
        $this->assertEquals('title1', $res->getTitle());
        $this->assertEquals(true, $res->isActive());

        $res2 = $scoring->getOfficeById(2);

        $this->assertInstanceOf(OfficeInterface::class, $res2);
        $this->assertEquals(2, $res2->getId());
        $this->assertEquals('title2', $res2->getTitle());
        $this->assertEquals(false, $res2->isActive());

        $this->assertNull($scoring->getOfficeById(101));
    }

    public function testReadAppTypes()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => null, 'num' => 4]),
                $this->equalTo(['id', 'title', 'is_active'])
            )
            ->will($this->returnValue([
                ['id' => '1', 'title' => 'title1', 'is_active' => '1'],
                ['id' => '2', 'title' => 'title2', 'is_active' => '0'],
            ]));

        $scoring = new DummyScoring($this->mssql, $this->cache, 60);
        $res = $scoring->readAppTypes();

        $this->assertEquals(2, count($res));
        $this->assertEquals(1, $res[1]->getId());
        $this->assertEquals(2, $res[2]->getId());
        $this->assertEquals('title1', $res[1]->getTitle());
        $this->assertEquals('title2', $res[2]->getTitle());
        $this->assertEquals(true, $res[1]->isActive());
        $this->assertEquals(false, $res[2]->isActive());
    }

    /**
     * @expectedException AppBundle\Integration\Scoring\ScoringException
     */
    public function testReadAppTypesFailed()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => null, 'num' => 4]),
                $this->equalTo(['id', 'title', 'is_active'])
            )
            ->will($this->throwException(new \Exception()));

        $scoring = new DummyScoring($this->mssql, $this->cache, 60);
        $scoring->readAppTypes();
    }

    public function testGetAppTypes()
    {
        $data = [
            1 => new AppType(1, 'title1', true),
            2 => new AppType(2, 'title2', false),
        ];

        $scoring = $this->getMockBuilder(DummyScoring::class)
            ->setMethods(['readAppTypes'])
            ->setConstructorArgs([$this->mssql, $this->cache, 60])
            ->getMock();

        $scoring->expects($this->once())
            ->method('readAppTypes')
            ->will($this->returnValue($data));

        $res = $scoring->getAppTypes();

        $this->assertEquals(2, count($res));
        $this->assertEquals(1, $res[1]->getId());
        $this->assertEquals(2, $res[2]->getId());
        $this->assertEquals('title1', $res[1]->getTitle());
        $this->assertEquals('title2', $res[2]->getTitle());
        $this->assertEquals(true, $res[1]->isActive());
        $this->assertEquals(false, $res[2]->isActive());

        $res2 = $scoring->getAppTypes();
        $this->assertEquals($res, $res2);
    }

    public function testGetActiveAppTypes()
    {
        $data = [
            1 => new AppType(1, 'title1', true),
            2 => new AppType(2, 'title2', false),
        ];

        $scoring = $this->getMockBuilder(Scoring::class)
            ->setMethods(['getAppTypes'])
            ->disableOriginalConstructor()
            ->getMock();

        $scoring->expects($this->any())
            ->method('getAppTypes')
            ->will($this->returnValue($data));

        $res = $scoring->getActiveAppTypes();

        $this->assertEquals(1, count($res));
        $this->assertEquals(1, $res[1]->getId());
        $this->assertEquals('title1', $res[1]->getTitle());
        $this->assertEquals(true, $res[1]->isActive());
    }

    public function testGetAppTypeById()
    {
        $data = [
            1 => new AppType(1, 'title1', true),
            2 => new AppType(2, 'title2', false),
        ];

        $scoring = $this->getMockBuilder(Scoring::class)
            ->setMethods(['getAppTypes'])
            ->disableOriginalConstructor()
            ->getMock();

        $scoring->expects($this->any())
            ->method('getAppTypes')
            ->will($this->returnValue($data));

        $res = $scoring->getAppTypeById(1);

        $this->assertInstanceOf(AppTypeInterface::class, $res);
        $this->assertEquals(1, $res->getId());
        $this->assertEquals('title1', $res->getTitle());
        $this->assertEquals(true, $res->isActive());

        $res2 = $scoring->getAppTypeById(2);

        $this->assertInstanceOf(AppTypeInterface::class, $res2);
        $this->assertEquals(2, $res2->getId());
        $this->assertEquals('title2', $res2->getTitle());
        $this->assertEquals(false, $res2->isActive());

        $this->assertNull($scoring->getAppTypeById(101));
    }

    public function testGetAppMeta()
    {
        $appType = new AppType('44', 'title44', true);
        $office = new Office('46', 'title46', true);

        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => 42, 'num' => 3]),
                $this->equalTo(['fio', 'ul_name', 'id_client_ubs', 'anketa_type_id',
                    'summa', 'userfio', 'num_division', 'state_id', 'state_name',
                    'client_type', 'prod_id', 'prod_name', 'type_of_issue_id',
                    'type_of_issue', 'related_companies_cnt'])
            )
            ->will($this->returnValue([[
                'fio' => 'client',
                'ul_name' => 'company',
                'id_client_ubs' => 43,
                'client_type' => 'ul',
                'anketa_type_id' => 44,
                'summa' => 45,
                'userfio' => 'inspector',
                'num_division' => 46,
                'state_id' => 47,
                'state_name' => 'status',
                'prod_id' => 48,
                'prod_name' => 'product',
                'type_of_issue_id' => 49,
                'type_of_issue' => 'issuetype',
                'related_companies_cnt' => 50,
            ]]));

        $scoring = $this->getMockBuilder(DummyScoring::class)
            ->setMethods(['getAppTypeById', 'getOfficeById'])
            ->setConstructorArgs([$this->mssql, $this->cache, 60])
            ->getMock();

        $scoring->expects($this->once())
            ->method('getAppTypeById')
            ->with($this->equalTo(44))
            ->will($this->returnValue($appType));

        $scoring->expects($this->once())
            ->method('getOfficeById')
            ->with($this->equalTo(46))
            ->will($this->returnValue($office));

        $res = $scoring->getAppMeta(42);

        $this->assertInstanceOf(AppMetaInterface::class, $res);
        $this->assertEquals(42, $res->getId());
        $this->assertEquals($appType, $res->getAppType());
        $this->assertEquals(47, $res->getStatus());
        $this->assertEquals('status', $res->getStatusMessage());
        $this->assertEquals($office, $res->getOffice());
        $this->assertEquals([
            'inspectorName' => 'inspector',
            'clientType' => 'ul',
            'clientName' => 'company',
            'clientAbsId' => 43,
            'creditAmount' => 45,
            'productId' => 48,
            'productTitle' => 'product',
            'issueTypeId' => 49,
            'issueTypeTitle' => 'issuetype',
            'relatedCompaniesCount' => 50,
        ], $res->getExtra());
    }

    public function testGetAppMetaEmpty()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => 42, 'num' => 3]),
                $this->equalTo(['fio', 'ul_name', 'id_client_ubs', 'anketa_type_id',
                    'summa', 'userfio', 'num_division', 'state_id', 'state_name',
                    'client_type', 'prod_id', 'prod_name', 'type_of_issue_id',
                    'type_of_issue', 'related_companies_cnt'])
            )
            ->will($this->returnValue([]));

        $scoring = new Scoring($this->mssql, $this->cache, 60);
        $this->assertNull($scoring->getAppMeta(42));
    }

    /**
     * @expectedException AppBundle\Integration\Scoring\ScoringException
     */
    public function testGetAppMetaFailed()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => 42, 'num' => 3]),
                $this->equalTo(['fio', 'ul_name', 'id_client_ubs', 'anketa_type_id',
                    'summa', 'userfio', 'num_division', 'state_id', 'state_name',
                    'client_type', 'prod_id', 'prod_name', 'type_of_issue_id',
                    'type_of_issue', 'related_companies_cnt'])
            )
            ->will($this->throwException(new \Exception()));

        $scoring = new Scoring($this->mssql, $this->cache, 60);
        $scoring->getAppMeta(42);
    }

    public function testGetAppData()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => 42, 'num' => 2]),
                $this->equalTo(['q_id', 'q_type', 'q_title', 'q_value'])
            )
            ->will($this->returnValue([
                [
                    'q_id' => 43,
                    'q_type' => 'type43',
                    'q_title' => 'question43',
                    'q_value' => 'value43',
                ],
                [
                    'q_id' => 44,
                    'q_type' => 'type44',
                    'q_title' => 'question44',
                    'q_value' => 'value44',
                ],
            ]));

        $scoring = new Scoring($this->mssql, $this->cache, 60);
        $res = $scoring->getAppData(42);

        $this->assertInstanceOf(AppDataInterface::class, $res);
        $this->assertEquals(42, $res->getId());

        $ans43 = $res->getAnswerByQuestionId(43);
        $this->assertEquals(43, $ans43->getId());
        $this->assertEquals('type43', $ans43->getType());
        $this->assertEquals('question43', $ans43->getQuestion());
        $this->assertEquals('value43', $ans43->getValue());

        $ans44 = $res->getAnswerByQuestionId(44);
        $this->assertEquals(44, $ans44->getId());
        $this->assertEquals('type44', $ans44->getType());
        $this->assertEquals('question44', $ans44->getQuestion());
        $this->assertEquals('value44', $ans44->getValue());

        $this->assertEquals([43 => $ans43, 44 => $ans44], $res->getAnswers());
    }

    public function testGetAppDataEmpty()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => 42, 'num' => 2]),
                $this->equalTo(['q_id', 'q_type', 'q_title', 'q_value'])
            )
            ->will($this->returnValue([]));

        $scoring = new Scoring($this->mssql, $this->cache, 60);
        $this->assertNull($scoring->getAppData(42));
    }

    /**
     * @expectedException AppBundle\Integration\Scoring\ScoringException
     */
    public function testGetAppDataFailed()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('SP_JUDGMENT_GET_INFO'),
                $this->equalTo(['anketaId' => 42, 'num' => 2]),
                $this->equalTo(['q_id', 'q_type', 'q_title', 'q_value'])
            )
            ->will($this->throwException(new \Exception()));

        $scoring = new Scoring($this->mssql, $this->cache, 60);
        $scoring->getAppData(42);
    }

    public function testCustomProcedure()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('proc'),
                $this->equalTo(['key' => 'val']),
                $this->equalTo(['result'])
            )
            ->will($this->returnValue(['return']));

        $scoring = new Scoring($this->mssql, $this->cache, 60);
        $result = $scoring->customProcedure('proc', ['key' => 'val'], ['result']);

        $this->assertEquals(['return'], $result);
    }

    /**
     * @expectedException AppBundle\Integration\Scoring\ScoringException
     */
    public function testCustomProcedureFailed()
    {
        $this->mssql->expects($this->once())
            ->method('execProcedure')
            ->with(
                $this->equalTo('proc'),
                $this->equalTo(['key' => 'val']),
                $this->equalTo(['result'])
            )
            ->will($this->throwException(new \Exception()));

        $scoring = new Scoring($this->mssql, $this->cache, 60);
        $scoring->customProcedure('proc', ['key' => 'val'], ['result']);
    }
}
