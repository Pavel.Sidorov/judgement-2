<?php

namespace Tests\AppBundle\Product\Microcredit\Primary\V1;

use PHPUnit\Framework\TestCase;
use AppBundle\Product\Microcredit\Primary\V1\Helper;

/**
 * @group product
 * @group product.microcredit
 */
class HelperTest extends TestCase
{
    public function testClientAge()
    {
        $this->assertEquals(null, Helper::clientAge('32.01.1980'));

        $date = new \DateTime('today -20 years');
        $this->assertEquals(20, Helper::clientAge($date->format('d.m.Y')));
    }

    public function testCreditPurpose()
    {
        $this->assertEquals(null, Helper::creditPurpose([]));
        $this->assertEquals('replenishment', Helper::creditPurpose(['1. текст']));
        $this->assertEquals('investment', Helper::creditPurpose(['2. текст']));
        $this->assertEquals('investment', Helper::creditPurpose(['3. текст']));
        $this->assertEquals('consumption', Helper::creditPurpose(['4. текст']));
        $this->assertEquals('mixed', Helper::creditPurpose(['1. текст', '2. текст']));
        $this->assertEquals('mixed', Helper::creditPurpose(['3. текст', '4. текст']));
    }

    public function testСreditIssueType()
    {
        $map = [
            'once' => [30, 31, 59, 93, 102, 103, 105, 121, 122, 143, 144, 145, 205],
            'bvkl' => [94, 99],
            'vkl' => [98],
            'overdraft' => [100, 101, 117, 118, 208, 209, 210, 211, 212, 213, 214, 215, 216],
        ];

        foreach ($map as $type => $products) {
            foreach ($products as $product) {
                $this->assertEquals($type, Helper::creditIssueType($product));
            }
        }

        $this->assertEquals(null, Helper::creditIssueType(null));
        $this->assertEquals(null, Helper::creditIssueType(999));
    }

    public function testCreditPaymentType()
    {
        $this->assertEquals('annuity', Helper::creditPaymentType(1, 'any'));
        $this->assertEquals('individual', Helper::creditPaymentType(0, 'any'));
        $this->assertEquals('overdraft', Helper::creditPaymentType('any', 'overdraft'));
        $this->assertEquals('equalpart', Helper::creditPaymentType('any', 'any'));
    }

    public function testClientActivity()
    {
        $this->assertEquals('wholesaleTrading', Helper::clientActivity(['Оптовая торговля'], [], ''));
        $this->assertEquals('retailTrading', Helper::clientActivity(['Розничная торговля'], [], ''));
        $this->assertEquals('services', Helper::clientActivity([], ['Бытовые услуги'], ''));
        $this->assertEquals('production', Helper::clientActivity([], [], 'any'));
        $this->assertEquals(null, Helper::clientActivity([], [], ''));
    }

    public function testCompanyOwners()
    {
        $in = [11, 12, 13, 21, 22, 23, 31, 32, 33];
        $out = [
            ['name' => 11, 'share' => 13],
            ['name' => 21, 'share' => 23],
            ['name' => 31, 'share' => 33],
            ['name' => null, 'share' => null],
            ['name' => null, 'share' => null],
            ['name' => null, 'share' => null],
            ['name' => null, 'share' => null],
            ['name' => null, 'share' => null],
            ['name' => null, 'share' => null],
            ['name' => null, 'share' => null],
        ];
        $this->assertEquals($out, Helper::companyOwners($in));
    }

    public function testCompanyAddresses()
    {
        $in = [11, 12, 'Собственность', 13, 21, 22, 'Аренда', 23, 31, 32, 'Аренда', 33];
        $out = [
            ['address' => 11, 'square' => 12, 'status' => 'property'],
            ['address' => 21, 'square' => 22, 'status' => 'rent'],
            ['address' => 31, 'square' => 32, 'status' => 'rent'],
            ['address' => null, 'square' => null, 'status' => null],
            ['address' => null, 'square' => null, 'status' => null],
            ['address' => null, 'square' => null, 'status' => null],
        ];
        $this->assertEquals($out, Helper::companyAddresses($in));
    }

    public function testCompanyAge()
    {
        $this->assertEquals('3_6', Helper::companyAge('3-5 месяцев'));
        $this->assertEquals('6_12', Helper::companyAge('6-11 месяцев'));
        $this->assertEquals('12_36', Helper::companyAge('12-36 месяцев'));
        $this->assertEquals(null, Helper::companyAge('any'));
    }

    public function testCompanyAddressesCount()
    {
        $a = ['address' => 'test'];
        $this->assertEquals(null, Helper::companyAddressesCount([]));
        $this->assertEquals('1', Helper::companyAddressesCount([$a]));
        $this->assertEquals('2', Helper::companyAddressesCount([$a, $a]));
        $this->assertEquals('3_5', Helper::companyAddressesCount([$a, $a, $a, $a]));
        $this->assertEquals('5_', Helper::companyAddressesCount([$a, $a, $a, $a, $a, $a]));
    }

    public function testCompanyStaffCount()
    {
        $this->assertEquals(null, Helper::companyStaffCount(-1));
        $this->assertEquals('no', Helper::companyStaffCount(0));
        $this->assertEquals('1_2', Helper::companyStaffCount(1));
        $this->assertEquals('3_5', Helper::companyStaffCount(3));
        $this->assertEquals('5_10', Helper::companyStaffCount(5));
        $this->assertEquals('10_', Helper::companyStaffCount(10));
    }

    public function testPropertyCreditRatio()
    {
        $this->assertEquals(null, Helper::propertyCreditRatio(null, 42));
        $this->assertEquals(null, Helper::propertyCreditRatio(42, null));
        $this->assertEquals(null, Helper::propertyCreditRatio(42, 0));
        $this->assertEquals('0_5', Helper::propertyCreditRatio(4, 100));
        $this->assertEquals('5_40', Helper::propertyCreditRatio(5, 100));
        $this->assertEquals('40_70', Helper::propertyCreditRatio(41, 100));
        $this->assertEquals('70_100', Helper::propertyCreditRatio(71, 100));
        $this->assertEquals('100_', Helper::propertyCreditRatio(101, 100));
    }

    public function testCompanyAssets()
    {
        $in = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        $out = [
            'cash' => 0,
            'bankAccount' => 1,
            'deposits' => 2,
            'receivableAccounts' => 3,
            'other' => null,
            'prepaid' => 4,
            'materials' => 5,
            'products' => 6,
            'goods' => 7,
            'goodsShipped' => null, // Товары отгруженные (реализация)
            'equipment' => 8,
            'vehicles' => 9,
            'realEstate' => 10,
            'loans' => 11,
        ];
        $this->assertEquals($out, Helper::companyAssets($in));
    }

    public function testCompanyLiabilities()
    {
        $in = [0, 1, 2, 3, 4, 5, 6, 7];
        $out = [
            'budget' => 0,
            'wage' => 1,
            'rent' => 2,
            'payableAccounts' => 3,
            'advances' => 4,
            'loansPbank' => null,
            'loans' => 5,
            'borrowings' => 6,
            'longTermLoans' => null,
            'other' => 7,
        ];
        $this->assertEquals($out, Helper::companyLiabilities($in));
    }

    public function testProvision()
    {
        $in = [
            'Автотранспорт', 12, 13, 14, 15,
            'Недвижимость', 22, 23, 24, 25,
            'Оборудование/Спецтехника', 32, 33, 34, 35,
            'ТМЦ', 42, 43, 44, 45,
        ];
        $tmp = [
            'type' => null,
            'description' => null,
            'name' => null,
            'phone' => null,
            'price' => null,
            'address' => null,
            'infoSource' => null,
            'discount' => null,
            'category' => null,
            'collateralValue' => null,
        ];
        $out = [
            [
                'type' => 'vehicle',
                'description' => 12,
                'name' => 13,
                'phone' => 14,
                'price' => 15,
                'address' => null,
                'infoSource' => null,
                'discount' => null,
                'category' => null,
                'collateralValue' => null,
            ],
            [
                'type' => 'realEstate',
                'description' => 22,
                'name' => 23,
                'phone' => 24,
                'price' => 25,
                'address' => null,
                'infoSource' => null,
                'discount' => null,
                'category' => null,
                'collateralValue' => null,
            ],
            [
                'type' => 'equipment',
                'description' => 32,
                'name' => 33,
                'phone' => 34,
                'price' => 35,
                'address' => null,
                'infoSource' => null,
                'discount' => null,
                'category' => null,
                'collateralValue' => null,
            ],
            [
                'type' => 'products',
                'description' => 42,
                'name' => 43,
                'phone' => 44,
                'price' => 45,
                'address' => null,
                'infoSource' => null,
                'discount' => null,
                'category' => null,
                'collateralValue' => null,
            ],
            $tmp,
            $tmp,
            $tmp,
            $tmp,
            $tmp,
            $tmp,
            $tmp,
        ];
        $this->assertEquals($out, Helper::provision($in));
    }

    public function testGuarantee()
    {
        $in = [11, 12, 21, 22, 31, 32];
        $out = [
            ['name' => 11, 'phone' => 12, 'comment' => null],
            ['name' => 21, 'phone' => 22, 'comment' => null],
            ['name' => 31, 'phone' => 32, 'comment' => null],
            ['name' => null, 'phone' => null, 'comment' => null],
            ['name' => null, 'phone' => null, 'comment' => null],
        ];
        $this->assertEquals($out, Helper::guarantee($in));
    }

    public function testMaritalStatus()
    {
        $test = [
            42 => null,
            'Не выбрано' => null,
            'Женат/Замужем' => 'maried',
            'Гражданский брак' => 'civilMarriage',
            'В браке не состоял' => 'single',
            'В браке, проживаем отдельно' => 'maried',
            'Вдова/Вдовец' => 'widowed',
            'Разведен(а)' => 'divorced',
        ];

        foreach ($test as $arg => $res) {
            $this->assertEquals($res, Helper::maritalStatus($arg));
        }
    }

    public function testСlientDependentCount()
    {
        $test = [
            'any' => null,
            0 => 'no',
            1 => '1_2',
            2 => '1_2',
            3 => '3_4',
            4 => '3_4',
            5 => '5_',
            42 => '5_',
        ];

        foreach ($test as $arg => $res) {
            $this->assertEquals($res, Helper::clientDependentCount($arg));
        }
    }

}
