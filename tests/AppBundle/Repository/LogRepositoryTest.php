<?php

namespace Tests\AppBundle\Entity;

use Tests\AppTestCase;
use AppBundle\DataFixtures\ORM\LoadTestData;

/**
 * @group repository
 * @group repository.log
 */
class LogRepositoryTest extends AppTestCase
{
    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures(array(
            new LoadTestData(),
        ));
    }

    public function testMethods()
    {
        $repo = $this->getDoctrine()
                ->getRepository('AppBundle:Log');

        $date1 = new \DateTime('2017-01-01T00:00:00');
        $date2 = new \DateTime('2017-01-02T00:00:00');
        $date3 = new \DateTime('2017-01-03T00:00:00');
        $date4 = new \DateTime('2017-01-04T00:00:00');

        $this->assertEquals(3, $repo->countLogs());
        $this->assertEquals(2, $repo->countLogs($date2));
        $this->assertEquals(1, $repo->countLogs($date2, $date3));
        $this->assertEquals(2, $repo->countLogs($date2, $date4, 'username2'));

        $log1 = $repo->findOneByMessage('message1');
        $log2 = $repo->findOneByMessage('message2');
        $log3 = $repo->findOneByMessage('message3');

        $this->assertEquals([$log3, $log2, $log1], $repo->getLogs());
        $this->assertEquals([$log3, $log2], $repo->getLogs($date2));
        $this->assertEquals([$log2], $repo->getLogs($date2, $date3));
        $this->assertEquals([$log3, $log2], $repo->getLogs($date2, $date4, 'username2'));
        $this->assertEquals([$log2], $repo->getLogs(null, null, null, 1, 1));
    }
}
