<?php

namespace Tests\AppBundle\Entity;

use Tests\AppTestCase;
use AppBundle\DataFixtures\ORM\LoadTestData;

/**
 * @group repository
 * @group repository.task
 */
class TaskRepositoryTest extends AppTestCase
{
    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures(array(
            new LoadTestData(),
        ));
    }

    public function testMethods()
    {
        $repo = $this->getDoctrine()
                ->getRepository('AppBundle:Task');

        $primaryTask = $repo->findOneByDescription('primaryTask');

        $this->assertEquals(1, $repo->countRootTasks('test', 'primary', 111));
        $this->assertEquals(1, $repo->countRootTasks('test', 'primary', 111, 'client'));
        $this->assertEquals(0, $repo->countRootTasks('test', 'primary', 111, 'client', $primaryTask->getId()));
        $this->assertEquals(1, $repo->countRootTasks('test', 'secondary', 111));
        $this->assertEquals(2, $repo->countRootTasks('test'));
        $this->assertEquals(1, $repo->countRootTasks(null, 'primary'));
        $this->assertEquals(2, $repo->countRootTasks(null, null, 111));
        $this->assertEquals(2, $repo->countRootTasks());
        $this->assertEquals(1, $repo->countRootTasks(null, null, null, null, $primaryTask->getId()));

        $task1 = $repo->findOneByDescription('primaryTask');
        $task2 = $repo->findOneByDescription('secondaryTask');

        $this->assertEquals([$task1], $repo->getRootTasks('test', 'primary', 111));
        $this->assertEquals([$task1], $repo->getRootTasks('test', 'primary', 111, 'client'));
        $this->assertEquals([$task2], $repo->getRootTasks('test', 'secondary', 111));
        $this->assertEquals([$task2], $repo->getRootTasks('test', 'secondary', 111 ,'client'));
        $this->assertEquals([$task1, $task2], $repo->getRootTasks('test'));
        $this->assertEquals([$task1], $repo->getRootTasks(null, 'primary'));
        $this->assertEquals([$task1, $task2], $repo->getRootTasks(null, null, 111));
        $this->assertEquals([$task1, $task2], $repo->getRootTasks(null, null, 111, 'client'));
        $this->assertEquals([$task1, $task2], $repo->getRootTasks());
        $this->assertEquals([$task1], $repo->getRootTasks(null, null, null, null, 0, 1));
        $this->assertEquals([$task2], $repo->getRootTasks(null, null, null, null, 1, 1));
    }
}
