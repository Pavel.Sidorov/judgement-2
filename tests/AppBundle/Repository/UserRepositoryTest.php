<?php

namespace Tests\AppBundle\Entity;

use Tests\AppTestCase;
use AppBundle\DataFixtures\ORM\LoadTestData;

/**
 * @group repository
 * @group repository.user
 */
class UserRepositoryTest extends AppTestCase
{
    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures(array(
            new LoadTestData(),
        ));
    }

    public function testMethods()
    {
        $repo = $this->getDoctrine()
                ->getRepository('AppBundle:User');

        $this->assertEquals(1, $repo->countUsers(true, 'userNoRoles', 'userNoRolesName', 'userNoRolesEmail'));
        $this->assertEquals(3, $repo->countUsers(true));
        $this->assertEquals(1, $repo->countUsers(false));
        $this->assertEquals(1, $repo->countUsers(null, 'userNoRoles'));
        $this->assertEquals(1, $repo->countUsers(null, null, 'userNoRolesName'));
        $this->assertEquals(1, $repo->countUsers(null, null, null, 'userNoRolesEmail'));
        $this->assertEquals(4, $repo->countUsers());

        $noRoles = $repo->findOneByUsername('userNoRoles');
        $allRoles = $repo->findOneByUsername('userAllRoles');
        $allRolesAnalysis = $repo->findOneByUsername('userAllRolesAnalysis');
        $disabled = $repo->findOneByUsername('userDisabled');

        $this->assertEquals([$noRoles], $repo->getUsers(true, 'userNoRoles', 'userNoRolesName', 'userNoRolesEmail'));
        $this->assertEquals([$allRolesAnalysis, $allRoles, $noRoles], $repo->getUsers(true));
        $this->assertEquals([$disabled], $repo->getUsers(false));
        $this->assertEquals([$noRoles], $repo->getUsers(null, 'userNoRoles'));
        $this->assertEquals([$noRoles], $repo->getUsers(null, null, 'userNoRolesName'));
        $this->assertEquals([$noRoles], $repo->getUsers(null, null, null, 'userNoRolesEmail'));
        $this->assertEquals([$allRolesAnalysis, $allRoles, $disabled, $noRoles], $repo->getUsers());
        $this->assertEquals([$allRolesAnalysis], $repo->getUsers(null, null, null, null, 0, 1));
        $this->assertEquals([$allRoles, $disabled], $repo->getUsers(null, null, null, null, 1, 2));
    }
}
