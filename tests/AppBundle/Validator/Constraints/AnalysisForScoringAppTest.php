<?php

namespace AppBundle\Tests\Validator\Constraints;

use Symfony\Component\Validator\Tests\Constraints\AbstractConstraintValidatorTest;
use Symfony\Component\Validator\Constraints\NotBlank;
use AppBundle\Validator\Constraints\AnalysisForScoringApp;
use AppBundle\Validator\Constraints\AnalysisForScoringAppValidator;
use AppBundle\Integration\Scoring\Scoring;
use AppBundle\Analysis\Package\PackageManager;
use AppBundle\Analysis\Package\Package;
use AppBundle\Analysis\Analysis\Analysis;
use AppBundle\Entity\Task;
use AppBundle\Integration\Scoring\AppMeta\AppMeta;
use AppBundle\Integration\Scoring\AppType\AppType;

/**
 * @group validator
 */
class AnalysisForScoringAppTest extends AbstractConstraintValidatorTest
{
    private $task;
    private $analysis;
    private $package;
    private $pm;
    private $appMeta;
    private $scoring;

    public function setUp()
    {
        $this->task = new Task();
        $this->task->setAnalysisName('test');
        $this->task->setAnalysisType('primary');
        $this->task->setAnalysisVersion(1);
        $this->task->setScoringId(42);

        $this->analysis = $this->getMockBuilder(Analysis::class)
            ->setMethods(['getName', 'getType', 'getVersion', 'getStatus', 'getScoringAppTypes', 'getScoringStatuses'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->analysis->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $this->analysis->expects($this->any())
            ->method('getType')
            ->will($this->returnValue('primary'));

        $this->analysis->expects($this->any())
            ->method('getVersion')
            ->will($this->returnValue(1));

        $this->package = new Package('test');
        $this->package->addAnalysis($this->analysis);

        $this->pm = new PackageManager();
        $this->pm->addPackage($this->package);

        $this->appMeta = $this->getMockBuilder(AppMeta::class)
            ->setMethods(['getAppType', 'getStatus', 'getStatusMessage'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->scoring = $this->getMockBuilder(Scoring::class)
            ->setMethods(['getAppMeta'])
            ->disableOriginalConstructor()
            ->getMock();

        parent::setUp();
    }

    public function createValidator()
    {
        return new AnalysisForScoringAppValidator($this->pm, $this->scoring);
    }

    /**
     * @expectedException Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function testWrongConstraint()
    {
        $this->validator->validate(null, new NotBlank());
    }

    /**
     * @expectedException Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function testWrongValueType()
    {
        $this->validator->validate(new \DateTime(), new AnalysisForScoringApp());
    }

    public function testNullValueIsValid()
    {
        $this->validator->validate(null, new AnalysisForScoringApp());

        $this->assertNoViolation();
    }

    public function testNoParamsIsValid()
    {
        $entity = new Task();

        $this->validator->validate($entity, new AnalysisForScoringApp());

        $this->assertNoViolation();
    }

    public function testNoAnalysis()
    {
        $this->task->setAnalysisName('unknown');

        $this->validator->validate($this->task, new AnalysisForScoringApp());

        $this->buildViolation('task.analysisVersion.noAnalysis')
            ->atPath('property.path.analysisVersion')
            ->assertRaised();
    }

    public function testScoringError()
    {
        $this->scoring->expects($this->once())
            ->method('getAppMeta')
            ->with($this->equalTo(42))
            ->will($this->throwException(new \Exception()));

        $this->validator->validate($this->task, new AnalysisForScoringApp());

        $this->buildViolation('task.scoringId.scoringError')
            ->atPath('property.path.scoringId')
            ->assertRaised();
    }

    public function testNoApp()
    {
        $this->scoring->expects($this->once())
            ->method('getAppMeta')
            ->with($this->equalTo(42))
            ->will($this->returnValue(null));

        $this->validator->validate($this->task, new AnalysisForScoringApp());

        $this->buildViolation('task.scoringId.noApp')
            ->atPath('property.path.scoringId')
            ->assertRaised();
    }

    public function testArchivalAnalysis()
    {
        $this->analysis->expects($this->any())
            ->method('getStatus')
            ->will($this->returnValue('archival'));

        $this->scoring->expects($this->once())
            ->method('getAppMeta')
            ->with($this->equalTo(42))
            ->will($this->returnValue($this->appMeta));

        $this->validator->validate($this->task, new AnalysisForScoringApp());

        $this->buildViolation('task.analysisVersion.archivalAnalysis')
            ->atPath('property.path.analysisVersion')
            ->assertRaised();
    }

    public function testNoProduct()
    {
        $this->analysis->expects($this->any())
            ->method('getStatus')
            ->will($this->returnValue('active'));

        $this->scoring->expects($this->once())
            ->method('getAppMeta')
            ->with($this->equalTo(42))
            ->will($this->returnValue($this->appMeta));

        $this->appMeta->expects($this->once())
            ->method('getAppType')
            ->will($this->returnValue(null));

        $this->validator->validate($this->task, new AnalysisForScoringApp());

        $this->buildViolation('task.scoringId.noAppType')
            ->atPath('property.path.scoringId')
            ->assertRaised();
    }

    public function testDisabledProduct()
    {
        $this->analysis->expects($this->any())
            ->method('getStatus')
            ->will($this->returnValue('active'));

        $this->scoring->expects($this->once())
            ->method('getAppMeta')
            ->with($this->equalTo(42))
            ->will($this->returnValue($this->appMeta));

        $this->appMeta->expects($this->once())
            ->method('getAppType')
            ->will($this->returnValue(new AppType(2, 'prod', false)));

        $this->validator->validate($this->task, new AnalysisForScoringApp());

        $this->buildViolation('task.scoringId.disabledAppType')
            ->setParameter('%appTypeId%', 2)
            ->setParameter('%appTypeTitle%', 'prod')
            ->atPath('property.path.scoringId')
            ->assertRaised();
    }

    public function testUnsupportedProduct()
    {
        $this->analysis->expects($this->any())
            ->method('getStatus')
            ->will($this->returnValue('active'));

        $this->analysis->expects($this->any())
            ->method('getScoringAppTypes')
            ->will($this->returnValue([1, 2]));

        $this->scoring->expects($this->once())
            ->method('getAppMeta')
            ->with($this->equalTo(42))
            ->will($this->returnValue($this->appMeta));

        $this->appMeta->expects($this->once())
            ->method('getAppType')
            ->will($this->returnValue(new AppType(3, 'prod', true)));

        $this->validator->validate($this->task, new AnalysisForScoringApp());

        $this->buildViolation('task.scoringId.unsupportedAppType')
            ->setParameter('%appTypeId%', 3)
            ->setParameter('%appTypeTitle%', 'prod')
            ->setParameter('%supportedIds%', '1, 2')
            ->atPath('property.path.scoringId')
            ->assertRaised();
    }

    public function testBadStatus()
    {
        $this->analysis->expects($this->any())
            ->method('getStatus')
            ->will($this->returnValue('active'));

        $this->analysis->expects($this->any())
            ->method('getScoringAppTypes')
            ->will($this->returnValue([1, 2]));

        $this->analysis->expects($this->any())
            ->method('getScoringStatuses')
            ->will($this->returnValue([5, 6]));

        $this->scoring->expects($this->once())
            ->method('getAppMeta')
            ->with($this->equalTo(42))
            ->will($this->returnValue($this->appMeta));

        $this->appMeta->expects($this->once())
            ->method('getAppType')
            ->will($this->returnValue(new AppType(2, 'prod', true)));

        $this->appMeta->expects($this->exactly(2))
            ->method('getStatus')
            ->will($this->returnValue(7));

        $this->appMeta->expects($this->once())
            ->method('getStatusMessage')
            ->will($this->returnValue('status'));

        $this->validator->validate($this->task, new AnalysisForScoringApp());

        $this->buildViolation('task.scoringId.badStatus')
            ->setParameter('%statusId%', 7)
            ->setParameter('%statusTitle%', 'status')
            ->setParameter('%supportedIds%', '5, 6')
            ->atPath('property.path.scoringId')
            ->assertRaised();
    }

    public function testPositive()
    {
        $this->analysis->expects($this->any())
            ->method('getStatus')
            ->will($this->returnValue('active'));

        $this->analysis->expects($this->any())
            ->method('getScoringAppTypes')
            ->will($this->returnValue([1, 2]));

        $this->analysis->expects($this->any())
            ->method('getScoringStatuses')
            ->will($this->returnValue([5, 6]));

        $this->scoring->expects($this->once())
            ->method('getAppMeta')
            ->with($this->equalTo(42))
            ->will($this->returnValue($this->appMeta));

        $this->appMeta->expects($this->once())
            ->method('getAppType')
            ->will($this->returnValue(new AppType(2, 'prod', true)));

        $this->appMeta->expects($this->once())
            ->method('getStatus')
            ->will($this->returnValue(6));

        $this->validator->validate($this->task, new AnalysisForScoringApp());

        $this->assertNoViolation();
    }
}
