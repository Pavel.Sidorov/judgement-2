<?php

namespace AppBundle\Tests\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Tests\Constraints\AbstractConstraintValidatorTest;
use Symfony\Component\Validator\Constraints\NotBlank;
use AppBundle\Repository\TaskRepository;
use AppBundle\Validator\Constraints\TasksPerScoringApp;
use AppBundle\Validator\Constraints\TasksPerScoringAppValidator;
use AppBundle\Entity\Task;

/**
 * @group validator
 */
class TasksPerScoringAppTest extends AbstractConstraintValidatorTest
{
    private $em;
    private $repo;

    public function setUp()
    {
        $this->repo = $this->getMockBuilder(TaskRepository::class)
            ->setMethods(['countRootTasks'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->em = $this->getMockBuilder(EntityManager::class)
            ->setMethods(['getRepository'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->em->expects($this->any())
            ->method('getRepository')
            ->with($this->equalTo('AppBundle:Task'))
            ->will($this->returnValue($this->repo));

        parent::setUp();
    }

    public function createValidator()
    {
        return new TasksPerScoringAppValidator($this->em);
    }

    /**
     * @expectedException Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function testWrongConstraint()
    {
        $this->validator->validate(null, new NotBlank());
    }

    /**
     * @expectedException Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function testWrongValueType()
    {
        $this->validator->validate(new \DateTime(), new TasksPerScoringApp());
    }

    public function testNullValueIsValid()
    {
        $this->validator->validate(null, new TasksPerScoringApp());

        $this->assertNoViolation();
    }

    public function testNoScoringIdIsValid()
    {
        $entity = new Task();

        $this->validator->validate($entity, new TasksPerScoringApp());

        $this->assertNoViolation();
    }

    public function testPositive()
    {
        $this->repo->expects($this->once())
            ->method('countRootTasks')
            ->with($this->equalTo('test'), $this->equalTo('primary'), $this->equalTo(112), $this->equalTo(null), $this->equalTo(42))
            ->will($this->returnValue(0));

        $entity = new class() extends Task {
            public function getId() {
                return 42;
            }
        };
        $entity->setAnalysisName('test');
        $entity->setAnalysisType('primary');
        $entity->setScoringId(112);

        $this->validator->validate($entity, new TasksPerScoringApp());

        $this->assertNoViolation();
    }

    public function testNegative()
    {
        $this->repo->expects($this->once())
            ->method('countRootTasks')
            ->with($this->equalTo('test'), $this->equalTo('primary'), $this->equalTo(112), $this->equalTo(null), $this->equalTo(42))
            ->will($this->returnValue(1));

        $entity = new class() extends Task {
            public function getId() {
                return 42;
            }
        };
        $entity->setAnalysisName('test');
        $entity->setAnalysisType('primary');
        $entity->setScoringId(112);

        $this->validator->validate($entity, new TasksPerScoringApp());

        $this->buildViolation('task.scoringId.tasksPerScoringApp')
            ->atPath('property.path.scoringId')
            ->assertRaised();
    }
}
