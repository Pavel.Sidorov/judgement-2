<?php

namespace Tests;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;

/**
 * TestCase
 */
class AppTestCase extends KernelTestCase
{
    /**
     * Инициализация
     */
    protected function setUp()
    {
        parent::setUp();
        static::bootKernel();
    }

    /**
     * Доступ к контейнеру
     *
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        return self::$kernel->getContainer();
    }

    /**
     * Доступ к doctrine
     *
     * @return EntityManagerInterface
     */
    protected function getDoctrine()
    {
        return $this->getContainer()->get('doctrine');
    }

    /**
     * Загрузить фикстуры
     *
     * @param array $fixtures
     */
    protected function loadFixtures(array $fixtures)
    {
        $em = $this->getDoctrine()->getManager();

        $loader = new ContainerAwareLoader($this->getContainer());
        foreach ($fixtures as $fixture) {
            $loader->addFixture($fixture);
        }

        $purger = new ORMPurger();
        $executor = new ORMExecutor($em, $purger);

        //$em->getConnection()->query('SET FOREIGN_KEY_CHECKS=0');
        $executor->execute($loader->getFixtures());
        //$em->getConnection()->query('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Аутентифицировать клиент
     *
     * @param Client $client
     * @param string $username
     * @param string $firewall
     *
     * @return TestCase
     */
    protected function authenticate(Client $client, $username, $firewall = 'default')
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneByUsername($username);

        if (!$user) {
            throw new \Exception(sprintf('Пользователь "%s" не найден', $username));
        }

        $token = new UsernamePasswordToken($user, null, $firewall, $user->getRoles());

        $session = $client->getContainer()->get('session');
        $session->set('_security_'.$firewall, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        return $this;
    }
}
